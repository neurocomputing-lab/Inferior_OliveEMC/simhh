# simHH - HPC Multicompartmnetal HH simulator 

## Simulator setup through JSON files   
The Multicompartmental H-H neural net simulator accepts basic run options through command line, and more complete configuration through structured JSON files.  
The distinct configurable properties of the simulation are represented as top-level properties in the JSON file.  
  
The simulation can be configured from a single file containing all necessary data.  
The configuration file to be used in the simulation are specified in the following command-line form:  
`<executable> <configuration_file>`  eg. `./build/simHH_cmdwrap ./configuration/test.json`
  
JSON configuration file examples can be found in the simulator's source directory under the folder `./configuration`.  
  
### Config file structure  
A JSON config file may contain the following properties:  
  
- `SimTime`: The time duration to be simulated, a positive number in seconds.  
- `SimTimestep`: The time step to be used by the ODE solver to simulate neuron dynamics, in seconds. Warning: The simulation may diverge for unsuitable timestep values !  
- `GPU_backend`: `False` for CPU execution a positive integer for the amount of GPU used for the GPU backend.
- `MPI_alltoallv`: `True` for using the alltoallv MPI communication schema `False` for using the Allgather schema.



- `CellPopulation`: Describes the population of cells participating in the simulation. Defined in [Cell Population](#cell-population) section.
- `ConnectivityModel`: Description of the gap junction graph connecting the cells, defined in [Connectivity Model](#connectivity-model) section.
- `StimulusModel`: Description of the external stimulus applied to the cells, defined in [Stimulus Model](#stimulus-model) section. 
- `MetaDataMonitor`: [optional] When meta data is required the user can set this up via the metadatamonit, defined in [Meta Data Monitor](#meta-data-monitor).
- `OutputMonitor`:

### Cell population models
Neural nets are made up of several neural cells, with connections among themselves. Depending on neural tissue type examined, these cells may be considered identical in electrophysiology, a mix of distinct neuron types, or each one unique with regard to elecrophysiological behaviour. The JSON object describing a single cell's eelctrophysiology is defined in [Neuron Model](#neuron-model) section. The simulator currently supports only one neuron population models, resulting in only one entry for `CellPopulation` top level property's:
	- `NeuronModel`: The common electrophysiological model of all simulated neurons. (A Json vector off all cells in the network.)

### Neuron model  
Each neuron is currently modeled in the simulator as a linear chain of interconnected cell compartments, each representing a portion of the cell. For example, the ball-and-stick neuron model, which is commonly used in neuroscience research, segments a neuron into the soma part, where most complex chemical processes take place, and the dendrites part, which roughly models the kinetics of all stimuli received and emitted by the cell. Interaction with probes or other neurons is assumed to be made by contact among dendrites. *Currently, the dendrites-containing part is assumed to be the first in the specified compartment list.*  
  
Compartment voltages are always measured with the cell exterior as reference.  
Each compartment is currently modeled to have the following electrical properties:  
- Electrical capacitance, connecting electric currents to rate of voltage change  
- Passive electrical connection to adjacent compartments, modeled as TODO. Ion concentrations are assumed to be isolated between compartments.  
- Passive electrical connection to intercellular fluid, modeled as a resistor and reversal potential in series.  
- Active ion channels, permitting flow of certain ions dynamically with regard to time. This type of channel is modeled as a constant reversal potential, in series with a variable resistor. The resistor's conductance is directly controlled by some state state variables *yᵢ* internal to the channel, called *gate variables*. Specifically, channel conductance is *g&nbsp;\*&nbsp;∏(yᵢ^pᵢ)*, where *g* is maximum ion channel conductivity and *pᵢ* is the power factor of each gate variable. Despite the resistive electrical behaviour of ion channels, only specific types of ions are transported through them.  
  
The model is assumed to be correct; the user is required to specify a stable model, to get non-divergent results.  
  
The `NeuronModel` object contains the `Compartments` top-level property. `Compartments` must be a list of compartment-specifying objects. There must be at least one comaprtment. The other properties that can be used are:
* `Label`: TODO
* `NeuronModelSize`: TODO
* `RandomSeed`: TODO
  
Each compartment object must define the following properties:  
* `InverseCapacitance`: A positive number, representing the compartment's electrical capacitance (in TODO units)  
* `PassiveLeakConductivity`: A positive number, representing the compartment membrane's ohmic electrical conductivity of the membrane portion, discounting active ion channels (in TODO units),  
* `PassiveLeakInversionPotential`: A number representing the compartment membrane's reversal potential from cell interior to exterior (in milliVolts)
* `InitialVoltage`: A number representing the typical initial voltage (in milliVolts) of the compartment, in the simulation. May be overridden by explicit voltage initialization through `CellPopulation` options.  
* `ConductivityRatio`: TODO  
* `IonChannels`: A list of objects, each specifying an ion channel connecting the compartment's intracellular fluid with the cell's exterior. This list can be empty, to model an entirely passive comaprtment.
 * [optional] `CalciumConcentration`: an ION description describing the calcium dynamics 

Each ion channel object must define the following properties:  
* `LeakConductivity`: a positive number, representing the ion channel's electrical conductivity (in TODO units),  
* [optional] `LeakConductivity_Randomization_offset`: TODO
* `LeakInversionPotential`: A number representing the ion channel's reversal potential (in milliVolts)  
* [optional] `LeakInversionPotential_Randomization_offset`: TODO
* `IsCalciumChannel`: A boolean of whether the ion channel conveys Ca²⁺ ions. *This property is important when calcium-activated ion channels are modeled; each calcium-activated ion channel is controlled only by the Ca²⁺ preceding it in the specified ion channels list*.  
* `Gates`: A list of objects, each specifying a gate variable controlling the channel, as described above.  
  
Gate variables model the chemical ion gate structures that enable or disable ion flow through the cell's membrane. Electrical conductivity of the ion channel they belong to is proportional to each variable, raised to a power specific to each variable.  
A gate variable may be a function of instant membrane potential (called static further on, since it displays no memory effect), or the variable's rate of change is a function of the variable's value and the compartment's membrane potential, and also calcium concentration, in calcium activated ion channels. Gate variable dynamics are commonly described in terms of functions *α* and *β*, which govern the rate of the respective chemical gate structure opening and closing. The *α* and *β* values are functions of membrane potential, in the classical HH model.  
  
Each ion channel gate object must specify the following:  
* `AlphaFormula`: The *a(x)* function's formulaic definition. Defined in detail further on.  
* `BetaFormula`: The *β(x)* function's formulaic definition. Defined in detail further on.  
* `CurrentGating`:  
* Is `BetaFunction` for static gates whose value is determined by instant membrane potential. Value yᵢ is the β function of membrane potential, as described in `BetaFormula`.  
* Is `GateVariable` for dynamic gates, gating by their internal state variable yᵢ .  
* `CurrentGatingExponent`: The power factor of the gate variable affecting ion channel conductivity. *Currently only positive integers are supported.*  
* `AlphaGating`: The free variable used for *α(x)* computation. Typically `CompartmentVoltage`. *Alternatively, `PreviousGateValue` to implement more complex gates, like calcium-activated ones where the calcium concentration pseudo-gate is listed.*  
* `Dynamics`:  
*(Not used when gate is static.)*  
	* `Classical`: *yᵢ* rate of change is *α\*(1-yᵢ) - β\*yᵢ*  
	* `Ratio`: *yᵢ* rate of change is *(α-yᵢ)/β*  
	* `ConcentratedCalcium`: *yᵢ* is not a channel gate in itself, but represents calcium concentration, in TODO units. *The next gate in the gate description list may be controlled by this additional state variable.* *Calcium concentration is computed by the flow of calcium channels preceding the current channel in the channels list.* Concentration dynamics are those of a leaky bucket; the α function representing the ratio between calcium flow(in TODO units) and rate of concentration change(in TODO units/milliSecond), and the β function represents calcium ions leakage per time per existing concentration (in TODO units).  
* `InitialState`: The initial value of the gate's state variable. Not used when gate is static.  
  
  
Each formula object is in one of the following *f(V)* forms, as specified by the `type` property:  
* `LinearByExponential` type:  
$$  f(V) =  \rm{offset} +{ \frac {\rm{varscale_{lin}} \cdot ( V + \rm{varoffset_{lin}} )} { \rm{scale_{exp}} \cdot e^{( \rm{varscale_{exp}} \cdot ( V + \rm{varoffset_{exp}} ) )} + \rm{offset_{exp}} } } $$ 
Required variables are respectively `LinearVarOffset`, `LinearVarScale`, `ExponentialVarOffset`, `ExponentialVarScale`, `ExponentialScale`, `ExponentialOffset`, `Offset`.  

* `ExponentialByExponential` type:  
$$  f(V) =  \rm{offset} +{ \frac {scale_{num} \cdot e^{( \rm{varscale_{num}} \cdot ( V + \rm{varoffset_{num}} ) )} + offset_{num}} { \rm{scale_{den}} \cdot e^{( \rm{varscale_{den}} \cdot ( V + \rm{varoffset_{den}} ) )} + \rm{offset_{den}} } }  $$ 
Required variables are respectively `NumScale`,`NumVarScale`, `NumVarOffset`, `NumOffset`,`DenScale`,`DenVarScale`, `DenVarOffset`, `DenOffset`, `Offset`.  

* `InverseOfTwoExponentials` type:
$$f(V) =  \rm{offset} +{ \frac{1}{ \rm{scale_{exp1}} \cdot e^{( \rm{varscale_{exp1}} \cdot ( V + \rm{varoffset_{exp1}} ) )} + scale_{exp2} \cdot e^{( \rm{varscale_{exp2}} \cdot ( V + \rm{varoffset_{exp2}}
) )} + \rm{offset_{den} }} }  $$ 
Required variables are respectively `OneScale`,`OneVarScale`, `OneVarOffset`, `TwoScale`,`TwoVarScale`, `TwoVarOffset`, `DenOffset`, `Offset`.  

* `ClippedLinear` type:  
$$  f(x) =  \rm{max}(\rm{scale}\cdot ( V + \rm{offset}), \rm{maximum})  $$  
Required variables are respectively `VarScale`,`VarOffset`, `Maximum`.  
  
### Connectivity model  
The simulator models connectivity among neurons, as electrical gap junctions between pairs of cells. Gap junction current from cell i to cell j is 
$$  I_{gap} = w_{ij} \cdot (V_i-V_j) \cdot e^{(0.2 + 0.8(-0.01*(V_i-V_j)^2))}  $$
(voltage in milliVolts, TODO current units) , where Vᵢ-Vⱼ are the membrane potentials of the two involved neurons on their contacting compartments, and *wᵢⱼ* is the gap junction weight factor. A junction weight of zero is equivalent to no connectivity between the corresponding pair of neurons. Thus, a gap junction connectivity can be described by a matrix *W*, where Wᵢⱼ represents each neuron pair's gap junction weight, or zero for non-existing junctions.  
Currently, two gap junction connectivity models are supported, selected by the `Type` property of the `ConnectivityModel` object:  

`Full`: All neurons are connected to each other through gap junctions. All junctions have the same weight. The following property must be specified:  
* `Weight`: The weight for all gap junctions.  


`RandomBinary`: A pseudo-random set of gap junctions is selected, out of the set of all posible neuron pairs. The probability of each neuron pair being connected by a gap junction is constant, and represents the expected connectivity graph density. All existing junctions have the same weight.  
*Currently, randomly generated gap junctions are assumed to be asymmetrical; this means electrical current may leave a cell according to a voltage difference with an adjacent cell, without the same amount of current arriving at the adjacent cell. This may change in future versions.*  
The following properties must be specified:  
* `Density`: The expected connectivity graph density. At least 0, at most 1.  
* `Weight`: The weight for all existing gap junctions.  
* `RandomSeed`: A 32-bit signed integer to seed the pseudo-random number generator, or `'time'`to seed with the number of seconds since epoch. For a simulator version, given the same amount of neurons, same density and same random seed, the randomly generated graph should be identical for each run.  

`Gaussian`: TODO

`Read`: TODO


### Stimulus model  
The simulator supports applying external stimuli to neurons in the simulated net, in the form of immediate electrical current injection. Current is assumed to be injected in the first compartment of each neuron. Stimulus type is specified by the `Type` property of the `StimulusModel` object:  

`DcCurrentPulseModulo`: A single DC current pulse, applied on all cells at the same time window. (The window may not entirely overlap the simulation time window, in which case the stimulus is partially applied or omitted, as appropriate.) Pulse amplitude *I* is a function of each simulated neuron's serial number *i* of instantiation, such that *I = c + r\*(i mod k)*, where *c, r, k* are specified constants. The following properties must be specified:  
* `StartTime`: The time from simulation start the pulse commences, in seconds  
* `Duration`: The duration of the pulse, in seconds  
* `BaseCurrent`: Constant *c* mentioned above, in TODO units  
* `Modulo`: Constant *k* mentioned above, must be a positive integer  
* `ModuloCurrent`: Constant *r* mentioned above, in TODO units  

`TODO`: add OU noise option ++ the rest


### Meta Data Monitor
`TODO`

### Meta Data Monitor
`TODO`