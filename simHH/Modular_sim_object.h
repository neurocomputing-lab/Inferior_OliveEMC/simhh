#ifndef MODULARHH_H
#define MODULARHH_H

#include <vector>
#include <functional>
#include <cmath>

// in which data chucks do we write formatted outputfile ?
#define CHUNK_SIZE 1024

struct Formula{
	//byte size of enum is int, ways to force size are non portable
	enum {
		NONE = 0,
		LINEAR_BY_EXPONENTIAL,
		INVERSE_OF_TWO_EXPONENTIALS,
		EXPONENTIAL_BY_EXPONENTIAL,
		CLIPPED_LINEAR
	}type;
	struct LinByExp{
		//( ( lin_scale * (V + lin_offset) ) / ( exp_scale * exp( exp_var_scale * ( V + exp_offset) ) ) ) + offset
		// LATEX code: \it{offset} +{  \frac {\it{varscale_{lin}} \cdot ( V + \it{varoffset_{lin}} )} {  \it{scale_{exp}} \cdot  e^{( \it{varscale_{exp}} \cdot ( V + \it{varoffset_{exp}} ) )} + \it{offset_{exp}} } }
		float lin_offset;
		float lin_scale;
		float exp_var_offset; //inside the exponential
		float exp_var_scale;
		float exp_scale; //outside the exponential
		float exp_offset;
		float offset;
	};
	struct InvOfTwoExp{
		//( 1 / ( exp1_scale * exp( exp1_var_scale * ( V + exp1_var_offset ) ) + exp2_scale * exp( exp2_var_scale * ( V + exp2_var_offset ) ) + den_offset ) ) + offset
		// LATEX code: \it{offset} +{  \frac{1}{  \it{scale_{exp1}} \cdot  e^{( \it{varscale_{exp1}} \cdot ( V + \it{varoffset_{exp1}} ) )} + scale_{exp2} \cdot e^{( \it{varscale_{exp2}} \cdot ( V + \it{varoffset_{exp2}} ) )} + \it{offset_{den} }} }
		float exp1_var_offset;
		float exp1_var_scale;
		float exp1_scale;
		float exp2_var_offset;
		float exp2_var_scale;
		float exp2_scale;
		float den_offset;
		float offset;
	};
	struct ExpByExp{
		//( ( num_scale * exp( num_var_scale * ( v + num_var_offset ) ) + num_offset ) / ( den_scale * exp( den_var_scale * ( v + den_var_offset ) ) + den_offset ) ) + offset
		// LATEX code: \it{offset} +{  \frac {scale_{num} \cdot e^{( \it{varscale_{num}} \cdot ( V + \it{varoffset_{num}} ) )} + offset_{num}} {  \it{scale_{den}} \cdot  e^{( \it{varscale_{den}} \cdot ( V + \it{varoffset_{den}} ) )} + \it{offset_{den}} } }
		float num_var_offset;
		float num_var_scale;
		float num_scale;
		float num_offset;
		float den_var_offset;
		float den_var_scale;
		float den_scale;
		float den_offset;
		float offset;
	};
	struct ClippedLinear{
		// max( scale * (v + offset) , maximum)
		float offset;
		float scale;
		float maximum;
	};
	union{
		LinByExp lin_by_exp;
		InvOfTwoExp inv_of_two_exp;
		ExpByExp exp_by_exp;
		ClippedLinear clipped_linear;
	};

};               //40byte   //this is flexHH minded
struct GateInfo{
	Formula beta_formula;
	enum{
		GATE_BY_VAR,
		GATE_BY_BETA
	} current_gating_variable;
	int32_t gating_power_factor; //LATER perhaps float?

	enum{
		ALPHA_OF_POTENTIAL,
		ALPHA_OF_PREVIOUS_GATE_VALUE,
		ALPHA_OF_CALCIUM_CONCENTRATION
	} alpha_dependency;
	Formula alpha_formula;

	enum{
		DYNAMICS_CLASSIC, // (1 - y)*alpha - y*beta
		DYNAMICS_RATIO, // (alpha - y) / beta
		DYNAMICS_CONCENTRATED_CALCIUM // ( -i_Ca )*alpha - y*beta
	}value_dynamics;

	float initial_value;
};              //100byte
struct ChannelInfo{
	std::vector<GateInfo> multiplied_gates;
	float leak_conductivity;
	float leak_conductivity_Randomization_offset;
	float leak_inversion_potential;
	float leak_inversion_potential_Randomization_offset;
    uint32_t random_seed;
	bool is_ca2plus_channel;
};           //48byte
struct CompartmentInfo{
	std::vector<ChannelInfo> ion_channels;
	GateInfo CalciumConcentration;
	bool  enable_CalciumConcentration;
	float inverse_capacitance;
	float passive_leak_conductivity;
    float passive_leak_conductivity_Randomization_offset;
	float passive_leak_inversion_potential;
	float gp;
	float initial_voltage;
};       //152byte
struct NeuronInfo{
    uint32_t multiplier;
    uint32_t random_seed;
    char custom_leak_conductance_lists[400];
    bool enable_custom_leak;
    float *custom_leak_array;
    float Ratio_Random_offset;
    std::vector<CompartmentInfo> series_compartments;
    bool ok() const {
        return !series_compartments.empty();
    }
};

struct StimulusInfo{
    enum{
        UNSET = 0,
        NONE,
        DCCURRENTPULSEMODULO,
        DCCURRENTSTEPANDHOLD,
        DCCURRENTRAMPANDHOLD,
        DCCURRENTPULSECELL,
        ORNSTEINUHLENBECK
    }type;

    struct DcCurrentPulseCell{
        uint32_t start_step = 0;       //start
        uint32_t end_step = 0;         //end
        uint32_t Cell_id = 0;
        float base          = 0;
        float pulse         = 0;
    };
    struct DcCurrentPulseModulo{
        uint32_t start_step = 0;       //start
        uint32_t end_step   = 0;         //end
        int modulo          = 0;
        float modulo_scale  = 0;
        float base          = 0;
    };
    struct DcCurrentStepandHold{
        uint32_t start_step = 0;            //start
        uint32_t end_step = 0;         //end
        uint32_t end_hold = 0;
        float basecurrent = 0;
        float holdcurrent = 0;
        float stepcurrent = 0;
    };
    struct DcCurrentRampandHold{
        uint32_t start_ramp = 0;            //start
        uint32_t end_ramp = 0;         //end
        uint32_t end_hold = 0;
        float basecurrent = 0;
        float holdcurrent = 0;
        float rampcurrent = 0;
    };
    struct OrnsteinUhlenbeck{
        float theta = 0;
        float sigma = 1;
        float mu = 0;
        float alpha = 0;
        float dt = 0;
        float sqrt_dt = 0;
        uint32_t random_seed = 0;
        XorShiftMul *rng;
        OrnsteinUhlenbeck(){
            random_seed =  0b10111110111010101100100010001011;
            rng = new XorShiftMul(((uint64_t) random_seed << 32) + random_seed);
        }
    };
    union{
        DcCurrentPulseModulo dcCurrentPulseModulo;
        DcCurrentStepandHold dcCurrentStepandHold;
        DcCurrentRampandHold dcCurrentRampandHold;
        DcCurrentPulseCell   dcCurrentPulseCell;
        OrnsteinUhlenbeck    ornsteinUhlenbeck;
    };
    bool ok() const {
        return (type != UNSET);
    }
    StimulusInfo(){ type = UNSET; }
};
struct VoltageInitialization{
	enum Type{
		DEFAULT,
		PER_COMPARTMENT
	};

	struct PerCompartment{
		struct CompartmentInit{
			enum Type{
				COMP_DEFAULT,
				COMP_MODULO
			};
			struct CompModulo{
				float base;
				int modulo;
				float modulo_scale;
				bool ok() const{
					return (std::isfinite(base) && modulo > 0 && std::isfinite(modulo_scale));
				}
			};
			CompModulo comp_modulo;

			Type type;
			CompartmentInit(){ type = COMP_DEFAULT; }
			bool ok() const{
				if(type == COMP_DEFAULT) return true;
				else if(type == COMP_MODULO) return comp_modulo.ok();
				else return false;
			}
		};
		std::vector<CompartmentInit> comps;
	};

	Type type;
	PerCompartment per_compartment;
	VoltageInitialization(){ type = DEFAULT;}

	bool ok() const{
		if(type == DEFAULT) return true;
		else if(type == PER_COMPARTMENT){
			if(per_compartment.comps.size() == 0) return false;
			for(auto comp : per_compartment.comps){
				if(!( comp.ok() )) return false;
			}
			return true;
		}
		else return false;
	}
};
struct ConnectivityInfo{
	enum{
		UNSET = 0,
		NONE,
		CONSTANT,
		READ,
		RANDOM_BINARY,
		GAUSSIAN
	}type;
	bool use_condu;
	struct Constant{
		float weight;
	};
	struct Read{
		char filename[400];
		float weight;
		uint32_t MaxDensity;
	};
    struct RandomBinary{
        float weight;
        uint32_t density;
        uint32_t random_seed;
    };
    struct Gaussian{
        float weight;
        uint32_t density;
        float mean;
        float variance;
        uint32_t random_seed;
    };
	union{
		Constant constant;
		Read read;
		RandomBinary random_binary;
	    Gaussian gaussian;
	};
	[[nodiscard]] bool ok() const {
		return (type != UNSET);
	}
	ConnectivityInfo():
	    type(UNSET),
	    use_condu(false)
	    {}
};
struct CellPopulationInfo{
    enum{
		UNSET,
		SET
	}type;
    size_t population_size;
    size_t comparment_count;
    size_t channel_count;
    size_t gate_count;
    std::vector<NeuronInfo> neuron_models;
    [[nodiscard]] bool ok() const{
        if(type == UNSET) return false;
        if(neuron_models.empty()) return false;
        for(auto model : neuron_models) if(!model.ok()) return false;
        return true;
    }
    //default constructor
    CellPopulationInfo(): type(UNSET), population_size(0),comparment_count(0),channel_count(0),gate_count(0){}
};
struct OutputMonitor{
    enum{
        UNSET,
        NONE,
        ALL
    }type;

    miniLogger log = miniLogger(LOG_DEFAULT,std::cout, "OutputMonitor");
    std::string name;
    bool FilesOpen;
    bool formatting;
    bool WriteCompartPotentials;
    FILE *WriteCompartPotentials_f;
    bool WriteCurrents;
    FILE *WriteCurrents_f;
    bool WriteActivationVariables;
    FILE *WriteActivationVariables_f;
    bool WriteCalciumLevels;
    FILE *WriteCalciumLevels_f;
    bool WriteGapJunctions;
    FILE *WriteGapJunctions_f;
    uint32_t timestep_save_interval;

    OutputMonitor(){
        type = NONE;
        name = "";
        FilesOpen = false;
        formatting = false;
        WriteCompartPotentials = false;
        WriteCurrents = false;
        WriteActivationVariables = false;
        WriteCalciumLevels = false;
        WriteGapJunctions = false;
        WriteCompartPotentials_f = nullptr;
        WriteCurrents_f= nullptr;
        WriteActivationVariables_f= nullptr;
        WriteCalciumLevels_f= nullptr;
        WriteGapJunctions_f = nullptr;
        timestep_save_interval = 0;
    }
    bool OpenFiles(int world_rank, bool MPI_inuse){

        //---> TODOO gaat mis met preciesies
        FilesOpen = true;
        if(WriteCompartPotentials) {
            char buff[250];
            if (MPI_inuse) {
                snprintf(buff, sizeof(buff), "%s_Compartment_potentials_%d.dat", name.c_str(), world_rank);
            } else {
                snprintf(buff, sizeof(buff), "%s_Compartment_potentials.dat", name.c_str());
            }
            if((WriteCompartPotentials_f=fopen(buff, "w"))== nullptr){
                log(LOG_ERR) << "Could not open file: " << buff << LOG_ENDL;
                exit(1);
                return false;
            }
        }

        if(WriteCurrents){
            char buff[100];
            if (MPI_inuse) {
                snprintf(buff, sizeof(buff), "%s_Channel_currents_%d.dat", name.c_str(), world_rank);
            } else {
                snprintf(buff, sizeof(buff), "%s_Channel_currents.dat", name.c_str());
            }
            if((WriteCurrents_f=fopen(buff, "w"))== nullptr){
                log(LOG_ERR) <<"Could not open file: " << buff << LOG_ENDL;
                exit(1);
                return false;
            }
        }

        if(WriteCalciumLevels){
            char buff[100];
            if (MPI_inuse) {
                snprintf(buff, sizeof(buff), "%s_Compartment_calclvl_%d.dat", name.c_str(), world_rank);
            } else {
                snprintf(buff, sizeof(buff), "%s_Compartment_calclvl.dat", name.c_str());
            }
            if((WriteCalciumLevels_f=fopen(buff, "w"))== nullptr){
                log(LOG_ERR) <<"Could not open file: " << buff << LOG_ENDL;
                exit(1);
                return false;
            }
        }
        if(WriteActivationVariables){
            char buff[100];
            if (MPI_inuse) {
                snprintf(buff, sizeof(buff), "%s_Gate_activationlvl_%d.dat", name.c_str(), world_rank);
            } else {
                snprintf(buff, sizeof(buff), "%s_Gate_activationlvl.dat", name.c_str());
            }
            if((WriteActivationVariables_f=fopen(buff, "w"))== nullptr){
                log(LOG_ERR) <<"Could not open file: " << buff << LOG_ENDL;
                exit(1);
                return false;
            }
        }
        return true;
    }
    bool CloseFiles(){
        if(!FilesOpen) {
            log(LOG_ERR) <<"Trying to close outputFiles but they where never opened" << LOG_ENDL;
            return false;
        }
        if(WriteCompartPotentials) {
            if(fclose(WriteCompartPotentials_f) != false) {
                log(LOG_ERR) <<"Could not close WriteCompartPotentials file" << LOG_ENDL;
            }
        }
        if(WriteCurrents){
            if(fclose(WriteCurrents_f)!= false) {
                log(LOG_ERR) <<"Could not close WriteCurrents file" << LOG_ENDL;
            }
        }
        if(WriteCalciumLevels){
            if(fclose(WriteCalciumLevels_f) != false) {
                log(LOG_ERR) <<"Could not close WriteCalciumLevels file" << LOG_ENDL;
            }
        }
        if(WriteActivationVariables){
            if(fclose(WriteActivationVariables_f) != false) {
                log(LOG_ERR) <<"Could not close WriteActivationVariables file" << LOG_ENDL;
            }
        }
        return true;
    }
    bool PrintTimestep(const float *vsNow, const float *ysNow, const float *calcNow, const float *CurrentNow,  float dt, uint32_t sStep, uint32_t nCells, uint32_t nComps,uint32_t nChannels, uint32_t nGates, uint32_t nSteps, timestamp_t *timing) const {
        Timer functimer;
        if(WriteCompartPotentials){
            //--->pretty print
            if(formatting){
                if (sStep == 0) {
                    fprintf(WriteCompartPotentials_f, "nCells = %d\t, nCompartments = %d\t, nChannels = %d\t, nGates = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nComps, nChannels,nGates, dt * nSteps, nSteps, dt);
                    fprintf(WriteCompartPotentials_f, "\n");
                }
                int buffer_count = 0;
                char *file_buffer = new char[CHUNK_SIZE + 64];
                buffer_count += sprintf( &file_buffer[buffer_count], "%d\t",sStep);
                for (uint32_t k = 0; k < nComps; k++) {
                    buffer_count += sprintf( &file_buffer[buffer_count], "%+03.24f\t",vsNow[k]);  // 13 chars big !
                    if( buffer_count >= CHUNK_SIZE ) //if buffer is chucksize or bigger print the buffer to file
                    {
                        fwrite(file_buffer, buffer_count, 1, WriteCompartPotentials_f);
                        buffer_count = 0 ;
                    }
                }
                //---> print endofline when formatting is on.
                buffer_count += sprintf( &file_buffer[buffer_count], "\n");
                //---> make shure the buffer is empy
                if( buffer_count > 0 ) fwrite( file_buffer, buffer_count, 1, WriteCompartPotentials_f) ;
                //---> delete allocation
                delete[] file_buffer;
            }
            else {
                //---> Raw dump
                fwrite(vsNow, sizeof(float), nComps, WriteCompartPotentials_f);
            }
        }
        if(WriteCalciumLevels) {
            //--->pretty print
            if(formatting){
                if (sStep == 0) {
                    fprintf(WriteCalciumLevels_f, "nCells = %d\t, nCompartments = %d\t, nChannels = %d\t, nGates = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nComps, nChannels,nGates, dt * nSteps, nSteps, dt);
                    fprintf(WriteCalciumLevels_f, "\n");
                }
                int buffer_count = 0;
                char *file_buffer = new char[CHUNK_SIZE + 64];
                buffer_count += sprintf( &file_buffer[buffer_count], "%d\t",sStep);
                for (uint32_t k = 0; k < nComps; k++) {
                    buffer_count += sprintf( &file_buffer[buffer_count], "%+03.6f\t",calcNow[k]);  // 13 chars big !
                    if( buffer_count >= CHUNK_SIZE ) //if buffer is chucksize or bigger print the buffer to file
                    {
                        fwrite(file_buffer, buffer_count, 1, WriteCalciumLevels_f);
                        buffer_count = 0 ;
                    }
                }
                //---> print endofline when formatting is on.
                buffer_count += sprintf( &file_buffer[buffer_count], "\n");
                //---> make shure the buffer is empy
                if( buffer_count > 0 ) fwrite( file_buffer, buffer_count, 1, WriteCalciumLevels_f) ;
                //---> delete allocation
                delete[] file_buffer;
            }
            else {
                //---> Raw dump
                fwrite(calcNow, sizeof(float), nComps, WriteCalciumLevels_f);
            }
        }
        if(WriteCurrents){

            //--->pretty print
            if(formatting){
                if (sStep == 0) {
                    fprintf(WriteCurrents_f, "nCells = %d\t, nCompartments = %d\t, nChannels = %d\t, nGates = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nComps, nChannels,nGates, dt * nSteps, nSteps, dt);
                    fprintf(WriteCurrents_f, "\n");
                }
                int buffer_count = 0;
                char *file_buffer = new char[CHUNK_SIZE + 64];
                buffer_count += sprintf( &file_buffer[buffer_count], "%d\t",sStep);
                for (uint32_t k = 0; k < nChannels; k++) {
                    buffer_count += sprintf( &file_buffer[buffer_count], "%+03.24f\t",CurrentNow[k]);  // 13 chars big !
                    if( buffer_count >= CHUNK_SIZE ) //if buffer is chucksize or bigger print the buffer to file
                    {
                        fwrite(file_buffer, buffer_count, 1, WriteCurrents_f);
                        buffer_count = 0 ;
                    }
                }
                //---> print endofline when formatting is on.
                buffer_count += sprintf( &file_buffer[buffer_count], "\n");
                //---> make shure the buffer is empy
                if( buffer_count > 0 ) fwrite( file_buffer, buffer_count, 1, WriteCurrents_f) ;
                //---> delete allocation
                delete[] file_buffer;
            }
            else {
                //---> Raw dump
                fwrite(CurrentNow, sizeof(float), nChannels, WriteCurrents_f);
            }
//            printf("no support yet\n");
//            exit(0);
        }
        if(WriteActivationVariables){
            //--->pretty print
            if(formatting){
                if (sStep == 0) {
                    fprintf(WriteActivationVariables_f, "nCells = %d\t, nCompartments = %d\t, nChannels = %d\t, nGates = %d\t, Simtime = %.4f\t, nSteps = %d\t, dt = %.8f\n", nCells, nComps, nChannels,nGates, dt * nSteps, nSteps, dt);
                    fprintf(WriteActivationVariables_f, "\n");
                }
                int buffer_count = 0;
                char *file_buffer = new char[CHUNK_SIZE + 64];
                buffer_count += sprintf( &file_buffer[buffer_count], "%d\t",sStep);
                for (uint32_t k = 0; k < nGates; k++) {
                    buffer_count += sprintf( &file_buffer[buffer_count], "%+03.32f\t",ysNow[k]);  // 13 chars big !
                    if( buffer_count >= CHUNK_SIZE ) //if buffer is chucksize or bigger print the buffer to file
                    {
                        fwrite(file_buffer, buffer_count, 1, WriteActivationVariables_f);
                        buffer_count = 0 ;
                    }
                }
                //---> print endofline when formatting is on.
                buffer_count += sprintf( &file_buffer[buffer_count], "\n");
                //---> make shure the buffer is empy
                if( buffer_count > 0 ) fwrite( file_buffer, buffer_count, 1, WriteActivationVariables_f) ;
                //---> delete allocation
                delete[] file_buffer;
            }
            else {
                //---> Raw dump
                fwrite(ysNow, sizeof(float), nGates, WriteActivationVariables_f);
            }
        }
        *timing += functimer.get_time();
        return true;
    }
    bool ok() const {
        if(type == UNSET) return false;
        return true;
    }

};

struct MetaDataMonitor{
    std::string name;
    FILE *MetaData_f;
    miniLogger log = miniLogger(LOG_DEFAULT,std::cout, "MetaDataMonitor");
    enum{
        UNSET,
        Full
    }type;

    MetaDataMonitor(){
        MetaData_f = nullptr;
    }
    bool open_file(int world_rank, bool MPI_inuse){
        char buff[256];
        if (MPI_inuse) {
            snprintf(buff, sizeof(buff), "%s_%d.json", name.c_str(), world_rank);
            name = name + "_" + std::to_string(world_rank) + ".json";
        } else {
            snprintf(buff, sizeof(buff), "%s.json", name.c_str());
            name = name + ".json";
        }
        if((MetaData_f=fopen(buff, "w"))== nullptr){
            log(LOG_ERR) << "Could not open file: " << buff << LOG_ENDL;
            return false;
        }
        return true;
    }
    bool CloseFiles(){
        if(MetaData_f == nullptr) {
            log(LOG_ERR) <<"Trying to close outputFiles but they where never opened" << LOG_ENDL;
            return false;
        }
        if(fclose(MetaData_f) != false) {
            log(LOG_ERR) <<"Could not close Metadatafile file: " << name << LOG_ENDL;
            return false;
        }
        return true;
    }
    bool ok() const {
        if(type == UNSET) return false;
        return true;
    }
};

struct SimRunInfo{
	//sim parameters
	float time;
	float timestep;
    int GPU_backend;
    bool GPU_Time_kernels;
    bool MPI_alltoallv_enable;
    bool OrderedBlock;
    int MPThreads;
	miniLogger log = miniLogger(LOG_DEFAULT,std::cout, "SimRunInfo_object");

	CellPopulationInfo population;
	ConnectivityInfo  conn_info;
	StimulusInfo stimulusInfo;
	OutputMonitor output_monitor;
    MetaDataMonitor meta_data_monitor;

    SimRunInfo(const int &LogLevel){
        log.set_Loglevel(LogLevel);
        time = 0.0f;
        timestep = 0.0f;
        GPU_backend = false;
        MPI_alltoallv_enable = true;
        OrderedBlock = false;
        MPThreads = 0;
    }

	SimRunInfo(const int &LogLevel, int PID){
        log.set_Loglevel(LogLevel);
        log.set_processID(PID);
        log.mpi();
		time = 0.0f;
		timestep = 0.0f;
        GPU_backend = false;
        MPI_alltoallv_enable = true;
        OrderedBlock = false;
        MPThreads = 0;
    }

    bool setTime(float newtime){
		if (newtime <= 0) {
            log(LOG_ERR) << "Timing needs to be a positive Float (" << newtime << ")"  ;
		    return false;
		}

		time = newtime;
		return true;
	}
	bool setTimestep(float newtimestep){
		if (!(newtimestep >= 0.0000001 && newtimestep <= 0.01)) return false;
		timestep = newtimestep;
		return true;
	}

	bool setConnectivityFull(float newwei){
		if (!std::isfinite(newwei)) return false;
		conn_info.type = ConnectivityInfo::CONSTANT;
		conn_info.constant.weight = newwei;
		return true;
	}
	bool setConnectivityREAD(float newwei, uint32_t maxden){
		if (!std::isfinite(maxden)) return false;
		conn_info.type = ConnectivityInfo::READ;
		conn_info.read.weight = newwei;
		conn_info.read.MaxDensity = maxden;
		return true;
	}
	bool setConnectivityRandomBinary(float weight, uint32_t density, uint32_t seed){
        if(density == 0){
            conn_info.type = ConnectivityInfo::NONE;
            return true;
        }
		conn_info.type = ConnectivityInfo::RANDOM_BINARY;
		conn_info.random_binary.weight = weight;
		conn_info.random_binary.density = density;
		conn_info.random_binary.random_seed = seed;
		return true;
	}
    bool setConnectivityGaussian(float weight, uint32_t density, float mean, float variance, uint32_t seed){
        std::cout << " " << weight << " " << density << " " << mean << " " << variance << " " << seed << "\n";
        if (!( std::isfinite(weight)) && !(std::isfinite(weight)) && !(std::isfinite(weight))) return false;
        std::cout << " " << weight << " " << density << " " << mean << " " << variance << " " << seed << "\n";
        if(density == 0){
            conn_info.type = ConnectivityInfo::NONE;
            return true;
        }
        conn_info.type = ConnectivityInfo::GAUSSIAN;
        conn_info.gaussian.weight = weight;
        conn_info.gaussian.density = density;
        conn_info.gaussian.mean = mean;
        conn_info.gaussian.variance = variance;
        conn_info.gaussian.random_seed = seed;
        return true;
    }
	bool setConnectivityNone(){
		conn_info.type = ConnectivityInfo::NONE;
		return true;
	}

    bool setDcPulseCell(uint32_t start_step, uint32_t end_step, float base,float pulse,uint32_t cellID){
        if (!(
                std::isfinite(start_step) && std::isfinite(end_step) && end_step > 0 && std::isfinite(base) && std::isfinite(pulse)
        )) return false;
        stimulusInfo.type = StimulusInfo::DCCURRENTPULSECELL;
        stimulusInfo.dcCurrentPulseCell.start_step = start_step;
        stimulusInfo.dcCurrentPulseCell.end_step = end_step;
        stimulusInfo.dcCurrentPulseCell.base = base;
        stimulusInfo.dcCurrentPulseCell.pulse  = pulse;
        stimulusInfo.dcCurrentPulseCell.Cell_id = cellID;
        return true;
    }
    bool setOrnsteinUhlenbeck(double oTheta, double oSigma, double oMu, double oAlpha, double dt, double sqrt_dt) {
        if (!(oTheta >= 0 && oSigma >= 0 && oMu >= 0 && oAlpha >= 0 && oAlpha <= 1)) return false;
        if (oAlpha != 0) {
            puts("no correlated noise support!");
            return false;
        }
        stimulusInfo.type = StimulusInfo::ORNSTEINUHLENBECK;
        stimulusInfo.ornsteinUhlenbeck.theta = oTheta;
        stimulusInfo.ornsteinUhlenbeck.sigma = oSigma;
        stimulusInfo.ornsteinUhlenbeck.mu = oMu;
        stimulusInfo.ornsteinUhlenbeck.alpha = oAlpha;
        stimulusInfo.ornsteinUhlenbeck.dt = dt;
        stimulusInfo.ornsteinUhlenbeck.sqrt_dt = sqrt_dt;
        return true;
    }
	bool setDcPulseModulo(uint32_t start_step, uint32_t end_step, float base,int32_t modulo, float modulo_scale){
		if (!(
			std::isfinite(start_step) && std::isfinite(end_step) && end_step > 0 && std::isfinite(base)
			&& modulo > 0 && std::isfinite(modulo_scale)
		)) return false;
		stimulusInfo.type = StimulusInfo::DCCURRENTPULSEMODULO;
		stimulusInfo.dcCurrentPulseModulo.base = base;
        stimulusInfo.dcCurrentPulseModulo.start_step = start_step;
        stimulusInfo.dcCurrentPulseModulo.end_step = end_step;
        stimulusInfo.dcCurrentPulseModulo.modulo = modulo;
        stimulusInfo.dcCurrentPulseModulo.modulo_scale = modulo_scale;
		return true;
	}
    bool setDcStepandHold(uint32_t start_step, uint32_t end_step,  uint32_t end_hold, float basecurrent, float stepcurrent, float holdcurrent){
        if (!(
                std::isfinite(start_step) && std::isfinite(end_step) && std::isfinite(end_hold) && end_step >= start_step && end_hold >= end_step && std::isfinite(basecurrent) && std::isfinite(stepcurrent) && std::isfinite(holdcurrent)
        )) return false;
        stimulusInfo.type = StimulusInfo::DCCURRENTSTEPANDHOLD;

        stimulusInfo.dcCurrentStepandHold.start_step = start_step;
        stimulusInfo.dcCurrentStepandHold.end_step  = end_step;
        stimulusInfo.dcCurrentStepandHold.end_hold = end_hold;
        stimulusInfo.dcCurrentStepandHold.basecurrent = basecurrent;
        stimulusInfo.dcCurrentStepandHold.stepcurrent = stepcurrent;
        stimulusInfo.dcCurrentStepandHold.holdcurrent = holdcurrent;
        return true;
    }
    bool setDcRampandHold(uint32_t start_ramp, uint32_t end_ramp,  uint32_t end_hold, float basecurrent, float rampcurrent, float holdcurrent){
        if (!(
                std::isfinite(start_ramp) && std::isfinite(end_ramp) && std::isfinite(end_hold) && end_ramp >= start_ramp && end_hold >= end_ramp && std::isfinite(basecurrent) && std::isfinite(rampcurrent) && std::isfinite(holdcurrent)
        )) return false;
        stimulusInfo.type = StimulusInfo::DCCURRENTRAMPANDHOLD;

        stimulusInfo.dcCurrentRampandHold.start_ramp = start_ramp;
        stimulusInfo.dcCurrentRampandHold.end_ramp  = end_ramp;
        stimulusInfo.dcCurrentRampandHold.end_hold = end_hold;
        stimulusInfo.dcCurrentRampandHold.basecurrent = basecurrent;
        stimulusInfo.dcCurrentRampandHold.rampcurrent = rampcurrent;
        stimulusInfo.dcCurrentRampandHold.holdcurrent = holdcurrent;
        return true;
    }

	bool ok(){
		//check for unset variables
		if(!(timestep != 0 && time != 0)){
		    printf("wrong population \n");
		    return false;
		}
        //check for inconsistent variables
        if(!(timestep != 0 && time != 0 && time >= timestep )){
            log(LOG_ERR) << "Simulation time ("<<time<<") should not be less than timestep("<<timestep<<")" << LOG_ENDL;
            return false;
        }
		//check if all structs are initialized
        if(!population.ok()){
            log(LOG_ERR) << "Population is not setup correctly" << LOG_ENDL;
		    return false;
		}
        if(!output_monitor.ok()){
            log(LOG_ERR) << "OutPutMonitor is not setup correctly" << LOG_ENDL;
            return false;
		}
        if(!meta_data_monitor.ok()){
            log(LOG_ERR) << "MetaDataMonitor is not setup correctly" << LOG_ENDL;
            return false;
        }
		if(!stimulusInfo.ok()) {
            log(LOG_ERR) << "StimulusInfo is not setup correctly" << LOG_ENDL;
		    return false;
		}
		if(!conn_info.ok()){
            log(LOG_ERR) << "ConnectionInfo is not setup correctly" << LOG_ENDL;
		    return false;
		}
		return true;
	}
};            //528byte

#endif
