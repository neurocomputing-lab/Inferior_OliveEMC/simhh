//
// Created by max on 24-03-21.
//
#include "GPU_kernels.h"
#include "initializations.h"
#include <curand_kernel.h>  // for random
#include <cstring>          // std::memcpy

#define DEBUG_GPU             0
#define DEBUG_GPU_EMUL        0

//stream 0 = Gap-juncition -> compartmentupdate -> copy voltages between gpu's -> copy voltage back for mpi.
//stream 1 = GateUpdate
//stream 2 = Copy state back to CPU
const int num_streams = 3;      //we get this amount of streams for every GPU!


//#ifdef USE_MPI
//    //cuda Error checking with MPI
//#define CUDA_CHECK_RETURN(value) {										\
//	cudaError_t _m_cudaStat = value;									\
//	if (_m_cudaStat != cudaSuccess) {									\
//        int world_rank;                                                          \
//        MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);                              \
//        char * name = (char*) malloc (MPI_MAX_PROCESSOR_NAME * sizeof(char));     \
//        int name_len;                                                            \
//        MPI_Get_processor_name(name, &name_len);                                 \
//		fprintf(stderr, "%3d %s: CUDA Error %s at line %d in file %s\n",	\
//             world_rank,name,cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);		\
//        printf("%3d %s: CUDA Error %s at line %d in file %s\n",		            \
//			 world_rank,name,cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);		\
//		if(value == 2) exit(2);                                         \
//		exit(1);														\
//	} }
//#else
    //cuda Error checking no MPI
#define CUDA_CHECK_RETURN(value) {										\
	cudaError_t _m_cudaStat = value;									\
	if (_m_cudaStat != cudaSuccess) {									\
		fprintf(stderr, " CUDA Error %s at line %d in file %s\n",	\
             cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);		\
        printf("CUDA Error %s at line %d in file %s\n",		            \
			 cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);		\
		if(value == 2) exit(2);                                         \
		exit(1);														\
	} }
//#endif



/*device functions */
namespace GPU_kernel_functions {
    struct eventmonitor_GPU{
        cudaEvent_t Igap_start;                           //CudaEvent for metadata collection
        cudaEvent_t Igap_stop;                            //CudaEvent for metadata collection
        cudaEvent_t Icalc_comp_start;                     //CudaEvent for metadata collection
        cudaEvent_t Icalc_comp_stop;                      //CudaEvent for metadata collection
        cudaEvent_t Icalc_gate_start;                     //CudaEvent for metadata collection
        cudaEvent_t Icalc_gate_stop;                      //CudaEvent for metadata collection
        cudaEvent_t p2p_copy_start;                     //CudaEvent for metadata collection
        cudaEvent_t p2p_copy_stop;                      //CudaEvent for metadata collection
        cudaEvent_t dev_copy_start;                     //CudaEvent for metadata collection
        cudaEvent_t dev_copy_stop;                      //CudaEvent for metadata collection

        void init() {
            CUDA_CHECK_RETURN(cudaEventCreate(&Igap_start));
            CUDA_CHECK_RETURN(cudaEventCreate(&Igap_stop));
            CUDA_CHECK_RETURN(cudaEventCreate(&Icalc_comp_start));
            CUDA_CHECK_RETURN(cudaEventCreate(&Icalc_comp_stop));
            CUDA_CHECK_RETURN(cudaEventCreate(&Icalc_gate_start));
            CUDA_CHECK_RETURN(cudaEventCreate(&Icalc_gate_stop));
            CUDA_CHECK_RETURN(cudaEventCreate(&p2p_copy_start));
            CUDA_CHECK_RETURN(cudaEventCreate(&p2p_copy_stop));
            CUDA_CHECK_RETURN(cudaEventCreate(&dev_copy_start));
            CUDA_CHECK_RETURN(cudaEventCreate(&dev_copy_stop));
        }
        float  getDevcopy() const{
            float milliseconds = 0;
            cudaEventElapsedTime(&milliseconds, dev_copy_start, dev_copy_stop);
            return milliseconds;
        }

        float  getP2pcopy() const{
            float milliseconds = 0;
            cudaEventElapsedTime(&milliseconds, p2p_copy_start, p2p_copy_stop);
            return milliseconds;
        }

        float  getGap() const{
            float milliseconds = 0;
            cudaEventElapsedTime(&milliseconds, Igap_start, Igap_stop);
            return milliseconds;
        }

        float  getComp() const{
            float milliseconds = 0;
            cudaEventElapsedTime(&milliseconds, Icalc_comp_start, Icalc_comp_stop);
            return milliseconds;
        }

        float getGate() const{
            float milliseconds = 0;
            cudaEventElapsedTime(&milliseconds, Icalc_gate_start, Icalc_gate_stop);
            return milliseconds;
        }
    };


    __device__ float fCustom_nieuw_a(const uint32_t fType, const float v,const GateConstStruct *gateConstStruct) {
        const float vDiff =  gateConstStruct->aX[1] - v;
        const float vDiff2 = gateConstStruct->aX[6] - v;
        const float z1 = gateConstStruct->aX[2] * vDiff;
        const float z2 = gateConstStruct->aX[5] * vDiff2;
        float num, denum;
        if(fType == 0){
            num = gateConstStruct->aX[5] * vDiff;
            denum = gateConstStruct->aX[0] * expf(z1) + gateConstStruct->aX[3];
        }else if(fType == 1){
            num = gateConstStruct->aX[8];
            denum = gateConstStruct->aX[0] * expf(z1) + gateConstStruct->aX[3] + gateConstStruct->aX[4] * expf(z2) + gateConstStruct->aX[7];
        }else if(fType == 2){
            num = gateConstStruct->aX[0] * expf(z1) + gateConstStruct->aX[3];
            denum = gateConstStruct->aX[4] * expf(z2) +gateConstStruct->aX[7];
        }else{
            return minSS(z1, gateConstStruct->aX[0]);
        }
        if(denum == 0) denum = 0.000000000000001;
        if (fType != 1) return num/denum +  gateConstStruct->aX[8];
        return num / denum;
    }
    __device__ float fCustom_nieuw_b(const uint32_t fType, const float v,const GateConstStruct *gateConstStruct) {
        const float bx0 =  gateConstStruct->bX[0];
        const float bx3 =  gateConstStruct->bX[3];
        const float bx4 =  gateConstStruct->bX[4];
        const float bx7 =  gateConstStruct->bX[7];
        const float bx8 =  gateConstStruct->bX[8];

        const float vDiff =  gateConstStruct->bX[1] - v;
        const float vDiff2 = gateConstStruct->bX[6] - v;
        const float z1 = gateConstStruct->bX[2] * vDiff;
        const float z2 = gateConstStruct->bX[5] * vDiff2;
        float num;
        float denum;
        if(fType == 0){
            num = gateConstStruct->bX[5] * vDiff;
            denum = bx0 * expf(z1) + bx3;
        }
        else if(fType == 1){
            num = bx8;
            denum = bx0 * expf(z1) + bx3 + bx4 * expf(z2) + bx7;
        }
        else if(fType == 2){
            num = bx0 * expf(z1) + bx3;
            denum = bx4 * expf(z2) + bx7;
        }else {
            return minSS(z1, bx0);
        }
        if(denum == 0) denum = 0.000000000000001;
        if (fType != 1) return num/denum + bx8;
        return num / denum;
    }

    /* flexHH terminology */
    __device__ float fn_iInteract(float gInt, const uint32_t Identifier, const float V, const float gp, float *gpNext, float *vsNow){
        float iCompPrev = 0;
        float iCompNext = 0;

        if(ORDEREDBLOCK){
            if (Identifier == 0 || Identifier == 1)
                iCompNext = (V - vsNow[BLOCKSIZE]) * gInt / (1 - *gpNext);
            if (Identifier == 1 || Identifier == 2)
                iCompPrev = (V - vsNow[-BLOCKSIZE]) * gInt / gp;
        }
        else{
            if (Identifier == 0 || Identifier == 1)
                iCompNext = (V - vsNow[1]) * gInt / (1 - *gpNext);
            if (Identifier == 1 || Identifier == 2)
                iCompPrev = (V - vsNow[-1]) * gInt / gp;
        }
        return iCompNext + iCompPrev;
    }
    __device__ float fn_ileak(const float gLeak, const float V, const float Vleak){
        return  gLeak * (V - Vleak);
    }
    __device__ float fn_iApp(const uint32_t step, const StimulusInfo &stimulusInfo, const bool enable, const uint32_t cellid, float *ou_noise, curandState_t *randgen, uint32_t local_cellid, float rand_glob) {
        if (enable) {
            if (stimulusInfo.type == StimulusInfo::DCCURRENTPULSEMODULO) {
                if ((step >= stimulusInfo.dcCurrentPulseModulo.start_step) & (step < stimulusInfo.dcCurrentPulseModulo.end_step)) {
                    return (stimulusInfo.dcCurrentPulseModulo.base + stimulusInfo.dcCurrentPulseModulo.modulo_scale * ((cellid) % (stimulusInfo.dcCurrentPulseModulo.modulo)));
                }
            } else if (stimulusInfo.type == StimulusInfo::DCCURRENTSTEPANDHOLD) {
                if ((step >= stimulusInfo.dcCurrentStepandHold.start_step) & (step < stimulusInfo.dcCurrentStepandHold.end_step)) {
                    return stimulusInfo.dcCurrentStepandHold.stepcurrent;
                }
                else if  ((step >= stimulusInfo.dcCurrentStepandHold.end_step) & (step < stimulusInfo.dcCurrentStepandHold.end_hold)) {
                    return stimulusInfo.dcCurrentStepandHold.holdcurrent;
                }else{
                    return stimulusInfo.dcCurrentStepandHold.basecurrent;
                }
            } else if (stimulusInfo.type == StimulusInfo::DCCURRENTRAMPANDHOLD){
                if ((step >= stimulusInfo.dcCurrentRampandHold.start_ramp) & (step < stimulusInfo.dcCurrentRampandHold.end_ramp)) {
                    return stimulusInfo.dcCurrentRampandHold.rampcurrent - ((stimulusInfo.dcCurrentRampandHold.rampcurrent - stimulusInfo.dcCurrentRampandHold.basecurrent)/((float) ((stimulusInfo.dcCurrentRampandHold.end_ramp - stimulusInfo.dcCurrentRampandHold.start_ramp) * step)));
                } else if ((step >= stimulusInfo.dcCurrentRampandHold.end_ramp) & (step < stimulusInfo.dcCurrentRampandHold.end_hold)) {
                    return stimulusInfo.dcCurrentStepandHold.holdcurrent;
                } else {
                    return stimulusInfo.dcCurrentStepandHold.basecurrent;
                }
            } else if (stimulusInfo.type == StimulusInfo::DCCURRENTPULSECELL) {
                if (stimulusInfo.dcCurrentPulseCell.Cell_id == cellid) {
                    if ((step >= stimulusInfo.dcCurrentPulseCell.start_step) & (step < stimulusInfo.dcCurrentPulseCell.end_step)) {
                        return stimulusInfo.dcCurrentPulseCell.pulse;
                    }
                } else {
                    return stimulusInfo.dcCurrentPulseCell.base;
                }
            } else if (stimulusInfo.type == StimulusInfo::ORNSTEINUHLENBECK) {
                float theta   = stimulusInfo.ornsteinUhlenbeck.theta;
                float sigma   = stimulusInfo.ornsteinUhlenbeck.sigma;
                float mu      = stimulusInfo.ornsteinUhlenbeck.mu;
                float alpha   = stimulusInfo.ornsteinUhlenbeck.alpha;
                float dt      = stimulusInfo.ornsteinUhlenbeck.dt;
                float sqrt_dt = stimulusInfo.ornsteinUhlenbeck.sqrt_dt;
                float Iapp_global = alpha*sigma*sqrt_dt*rand_glob;
                float random_normal = curand_normal(&randgen[local_cellid]);
                ou_noise[local_cellid] += theta*(mu-ou_noise[local_cellid])*dt + (1-alpha)*sigma*sqrt_dt*random_normal + alpha*Iapp_global;
                return ou_noise[local_cellid];
            }
        }
        return 0;
    }
    __device__ float fn_UpdateYS(const GateConstStruct *gateConstStruct, const float V, const float dt, const float YSN, const float CalciumLevel) {
        float z1;
        const float beta = fCustom_nieuw_b(gateConstStruct->bFtype & mask1,V,gateConstStruct);
        if ((gateConstStruct->aFtype & mask1) == 3) {        //vind dit niet een mooie oplossing tbh
            z1 = CalciumLevel;
        } else {
            z1 = V;
        }
        const float alpha = fCustom_nieuw_a(gateConstStruct->aFtype & mask1,z1,gateConstStruct);
    #if DEBUG_GPU == 1
        float dydt = 0;
        if (beta && (gateConstStruct->aFtype & mask2) == 4) {
            dydt = (alpha - YSN) / beta;
        } else {
            dydt = (1 - YSN) * alpha - YSN * beta;
        }
        const float dit = YSN + dt * dydt;
    //    if(isinf(dit)) {printf("dit %f YSN %f alpha %f beta %f V %f dydt %f , Calcium %f\n",dit,YSN,alpha,beta,V,dydt,CalciumLevel);}
    //    if(isnan(dit)) {printf("dit %f YSN %f alpha %f beta %f V %f dydt %f , Calcium %f\n",dit,YSN,alpha,beta,V,dydt,CalciumLevel);}
        return dit;
    #else
        if (beta && (gateConstStruct->aFtype & mask2) == 4) {
            return YSN + dt*((alpha - YSN) / beta);
        } else {
            return YSN + dt *((1 - YSN) * alpha - YSN * beta);
        }
        return 0;
    #endif
    }
    __device__ float fn_UpdateCalc(const float iCa,const GateConstStruct *gateConstStruct,const float V, const float dt, const float YSN){
        const float beta = fCustom_nieuw_b(gateConstStruct->bFtype & mask1,V,gateConstStruct);
        const float alpha = fCustom_nieuw_a(gateConstStruct->aFtype & mask1,V,gateConstStruct);
    #if DEBUG_GPU == 1
        const float dydt = (-iCa) * alpha - YSN * beta;                    //calcium update
        const float dit = YSN + dt * dydt;
        if(isinf(dit)) {printf("dit %f celloffset %d YSN %f alpha %f beta %f V %f dydt %f iCa %f\n",dit,10,YSN,alpha,beta,V,dydt,iCa);}
        if(isnan(dit)) {printf("dit %f celloffset %d YSN %f alpha %f beta %f V %f dydt %f iCa %f\n",dit,10,YSN,alpha,beta,V,dydt,iCa);}
        return dit;
    #else
        return YSN + dt*((-iCa) * alpha - YSN * beta);                    //calcium update
    #endif
    }
    __device__ float fn_iChannels(int temp, float *currentNext, const CompartmentConstStruct *compartment_variables,const float *ysNow,const float V, const float dt,const float *calcNow, float *calcNext,const float *g, const float *Vchan, float *ynNext){
        float iChannels = 0;
        float iCa = 0;
        uint32_t gate_plus = 0;

        for(int i = 0; i < compartment_variables->nChannels; i++){
            float Yprod = 1;
#if ORDEREDBLOCK == 1
            const float gc     = g[i*BLOCKSIZE];
            const float Vchanc = Vchan[i*BLOCKSIZE];
#else
            const float gc     = g[i];
            const float Vchanc = Vchan[i];
#endif

            for(int j = 0; j < compartment_variables->Channel[i].nGates; j++){

#if ORDEREDBLOCK == 1
               float YprodN = ysNow[gate_plus * BLOCKSIZE];
#else
               float YprodN = ysNow[gate_plus];
#endif
                if ((compartment_variables->Channel[i].Gate[j].bFtype & mask2) == 12) {
                    YprodN = fCustom_nieuw_b(compartment_variables->Channel[i].Gate[j].bFtype & mask1,V,&compartment_variables->Channel[i].Gate[j]);
                }
                YprodN = powf(YprodN,compartment_variables->Channel[i].Gate[j].p);
                Yprod = Yprod * YprodN;

#if DEBUG_GPU == 1
                if(isnan(YprodN)) printf("YprodN i(%d) is nan: p %f, Ysnow %f, chicheck %d \n",i+temp,compartment_variables->Channel[i].Gate[j].p,ysNow[gate_plus], (compartment_variables->Channel[i].Gate[j].bFtype & mask2));
#endif

//#if ORDEREDBLOCK == 1
//                ynNext[gate_plus * BLOCKSIZE] = fn_UpdateYS(&compartment_variables->Channel[i].Gate[j], V, dt, ysNow[gate_plus * BLOCKSIZE], calcNow[0]);
//#endif

                gate_plus++;
            }
            float iChannel = Yprod;
            iChannel =  gc * (V - Vchanc) * Yprod;
            if(compartment_variables->Channel[i].is_ca2plus_channel) {
                iCa += iChannel;
              }
            currentNext[i] = iChannel;
            iChannels += iChannel;
#if DEBUG_GPU == 1
            if(isnan(iChannel)) printf("iChannel is nan: Yprod = %f , gc = %f, V = %f, Vchanc = %f\n",Yprod,gc,V,Vchan);
#endif
        }



        //--->update Calcium concentration
        if(compartment_variables->enable_CalciumConcentration){
            float calc_conc = fn_UpdateCalc(iCa, &compartment_variables->CalciumConcentration,V,dt,calcNow[0]);
            calcNext[0] = calc_conc;
        }

        return iChannels;
    }

    /*initialization kernels */
    __global__ void initGPU_cellsize(const uint32_t nCells_local, float * ou_noise, curandState_t * randgen,const uint32_t cellid_offset) {
        const uint32_t cellid   = blockIdx.x * blockDim.x + threadIdx.x;
        if(cellid >= nCells_local) return; //todo decent padding to avoid this
        curand_init(1234 + cellid + cellid_offset, 0, 0, &randgen[cellid]);
        ou_noise[cellid] = 0;
    }

    /*kernel functions gap functions*/
    __global__ void IGAP_calc_sparse_condu         (const uint32_t nCells_local, const uint32_t nCells,const float weight, float* iGap, const float *vsGap, const CellConstStruct* neighConns,int cellid_offset){
        const uint32_t cellid = blockIdx.x * (blockDim.x/GAP_THREADS_PER_CELL) + (threadIdx.x/GAP_THREADS_PER_CELL);
        if(cellid >= nCells_local) return;
        const float VSN = vsGap[cellid + cellid_offset];
        const uint32_t neighcountt = neighConns[cellid].NeighCount;
        const uint64_t *ids =  neighConns[cellid].NeighIds;
        const float    *neighCondu = neighConns[cellid].NeighCondu;
        float fAcc = 0;
        float vAcc = 0;
        float temp = 0;
        for (uint32_t x = threadIdx.x%GAP_THREADS_PER_CELL; x < neighcountt; x+=GAP_THREADS_PER_CELL) {
            const float vDiff = VSN - vsGap[ids[x]];
            const float new_weight = neighCondu[x];
            fAcc += vDiff * new_weight * expf(vDiff * vDiff * (-1.0f / 100.0f));
            vAcc += new_weight * vDiff;
        }
        temp = 0.8f * fAcc + 0.2f * vAcc;
#if GAP_THREADS_PER_CELL > 16
        temp += __shfl_down_sync(0xffffffff, temp, 16);
#endif
#if GAP_THREADS_PER_CELL > 8
        temp += __shfl_down_sync(0xffffffff, temp, 8);
#endif
#if GAP_THREADS_PER_CELL > 4
        temp += __shfl_down_sync(0xffffffff, temp, 4);
#endif
#if GAP_THREADS_PER_CELL > 2
        temp += __shfl_down_sync(0xffffffff, temp, 2);
#endif
#if GAP_THREADS_PER_CELL > 1
        temp += __shfl_down_sync(0xffffffff, temp, 1);
#endif
        if(threadIdx.x%GAP_THREADS_PER_CELL == 0) iGap[cellid] = temp;
    }
    __global__ void IGAP_calc_sparse            (const uint32_t nCells_local, const uint32_t nCells,const float weight, float* iGap, const float *vsGap, const CellConstStruct* neighConns,int cellid_offset){
        const uint32_t cellid = blockIdx.x * (blockDim.x/GAP_THREADS_PER_CELL) + (threadIdx.x/GAP_THREADS_PER_CELL);
        if(cellid >= nCells_local) return;
        const float VSN = vsGap[cellid + cellid_offset];
        const uint32_t neighcountt = neighConns[cellid].NeighCount;
        const uint64_t *ids =  neighConns[cellid].NeighIds;
        float fAcc = 0;
        float vAcc = 0;
        float temp = 0;

/////exponential gaps
//        for (uint32_t x = threadIdx.x%GAP_THREADS_PER_CELL; x < neighcountt; x+=GAP_THREADS_PER_CELL) {
//            const float vDiff = VSN - vsGap[ids[x]];
//            fAcc += vDiff  * weight * expf(vDiff * vDiff * (-0.01f));
//            vAcc += weight * vDiff;
//        }

///linear gaps
        for (uint32_t x = threadIdx.x%GAP_THREADS_PER_CELL; x < neighcountt; x+=GAP_THREADS_PER_CELL) {
            const float vDiff = VSN - vsGap[ids[x]];
            vAcc += weight * vDiff;
        }

        temp = 0.8f * fAcc + 0.2f * vAcc;
    #if GAP_THREADS_PER_CELL > 16
        temp += __shfl_down_sync(0xffffffff, temp, 16);
    #endif
    #if GAP_THREADS_PER_CELL > 8
        temp += __shfl_down_sync(0xffffffff, temp, 8);
    #endif
    #if GAP_THREADS_PER_CELL > 4
        temp += __shfl_down_sync(0xffffffff, temp, 4);
    #endif
    #if GAP_THREADS_PER_CELL > 2
        temp += __shfl_down_sync(0xffffffff, temp, 2);
    #endif
    #if GAP_THREADS_PER_CELL > 1
        temp += __shfl_down_sync(0xffffffff, temp, 1);
    #endif
        if(threadIdx.x%GAP_THREADS_PER_CELL == 0) iGap[cellid] = temp;
    }
    __global__ void IGAP_calc_full              (const uint32_t nCells_local, const uint32_t nCells,const float weight, float* iGap, const float *vsGap,                                   int cellid_offset){
        const uint32_t cellid = blockIdx.x * (blockDim.x/GAP_THREADS_PER_CELL) + (threadIdx.x/GAP_THREADS_PER_CELL);
        if(cellid >= nCells_local) return;
        const float VSN = vsGap[cellid + cellid_offset];
        float temp = 0;
        float fAcc = 0;
        float vAcc = 0;
        for (uint32_t x = threadIdx.x%GAP_THREADS_PER_CELL; x < nCells; x+=GAP_THREADS_PER_CELL) {
            if(x>= nCells) return;
            const float vDiff = VSN - vsGap[x];
            fAcc += vDiff * weight * expf(vDiff * vDiff * (-1.0f / 100.0f));
            vAcc += weight * vDiff;
        }
        temp = 0.8f * fAcc + 0.2f * vAcc;
    #if GAP_THREADS_PER_CELL > 16
        temp += __shfl_down_sync(0xffffffff, temp, 16);
    #endif
    #if GAP_THREADS_PER_CELL > 8
        temp += __shfl_down_sync(0xffffffff, temp, 8);
    #endif
    #if GAP_THREADS_PER_CELL > 4
        temp += __shfl_down_sync(0xffffffff, temp, 4);
    #endif
    #if GAP_THREADS_PER_CELL > 2
        temp += __shfl_down_sync(0xffffffff, temp, 2);
    #endif
    #if GAP_THREADS_PER_CELL > 1
        temp += __shfl_down_sync(0xffffffff, temp, 1);
    #endif
        if(threadIdx.x%GAP_THREADS_PER_CELL == 0)iGap[cellid] = temp;
    }
    __global__ void CompartmentUpdate( //todo stimulus info beter en clean up the function plz
            uint32_t NO_of_Compartments,    float dt,               uint32_t step,  CompartmentConstStruct *compartmentConstStruct,
            float *vsNow,                   float *ysNow,           float *ysNext,float *vsNext,  float *calcNow,
            float *calcNext, float *currentNext,                float *Igap,            float *vsGAP,   const uint32_t cellid_offset,
            uint32_t *CellID_match_compID,  uint32_t *CompartmentIndexes,           uint32_t *ChannelIndexes,
            uint32_t *GateIndexes,          float *G_Channels,      float *Inverpotential_Channels, float *PassiveLeakConductivity, float *G_int,
            const StimulusInfo stimulusInfo,
            float *ou_noise, curandState_t *randgen, float rand_glob)
        {
            const uint32_t compartment_No   = blockIdx.x * blockDim.x + threadIdx.x;
            if(compartment_No >= NO_of_Compartments) return; //todo decent padding to avoid this

            const float V                   = vsNow[compartment_No];
            const uint32_t cellid           = CellID_match_compID[compartment_No];
            const uint32_t compartmentIndex = CompartmentIndexes[compartment_No];
            const uint32_t ChannelIndex     = ChannelIndexes[compartment_No];
            const uint32_t gate_index       = GateIndexes[compartment_No];

            const CompartmentConstStruct *compartment_variables = &compartmentConstStruct[compartmentIndex];

            //determine if we are a first comparment. (this should change :D \\todo)
            bool first_compartment = false;
            if (compartment_variables->identifier == 0 || compartment_variables->identifier == 3)
                first_compartment = true;

            //--->Ileak
            const float iLeak =  fn_ileak(compartment_variables->gLeak,V,compartment_variables->vLeak);
            //--->Interaction Current
            const float iInteract = fn_iInteract(G_int[cellid],compartment_variables->identifier,V,compartment_variables->gp,&compartmentConstStruct[compartmentIndex+1].gp,&vsNow[compartment_No]);
            //--->Gap Connection Current
            float iGap = 0; //todo
            if(first_compartment) iGap = Igap[cellid];      //todo ! (vrij simple gwn aanpassen naar compartment idexering !

            //--->Application Current
            const float iApp = fn_iApp(step, stimulusInfo, first_compartment, cellid + cellid_offset, ou_noise, randgen, cellid, rand_glob);
            //--->Channel current

            const float iChannels =  fn_iChannels(ChannelIndex,&currentNext[ChannelIndex],  compartment_variables,&ysNow[gate_index],V,dt,&calcNow[compartment_No],&calcNext[compartment_No],  &G_Channels[ChannelIndex], &Inverpotential_Channels[ChannelIndex],&ysNext[gate_index]);
            //--->Update potentials

//            const float output = V + dt * (( - iGap  - iInteract - iLeak + iApp) * compartment_variables->S);

            const float output = V + dt * (( - iGap -iChannels - iInteract - iLeak + iApp) * compartment_variables->S);
            vsNext[compartment_No] = output;
            //change to a comparment style sharing plz "D
            if (first_compartment) {
                vsGAP[cellid] = output;
            }
            /*
            debug option
            if(!compartment_No)
                printf("%d %d %d %+03.24f %+03.24f %+03.24f %+03.24f %+03.24f %+03.24f %+03.24f\n",step,compartment_No,compartmentIndex,iLeak ,iChannels, iApp,iGap,iInteract,output, V);
             */ //debug options
    }
    __global__ void GateUpate(uint32_t no_gates, float dt, const GateConstStruct *gateConstStruct, float *vsNow, float *ysNow, float *ysNext,float *calcNow, uint32_t *Comp_index_vs_gates, uint32_t *GateNO_vs_GateIndex,uint32_t celloffset) {
        __shared__ float tempbuf1[BLOCKSIZE];
        float *ysnow = tempbuf1;
        uint32_t Gate_ID = blockIdx.x * blockDim.x + threadIdx.x;
        if (Gate_ID >= no_gates) return;
        ysnow[threadIdx.x] = ysNow[Gate_ID];
        const uint32_t Compartment_NO = Comp_index_vs_gates[Gate_ID];
        const uint32_t Gate_index = GateNO_vs_GateIndex[Gate_ID];
//        printf("%d\t\t%d\t\t%d \n", Gate_ID, Gate_index, Compartment_NO);
        ysNext[Gate_ID] = fn_UpdateYS(&gateConstStruct[Gate_index], vsNow[Compartment_NO], dt, ysnow[threadIdx.x],  calcNow[Compartment_NO]);
    }
}

bool RunTimestep_gpu(size_t step, SimRunInfo &sim, Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l, process_info &ProcessInfo){
    using namespace GPU_kernel_functions;

    INIT_LOGP(LOG_DEFAULT,ProcessInfo.world_rank);
    for (size_t i = 0; i < ProcessInfo.GPU; i++) {

#if DEBUG_GPU_EMUL == 0  //to emulate multiple gpu's on a single gpu :D
        CUDA_CHECK_RETURN(cudaSetDevice(i));  //set device
#endif
        //--> Streams
        auto *streams = (cudaStream_t*)ProcessInfo.GPUstreams;
        streams = &streams[num_streams*i];
        //event monitor
        auto *temp = (eventmonitor_GPU*)ProcessInfo.GPUevents;
        eventmonitor_GPU monitor = temp[i];


        //--> set the sizes for the blocks can we put this in process info plz :D to much calculations..
        const dim3 blockDim(BLOCKSIZE);
        const dim3 gridDim_cells(((NetworkConst_l[i].GPU_numbers[i].nCells_gpu              + (blockDim.x - 1)) / blockDim.x));
        const dim3 gridDim_compartment(((NetworkConst_l[i].GPU_numbers[i].nCompartments_gpu + (blockDim.x - 1)) / blockDim.x));
        const dim3 gridDim_gates(((NetworkConst_l[i].GPU_numbers[i].nGates_gpu              + (blockDim.x - 1)) / (blockDim.x)));

        //-> for the gap junctions
        const dim3 gridDim_gap_sparse(((NetworkConst_l[i].GPU_numbers[i].nCells_gpu / GAP_THREADS_PER_BLOCK) + 1) * GAP_THREADS_PER_CELL);

#if DEBUG_GPU == 1
        CUDA_CHECK_RETURN(cudaDeviceSynchronize());
#endif

        //---> Igap calculations
        {
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.Igap_start, streams[0]);
            if (sim.conn_info.type == ConnectivityInfo::CONSTANT) {
                IGAP_calc_full <<< gridDim_gap_sparse, GAP_THREADS_PER_BLOCK,0,streams[0]>>>(
                        NetworkConst_l[i].GPU_numbers[i].nCells_gpu,
                        NetworkConst_l[i].nCells,
                        NetworkConst_l[i].weight,
                        NetworkState_l[i].Igap,
                        NetworkState_l[i].VS_GAP,
                        NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu
                );
            }    //fully connected
            else if (sim.conn_info.type != ConnectivityInfo::NONE) {
                if(sim.conn_info.use_condu){
                    IGAP_calc_sparse_condu<<< gridDim_gap_sparse, GAP_THREADS_PER_BLOCK, 0, streams[0]>>>(
                            NetworkConst_l[i].GPU_numbers[i].nCells_gpu,
                            NetworkConst_l[i].nCells,
                            NetworkConst_l[i].weight,
                            NetworkState_l[i].Igap,
                            NetworkState_l[i].VS_GAP,                       //full pull vector
                            NetworkConst_l[i].CellPopulation,
                            NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu
                    );
                }
                else {
                    IGAP_calc_sparse <<< gridDim_gap_sparse, GAP_THREADS_PER_BLOCK, 0, streams[0]>>>(
                            NetworkConst_l[i].GPU_numbers[i].nCells_gpu,
                            NetworkConst_l[i].nCells,
                            NetworkConst_l[i].weight,
                            NetworkState_l[i].Igap,
                            NetworkState_l[i].VS_GAP,                       //full pull vector
                            NetworkConst_l[i].CellPopulation,
                            NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu
                    );
                }
            }    //partially connected else not connected (so we do nothing "d)
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.Igap_stop, streams[0]);

        }

#if DEBUG_GPU == 1
        CUDA_CHECK_RETURN(cudaDeviceSynchronize());
#endif

        //---> Compartmentupdate
        {
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.Icalc_comp_start, streams[0]);
            CompartmentUpdate <<<gridDim_compartment, blockDim,0,streams[0]>>>(
                    NetworkConst_l[i].GPU_numbers[i].nCompartments_gpu,
                    sim.timestep,
                    step,
                    NetworkConst_l[i].compartment,
                    NetworkState_l[i].vsNow,
                    NetworkState_l[i].ysNow,
                    NetworkState_l[i].ysNext,
                    NetworkState_l[i].vsNext,
                    NetworkState_l[i].calcNow,
                    NetworkState_l[i].calcNext,
                    NetworkState_l[i].currentsNext,
                    NetworkState_l[i].Igap,
                    &NetworkState_l[i].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu], //only to store new results in
                    NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu,
                    NetworkConst_l[i].CellID_match_compID,
                    NetworkConst_l[i].CompartmentIndex,
                    NetworkConst_l[i].ChannelIndex,
                    NetworkConst_l[i].GateIndex,
                    NetworkConst_l[i].G_Channels,
                    NetworkConst_l[i].Inverpotential_Channels,
                    NetworkConst_l[i].PassiveLeakConductivity,
                    NetworkConst_l[i].G_int,
                    sim.stimulusInfo,
                    NetworkState_l[i].ou_noise,
                    (curandStateXORWOW_t *) NetworkState_l[i].rand_state,
                    NetworkState_l[i].rng->GetF()
            );
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.Icalc_comp_stop, streams[0]);
        }

#if DEBUG_GPU == 1
        CUDA_CHECK_RETURN(cudaDeviceSynchronize());
#endif

        //---> Gateupdate probably we can move this to after the mpi kernel ?
        {
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.Icalc_gate_start, streams[1]);
            GateUpate <<< gridDim_gates, blockDim, blockDim.x * 2 * sizeof(float) + 2 * sizeof(GateConstStruct),streams[1] >>>(
                    NetworkConst_l[i].GPU_numbers[i].nGates_gpu,             //uint32_t no_gates,
                    sim.timestep,                           //float dt
                    NetworkConst_l[i].Gate,                 //const GateConstStruct *gateConstStruct
                    NetworkState_l[i].vsNow,                //float *vsNow
                    NetworkState_l[i].ysNow,                //float *ysNow
                    NetworkState_l[i].ysNext,               //float *ysNext
                    NetworkState_l[i].calcNow,              //float *calcNow
                    NetworkConst_l[i].Comp_index_vs_gates,  //uint32_t *Comp_index_vs_gates
                    NetworkConst_l[i].GateNO_vs_GateIndex,  //uint32_t *GateNO_vs_GateIndex
                    NetworkConst_l[i].CellID_Offset         //uint32_t celloffset
            );
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.Icalc_gate_stop, streams[1]);
        }

#if DEBUG_GPU == 1
        CUDA_CHECK_RETURN(cudaDeviceSynchronize());
#endif

        //-->share between GPU's the state;
        {
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.p2p_copy_start, streams[0]);
            for(int remoteGPU = 0; remoteGPU < ProcessInfo.GPU; remoteGPU++) {
                if(remoteGPU != i) {
#if DEBUG_GPU_EMUL
                    CUDA_CHECK_RETURN(cudaMemcpyAsync(&NetworkState_l[remoteGPU].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                            &NetworkState_l[i].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                            NetworkConst_l[i].GPU_numbers[i].nCells_gpu * sizeof(float),
                                                            cudaMemcpyDeviceToDevice,
                                                            streams[0]));
#else
                    CUDA_CHECK_RETURN(cudaMemcpyPeerAsync(&NetworkState_l[remoteGPU].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                          remoteGPU,
                                                          &NetworkState_l[i].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                          i,
                                                          NetworkConst_l[i].GPU_numbers[i].nCells_gpu * sizeof(float),
                                                          streams[0]));
#endif
                }
            }
            if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.p2p_copy_stop, streams[0]);
        }

#if DEBUG_GPU == 1
        CUDA_CHECK_RETURN(cudaDeviceSynchronize());
#endif

        //--> copy back to the Device. todo check if we need to print this timestep or not!
        {
            if (sim.output_monitor.type != OutputMonitor::NONE) {
                if (((step) % sim.output_monitor.timestep_save_interval == 0)) {
                    if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.dev_copy_start, streams[2]);

                    if (sim.output_monitor.WriteCompartPotentials) CUDA_CHECK_RETURN(
                            cudaMemcpyAsync(NetworkState_l[i].hostVs, NetworkState_l[i].vsNow, NetworkConst_l[i].GPU_numbers[i].nCompartments_gpu * sizeof(float), cudaMemcpyDeviceToHost, streams[2]));
                    if (sim.output_monitor.WriteCalciumLevels) CUDA_CHECK_RETURN(
                            cudaMemcpyAsync(NetworkState_l[i].hostCalc, NetworkState_l[i].calcNow, NetworkConst_l[i].GPU_numbers[i].nCompartments_gpu * sizeof(float), cudaMemcpyDeviceToHost, streams[2]));
                    if (sim.output_monitor.WriteCurrents) CUDA_CHECK_RETURN(
                            cudaMemcpyAsync(NetworkState_l[i].hostCurrents, NetworkState_l[i].currentsNow, NetworkConst_l[i].GPU_numbers[i].nChannels_gpu * sizeof(float), cudaMemcpyDeviceToHost, streams[2]));
                    if (sim.output_monitor.WriteActivationVariables) CUDA_CHECK_RETURN(
                            cudaMemcpyAsync(NetworkState_l[i].hostYs, NetworkState_l[i].ysNow, NetworkConst_l[i].GPU_numbers[i].nGates_gpu * sizeof(float), cudaMemcpyDeviceToHost, streams[2]));

                    if(ProcessInfo.MonitorGPUkernels)cudaEventRecord(monitor.dev_copy_stop, streams[2]);
                }
            }
        }
    }
    return true;
}

bool setdevice(int ID){
#if DEBUG_GPU_EMUL == 0  //to emulate multiple gpu's on a single gpu :D
    CUDA_CHECK_RETURN(cudaSetDevice(ID));  //set device
#endif
    return true;
}


bool fn_copy_vsgap_to_host_gpu(float *VS_GAP_host, float *VS_GAP_dev, uint32_t size, uint32_t offset,process_info *ProcessInfo){
    auto *streams = (cudaStream_t*)ProcessInfo->GPUstreams;
    streams = &streams[num_streams*0];
    CUDA_CHECK_RETURN(cudaMemcpyAsync(&VS_GAP_host[offset],       &VS_GAP_dev[offset],        size * sizeof(float)    , cudaMemcpyDeviceToHost,streams[0])); //can this please be async :d
    return true;
}
bool fn_copy_vsgap_to_device_gpu(float *VS_GAP_host, float *VS_GAP_dev, uint32_t size,process_info *ProcessInfo){
    auto *streams = (cudaStream_t*)ProcessInfo->GPUstreams;
    streams = &streams[num_streams*0];
    CUDA_CHECK_RETURN(cudaMemcpyAsync(VS_GAP_dev,VS_GAP_host,        size * sizeof(float)    , cudaMemcpyHostToDevice,streams[0]));
    return true;
}



bool fn_copy_back_dev_to_host_gpu(SimRunInfo &sim, Network_state &NetworkState_l,NetworkConstStruct &NetworkConst_l, GPU_number_obj &GPUno){
    if (sim.output_monitor.WriteCompartPotentials)
        CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_l.hostVs,       NetworkState_l.vsNow,        GPUno.nCompartments_gpu * sizeof(float)    , cudaMemcpyDeviceToHost));
    if (sim.output_monitor.WriteCalciumLevels)
        CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_l.hostCalc,     NetworkState_l.calcNow,      GPUno.nCompartments_gpu * sizeof(float)    , cudaMemcpyDeviceToHost));
    if (sim.output_monitor.WriteCurrents)
        CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_l.hostCurrents, NetworkState_l.currentsNow,  GPUno.nChannels_gpu * sizeof(float)        , cudaMemcpyDeviceToHost));
    if (sim.output_monitor.WriteActivationVariables)
        CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_l.hostYs,       NetworkState_l.ysNow,        GPUno.nGates_gpu * sizeof(float)           , cudaMemcpyDeviceToHost));
    return true;
}

Network_state * fn_init_gpu_Backend_state(SimRunInfo &sim,process_info &ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l) {
    INIT_LOGP(LOG_DEFAULT, ProcessInfo.world_rank);

    auto NetworkState_int = new Network_state[ProcessInfo.GPU];
    std::memcpy(&NetworkState_int[0], NetworkState_l, sizeof(Network_state));

    //initialize memory
    {
        //remember values
        uint32_t compartment_offset = 0;
        uint32_t channel_offset = 0;
        uint32_t gate_offset = 0;

        log(LOG_DEBUG, 0) << "initialize memory" << LOG_ENDL;
        for (size_t i = 0; i < ProcessInfo.GPU; i++) {
#if DEBUG_GPU_EMUL == 0
            CUDA_CHECK_RETURN(cudaSetDevice(i));
#endif

            //--> allocate on the targeted GPU and save the pointers
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].vsNow, NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].vsNext, NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].calcNow, NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].calcNext, NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].currentsNow, NetworkConst_l->GPU_numbers[i].nChannels_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].currentsNext, NetworkConst_l->GPU_numbers[i].nChannels_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].ysNow, NetworkConst_l->GPU_numbers[i].nGates_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].ysNext, NetworkConst_l->GPU_numbers[i].nGates_gpu * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].VS_GAP, NetworkConst_l->nCells * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].VS_GAP_N, NetworkConst_l->nCells * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].Igap, NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float)));

            //copy the initial states to the correct buffers
            log(LOG_DEBUG, 0) << "populate memory with defaults" << LOG_ENDL;
            CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_int[i].vsNow,        &NetworkState_int[0].hostVs[compartment_offset], NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_int[i].calcNow,      &NetworkState_int[0].hostCalc[compartment_offset], NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_int[i].currentsNow,  &NetworkState_int[0].hostCurrents[channel_offset], NetworkConst_l->GPU_numbers[i].nChannels_gpu * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_int[i].ysNow,        &NetworkState_int[0].hostYs[gate_offset], NetworkConst_l->GPU_numbers[i].nGates_gpu * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_int[i].Igap,         &NetworkState_int[0].Igap_host[compartment_offset], NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_int[i].VS_GAP,       NetworkState_int[0].VS_GAP_host, NetworkConst_l->nCells * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(NetworkState_int[i].VS_GAP_N,     NetworkState_int[0].VS_GAP_host, NetworkConst_l->nCells * sizeof(float), cudaMemcpyHostToDevice));

            //-->initialize noise OU
            if(sim.stimulusInfo.type == StimulusInfo::ORNSTEINUHLENBECK) {
                CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].ou_noise,   NetworkConst_l->GPU_numbers[i].nCells_gpu * sizeof(float)));
                CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].ou_noise,   NetworkConst_l->GPU_numbers[i].nCells_gpu * sizeof(float)));
                CUDA_CHECK_RETURN(cudaMalloc((void **) &NetworkState_int[i].rand_state, NetworkConst_l->GPU_numbers[i].nCells_gpu * sizeof(curandState_t)));
                const dim3 blockDim(BLOCKSIZE);
                const dim3 gridDim_cells(((NetworkConst_l[0].GPU_numbers[i].nCells_gpu + (blockDim.x - 1)) / blockDim.x));
                GPU_kernel_functions::initGPU_cellsize<<<gridDim_cells, blockDim>>>(NetworkConst_l->GPU_numbers[i].nCells_gpu, NetworkState_int[i].ou_noise, (curandStateXORWOW_t *)NetworkState_int[i].rand_state,NetworkConst_l->GPU_numbers[i].CellID_Offset_gpu + NetworkConst_l->CellID_Offset);
            }

            //--> update the offset numbers
            compartment_offset += NetworkConst_l->GPU_numbers[i].nCompartments_gpu;
            channel_offset += NetworkConst_l->GPU_numbers[i].nChannels_gpu;
            gate_offset += NetworkConst_l->GPU_numbers[i].nGates_gpu;

            //fix the application current
            NetworkState_int[i].rng = new XorShiftMul(((uint64_t)sim.stimulusInfo.ornsteinUhlenbeck.random_seed<<32) + sim.stimulusInfo.ornsteinUhlenbeck.random_seed);  //heap alloc

            //to catch errors ?
            CUDA_CHECK_RETURN(cudaDeviceSynchronize());

        }
    }

    //Now clean Host memory to be able to use the pointers later as place holders
    log(LOG_DEBUG, 0) << "clean \"normal\" host memory" << LOG_ENDL;
    delete[] NetworkState_l->hostVs;
    delete[] NetworkState_l->hostCalc;
    delete[] NetworkState_l->hostCurrents;
    delete[] NetworkState_l->hostYs;
    delete[] NetworkState_l->Igap_host;
    delete[] NetworkState_l->VS_GAP_host;

    //create pinned memory buffers
    log(LOG_DEBUG, 0) << "create pinned host memory" << LOG_ENDL;
    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostVs,       NetworkConst_l->nCompartments_l * sizeof(float)));
    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostVs_print, NetworkConst_l->nCompartments_l * sizeof(float)));

    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostCalc,     NetworkConst_l->nCompartments_l * sizeof(float)));
    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostCalc_print,     NetworkConst_l->nCompartments_l * sizeof(float)));

    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostCurrents, NetworkConst_l->nChannels_l * sizeof(float)));
    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostCurrents_print, NetworkConst_l->nChannels_l * sizeof(float)));

    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostYs,       NetworkConst_l->nGates_l * sizeof(float)));
    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->hostYs_print,       NetworkConst_l->nGates_l * sizeof(float)));

    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->Igap_host,    NetworkConst_l->nCompartments_l * sizeof(float)));
    CUDA_CHECK_RETURN(cudaMallocHost((void **) &NetworkState_l->VS_GAP_host,  NetworkConst_l->nCells * sizeof(float)));

    //initialize memory
    {
        //remember values
        uint32_t compartment_offset = 0;
        uint32_t channel_offset = 0;
        uint32_t gate_offset = 0;

        for (size_t i = 0; i < ProcessInfo.GPU; i++) {

            NetworkState_int[i].hostVs =       &NetworkState_l->hostVs[compartment_offset];
            NetworkState_int[i].hostVs_print =       &NetworkState_l->hostVs_print[compartment_offset];

            NetworkState_int[i].hostCalc =     &NetworkState_l->hostCalc[compartment_offset];
            NetworkState_int[i].hostCalc_print =     &NetworkState_l->hostCalc_print[compartment_offset];

            NetworkState_int[i].hostCurrents = &NetworkState_l->hostCurrents[channel_offset];
            NetworkState_int[i].hostCurrents_print = &NetworkState_l->hostCurrents_print[channel_offset];

            NetworkState_int[i].hostYs =       &NetworkState_l->hostYs[gate_offset];
            NetworkState_int[i].hostYs_print =       &NetworkState_l->hostYs_print[gate_offset];


            NetworkState_int[i].Igap_host =    &NetworkState_l->Igap_host[compartment_offset];
            NetworkState_int[i].VS_GAP_host =  NetworkState_l->VS_GAP_host;                     //watch out this is the whole buffer always!

            //--> update the offset numbers
            compartment_offset += NetworkConst_l->GPU_numbers[i].nCompartments_gpu;
            channel_offset     += NetworkConst_l->GPU_numbers[i].nChannels_gpu;
            gate_offset        += NetworkConst_l->GPU_numbers[i].nGates_gpu;
        }
    }

    delete[] NetworkState_l;
    return NetworkState_int;
}
NetworkConstStruct* fn_init_gpu_Backend_const(SimRunInfo &sim,process_info &ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l) {

    INIT_LOGP(LOG_DEFAULT, ProcessInfo.world_rank);

    NetworkConst_l->CellPop      = NetworkConst_l->CellPopulation;
    NetworkConst_l->compartment  = NetworkConst_l->CellPopulation[0].compartment;
    NetworkConst_l->Channel      = NetworkConst_l->CellPopulation[0].compartment[0].Channel;
    NetworkConst_l->Gate         = NetworkConst_l->CellPopulation[0].compartment[0].Channel[0].Gate;

    //create new construct
    auto NetworkCost_int = new NetworkConstStruct[ProcessInfo.GPU];

    //initialize memory
    {
        //remember values
        uint32_t cell_offset = 0;
        uint32_t compartment_offset = 0;
        uint32_t channel_offset = 0;
        uint32_t gate_offset = 0;


        for (size_t i = 0; i < ProcessInfo.GPU; i++) {
            std::memcpy(&NetworkCost_int[i], NetworkConst_l, sizeof(NetworkConstStruct));

#if DEBUG_GPU_EMUL == 0
            CUDA_CHECK_RETURN(cudaSetDevice(i));
#endif
            log(LOG_DEBUG, 0) << "remap networkconst to device memory" << LOG_ENDL;
            CellConstStruct *CellConstStruct_dev;
            CompartmentConstStruct *CompartmentConstStruct_dev;
            ChannelConstStruct *ChannelConstStruct_dev;
            GateConstStruct *GateConstStruct_dev;

            log(LOG_DEBUG, 0) << "allocate space for networkconst on dev" << LOG_ENDL;

            //todo

            CUDA_CHECK_RETURN(cudaMalloc((void **) &CompartmentConstStruct_dev, NetworkConst_l->nCompartments_u * sizeof(CompartmentConstStruct)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &ChannelConstStruct_dev,     NetworkConst_l->nChannels_u * sizeof(ChannelConstStruct)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &GateConstStruct_dev,        NetworkConst_l->nGates_u * sizeof(GateConstStruct)));

            fn_cellStructure_Initializations(sim, &NetworkCost_int[i], nullptr, CompartmentConstStruct_dev, ChannelConstStruct_dev, GateConstStruct_dev, NetworkCost_int[i].CellID_Offset, NetworkCost_int[i].nCells_l);

            CUDA_CHECK_RETURN(cudaMemcpy(CompartmentConstStruct_dev, NetworkCost_int[i].compartment,    NetworkConst_l->nCompartments_u * sizeof(CompartmentConstStruct), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(ChannelConstStruct_dev,     NetworkCost_int[i].Channel,        NetworkConst_l->nChannels_u * sizeof(ChannelConstStruct), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(GateConstStruct_dev,        NetworkCost_int[i].Gate,           NetworkConst_l->nGates_u * sizeof(GateConstStruct), cudaMemcpyHostToDevice));


            //todo
            CUDA_CHECK_RETURN(cudaMalloc((void **) &CellConstStruct_dev,        NetworkConst_l->GPU_numbers[i].nCells_gpu * sizeof(CellConstStruct)));
            CUDA_CHECK_RETURN(cudaMemcpy(CellConstStruct_dev,        &NetworkCost_int[i].CellPopulation[cell_offset], NetworkConst_l->GPU_numbers[i].nCells_gpu * sizeof(CellConstStruct), cudaMemcpyHostToDevice));


            NetworkCost_int[i].CellPopulation = CellConstStruct_dev;
            NetworkCost_int[i].compartment = CompartmentConstStruct_dev;
            NetworkCost_int[i].Channel = ChannelConstStruct_dev;
            NetworkCost_int[i].Gate = GateConstStruct_dev;

            //----> CREATE THE LOOKUP tables for INDEXES in device memory

            //TODO fixen van deze indexering probleem is dat we niet de volledige structure op elke GPU hebben...
            uint32_t *CompartmentIndex = nullptr;
            CUDA_CHECK_RETURN(cudaMalloc((void **) &CompartmentIndex,    NetworkConst_l->nCompartments_l * sizeof(uint32_t)));
            CUDA_CHECK_RETURN(cudaMemcpy(CompartmentIndex,    NetworkConst_l->CompartmentIndex,    NetworkConst_l->nCompartments_l * sizeof(uint32_t), cudaMemcpyHostToDevice));
            NetworkCost_int[i].CompartmentIndex = CompartmentIndex;

            uint32_t *Comp_index_vs_gates = nullptr;
            CUDA_CHECK_RETURN(cudaMalloc((void **) &Comp_index_vs_gates, NetworkConst_l->nGates_l * sizeof(uint32_t)));
            CUDA_CHECK_RETURN(cudaMemcpy(Comp_index_vs_gates, NetworkConst_l->Comp_index_vs_gates, NetworkConst_l->nGates_l * sizeof(uint32_t), cudaMemcpyHostToDevice));
            NetworkCost_int[i].Comp_index_vs_gates = Comp_index_vs_gates;

            uint32_t *GateNO_vs_GateIndex = nullptr;
            CUDA_CHECK_RETURN(cudaMalloc((void **) &GateNO_vs_GateIndex, NetworkConst_l->GPU_numbers[i].nGates_gpu * sizeof(uint32_t)));
            CUDA_CHECK_RETURN(cudaMemcpy(GateNO_vs_GateIndex, &NetworkConst_l->GateNO_vs_GateIndex[gate_offset], NetworkConst_l->GPU_numbers[i].nGates_gpu * sizeof(uint32_t), cudaMemcpyHostToDevice));


            // these can just be offsetted
            uint32_t *CellID_match_compID = nullptr;
            uint32_t *ChannelIndex = nullptr;
            uint32_t *GateIndex = nullptr;

            //rebalance compartment ID to specific ID because of multiple GPU's !
            for(size_t it = compartment_offset; it < compartment_offset + NetworkConst_l->GPU_numbers[i].nCompartments_gpu; it++){
                NetworkConst_l->CellID_match_compID[it] =  NetworkConst_l->CellID_match_compID[it] - cell_offset;
                NetworkConst_l->ChannelIndex[it] =  NetworkConst_l->ChannelIndex[it] - channel_offset;
                NetworkConst_l->GateIndex[it] =  NetworkConst_l->GateIndex[it] - gate_offset;
            }

            CUDA_CHECK_RETURN(cudaMalloc((void **) &CellID_match_compID, NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(uint32_t)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &ChannelIndex,        NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(uint32_t)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &GateIndex,           NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(uint32_t)));


            CUDA_CHECK_RETURN(cudaMemcpy(CellID_match_compID, &NetworkConst_l->CellID_match_compID[compartment_offset], NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(uint32_t), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(ChannelIndex,        &NetworkConst_l->ChannelIndex[compartment_offset],        NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(uint32_t), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(GateIndex,           &NetworkConst_l->GateIndex[compartment_offset],           NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(uint32_t), cudaMemcpyHostToDevice));


            NetworkCost_int[i].CellID_match_compID = CellID_match_compID;
            NetworkCost_int[i].ChannelIndex = ChannelIndex;
            NetworkCost_int[i].GateIndex = GateIndex;
            NetworkCost_int[i].GateNO_vs_GateIndex = GateNO_vs_GateIndex;

            //randomizations copy over //these can just be offsetted
            float *G_int = nullptr;
            float *G_Channels = nullptr;
            float *Inverpotential_Channels = nullptr;
            float *PassiveLeakConductivity = nullptr;

            CUDA_CHECK_RETURN(cudaMalloc((void **) &G_int,                    NetworkConst_l->GPU_numbers[i].nCells_gpu        * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &G_Channels,               NetworkConst_l->GPU_numbers[i].nChannels_gpu     * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &Inverpotential_Channels,  NetworkConst_l->GPU_numbers[i].nChannels_gpu     * sizeof(float)));
            CUDA_CHECK_RETURN(cudaMalloc((void **) &PassiveLeakConductivity,  NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float)));

            CUDA_CHECK_RETURN(cudaMemcpy(G_int,                   NetworkConst_l->G_int,                   NetworkConst_l->GPU_numbers[i].nCells_gpu        * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(G_Channels,              NetworkConst_l->G_Channels,              NetworkConst_l->GPU_numbers[i].nChannels_gpu     * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(Inverpotential_Channels, NetworkConst_l->Inverpotential_Channels, NetworkConst_l->GPU_numbers[i].nChannels_gpu     * sizeof(float), cudaMemcpyHostToDevice));
            CUDA_CHECK_RETURN(cudaMemcpy(PassiveLeakConductivity, NetworkConst_l->PassiveLeakConductivity, NetworkConst_l->GPU_numbers[i].nCompartments_gpu * sizeof(float), cudaMemcpyHostToDevice));

            NetworkCost_int[i].G_int = G_int;
            NetworkCost_int[i].G_Channels = G_Channels;
            NetworkCost_int[i].Inverpotential_Channels = Inverpotential_Channels;
            NetworkCost_int[i].PassiveLeakConductivity = PassiveLeakConductivity;

            //--> update the offset numbers
            cell_offset        += NetworkConst_l->GPU_numbers[i].nCells_gpu;
            compartment_offset += NetworkConst_l->GPU_numbers[i].nCompartments_gpu;
            channel_offset     += NetworkConst_l->GPU_numbers[i].nChannels_gpu;
            gate_offset        += NetworkConst_l->GPU_numbers[i].nGates_gpu;


            //to catch errors
            CUDA_CHECK_RETURN(cudaDeviceSynchronize());
        }
    }


    //delete
    delete[] NetworkConst_l->CellPopulation;
    delete[] NetworkConst_l->compartment;
    delete[] NetworkConst_l->Channel;
    delete[] NetworkConst_l->Gate;

    //delete
    delete[] NetworkConst_l->G_int;
    delete[] NetworkConst_l->G_Channels;
    delete[] NetworkConst_l->Inverpotential_Channels;
    delete[] NetworkConst_l->PassiveLeakConductivity;

    //delete
    delete[] NetworkConst_l->CellID_match_compID;
    delete[] NetworkConst_l->CompartmentIndex;
    delete[] NetworkConst_l->ChannelIndex;
    delete[] NetworkConst_l->GateIndex;
    delete[] NetworkConst_l->Comp_index_vs_gates;
    delete[] NetworkConst_l->GateNO_vs_GateIndex;
    //delete
    delete[] NetworkConst_l;

    return NetworkCost_int;
}
void fn_init_gpu_Backend_streams(process_info *ProcessInfo,RunMetaData &runMetaData){
    auto *streams = new cudaStream_t[num_streams*ProcessInfo->GPU];
    auto *eventstemp = new GPU_kernel_functions::eventmonitor_GPU[ProcessInfo->GPU];

    for(int j = 0; j < ProcessInfo->GPU; j++) {
#if DEBUG_GPU_EMUL == 0  //to emulate multiple gpu's on a single gpu :D
        CUDA_CHECK_RETURN(cudaSetDevice(j));  //set device
#endif
        eventstemp[j].init();
        for (int i = 0; i < num_streams; i++) {
            CUDA_CHECK_RETURN(cudaStreamCreate(&streams[num_streams * j + i]));
        }

        //get the used memsize
        size_t free, total;
        CUDA_CHECK_RETURN(cudaMemGetInfo(&free, &total));
        runMetaData.metaGPU.sGPU_Meta[j].memsizeused = runMetaData.metaGPU.sGPU_Meta[j].memsizeused - free;
    }
    ProcessInfo->GPUstreams = (void*)streams;
    ProcessInfo->GPUevents = (void*)eventstemp;
}
void fn_copy_neighCondu_array_gpu(CellConstStruct &CellPopulation, void *host, size_t size,int GPU_ID){
#if DEBUG_GPU_EMUL == 0  //to emulate multiple gpu's on a single gpu :D
    CUDA_CHECK_RETURN(cudaSetDevice(GPU_ID));  //set device
#endif
    CUDA_CHECK_RETURN(cudaMalloc((void **) &CellPopulation.NeighCondu,size));
    CUDA_CHECK_RETURN(cudaMemcpy(CellPopulation.NeighCondu,host,size,cudaMemcpyHostToDevice));
}
void fn_copy_neighID_array_gpu(CellConstStruct &CellPopulation, void *host, size_t size,int GPU_ID){
#if DEBUG_GPU_EMUL == 0  //to emulate multiple gpu's on a single gpu :D
    CUDA_CHECK_RETURN(cudaSetDevice(GPU_ID));  //set device
#endif
    CUDA_CHECK_RETURN(cudaMalloc((void **) &CellPopulation.NeighIds,size));
    CUDA_CHECK_RETURN(cudaMemcpy(CellPopulation.NeighIds,host,size,cudaMemcpyHostToDevice));
}
bool GPU_checker(SimRunInfo &sim,process_info &ProcessInfo, RunMetaData &runMetaData){
    INIT_LOGP(LOG_DEFAULT,ProcessInfo.world_rank);

    //---> Checking the GPU's available to utalize for the Backend
    int Amount_of_GPUS_detected = 0;                                                // Detected GPUs
    cudaError_t _m_cudaStat = cudaGetDeviceCount(&Amount_of_GPUS_detected);
	if (_m_cudaStat != cudaSuccess) {
	    log(LOG_ERR) << cudaGetErrorString(_m_cudaStat) << LOG_ENDL;
	    fprintf(stderr, "%3d %s: CUDA Error %s at line %d in file %s\n",ProcessInfo.world_rank,ProcessInfo.processor_name,cudaGetErrorString(_m_cudaStat), __LINE__, __FILE__);
        Amount_of_GPUS_detected = 0;
	}

    log(LOG_DEBUG) << "amount of gpus detected: " << Amount_of_GPUS_detected << LOG_ENDL;
    if(Amount_of_GPUS_detected != ProcessInfo.GPU){
        log(LOG_WARN) << "Amount of GPUs (" <<Amount_of_GPUS_detected << ") is not equal to the amount of GPUs requested (" << ProcessInfo.GPU << ")" << LOG_ENDL;
#if DEBUG_GPU_EMUL == 1
        log(LOG_WARN) << "USING :" << ProcessInfo.GPU << " which is multy GPU single GPU emulation for debugging only 1 real GPU supported !" << LOG_ENDL;
#else
        if(Amount_of_GPUS_detected <  ProcessInfo.GPU){
            ProcessInfo.GPU = Amount_of_GPUS_detected;
            log(LOG_WARN) << "Setting ProcessINFO.GPU to: " << ProcessInfo.GPU << LOG_ENDL;
        }
#endif
    }

    //setup metadata
    runMetaData.metaGPU.NumGPUS = ProcessInfo.GPU;
    runMetaData.metaGPU.sGPU_Meta = new sGPU_Meta_s[ProcessInfo.GPU];


    //---> device discovery
    for(int device_num = 0; device_num < ProcessInfo.GPU; device_num++) {
        cudaDeviceProp prop;
#if DEBUG_GPU_EMUL == 1
        const int device_num_check = 0;
#else
        const int device_num_check = device_num;
#endif

        CUDA_CHECK_RETURN(cudaGetDeviceProperties(&prop, device_num_check));
        runMetaData.metaGPU.sGPU_Meta[device_num].name = new char[256];
        strncpy(runMetaData.metaGPU.sGPU_Meta[device_num].name, prop.name, 256);

        runMetaData.metaGPU.sGPU_Meta[device_num].clockRate = prop.clockRate;
        runMetaData.metaGPU.sGPU_Meta[device_num].totalGlobalMem = prop.totalGlobalMem;
        runMetaData.metaGPU.sGPU_Meta[device_num].major = prop.major;
        runMetaData.metaGPU.sGPU_Meta[device_num].minor = prop.minor;

        size_t free,total;
        CUDA_CHECK_RETURN(cudaMemGetInfo(&free, &total));
        runMetaData.metaGPU.sGPU_Meta[device_num].memsizeused = free;

        runMetaData.metaGPU.sGPU_Meta[device_num].minor = prop.minor;

        if(!device_num)
            log(LOG_INFO) << "Listing GPUS:\n";
        log(LOG_INFO) << "    Device Number:                  " << device_num << "\n";
        log(LOG_INFO) << "      PCI device id:                " << prop.pciBusID << "\n";
        log(LOG_INFO) << "      Device name:                  " << prop.name << "\n";
        log(LOG_INFO) << "      Clock Rate (KHz):             " << prop.clockRate << "\n";
        log(LOG_INFO) << "      Memory Clock Rate (KHz):      " << prop.memoryClockRate << "\n";
        log(LOG_INFO) << "      Memory Bus Width (bits):      " << prop.memoryBusWidth << "\n";
        log(LOG_INFO) << "      Peak Memory Bandwidth (GB/s): " << (2.0 * prop.memoryClockRate * (prop.memoryBusWidth / 8) / 1.0e6) << "\n";
        log(LOG_INFO) << "      Total global memory (Gbytes): " << (prop.totalGlobalMem / 1000000000) << "\n";
        log(LOG_INFO) << "      Compute cabability :          " << prop.major << "." << prop.minor << "\n";
        log(LOG_INFO) << "      Number of multiprocessors :   " << prop.multiProcessorCount << " \n";

        int canAccesPeer = 0;
        log(LOG_INFO) << "      Device " << device_num << " can acces:[";
        for (int ic = 0; ic < Amount_of_GPUS_detected; ic++) {
            if (device_num != ic) {
                cudaDeviceEnablePeerAccess(ic, 0);

                CUDA_CHECK_RETURN(cudaDeviceCanAccessPeer(&canAccesPeer, device_num_check, ic));
                log(LOG_INFO) << "{"<< ic << "," << canAccesPeer << "} ";
                //todo
//                if (sim.GPU_disable_peer_acces) {
//                    if (canAccesPeer) {
//                        CUDA_CHECK_RETURN(cudaDeviceDisablePeerAccess(ic));
//                    }
//                }
            }
        }
        log(LOG_INFO) << LOG_ENDL;
    }
    return true;
}
bool sync_process_stream(process_info &ProcessInfo, int process){
    for (size_t i = 0; i < ProcessInfo.GPU; i++) {
#if DEBUG_GPU_EMUL == 0  //to emulate multiple gpu's on a single gpu :D
        CUDA_CHECK_RETURN(cudaSetDevice(i));  //set device
#endif
        //--> Streams
        auto *streams = (cudaStream_t *) ProcessInfo.GPUstreams;
        streams = &streams[num_streams * i];
        CUDA_CHECK_RETURN(cudaStreamSynchronize(streams[process]));
    }
    return true;
}
bool updatemetadataGPU(process_info &ProcessInfo, RunMetaData &md){
    INIT_LOGP(LOG_DEFAULT,ProcessInfo.world_rank);
    for (size_t i = 0; i < ProcessInfo.GPU; i++) {
#if DEBUG_GPU_EMUL == 0  //to emulate multiple gpu's on a single gpu :D
        CUDA_CHECK_RETURN(cudaSetDevice(i));  //set device
#endif
        auto *temp = (GPU_kernel_functions::eventmonitor_GPU*)ProcessInfo.GPUevents;
        GPU_kernel_functions::eventmonitor_GPU monitor = temp[i];
        md.metaGPU.sGPU_Meta[i].time_Igap += monitor.getGap();
        md.metaGPU.sGPU_Meta[i].time_Icalc_comp += monitor.getComp();
        md.metaGPU.sGPU_Meta[i].time_Icalc_gate += monitor.getGate();
        md.metaGPU.sGPU_Meta[i].time_dev_copy += monitor.getDevcopy();
        md.metaGPU.sGPU_Meta[i].time_p2p_copy += monitor.getP2pcopy();
    }
    return true;
}
bool share_betweenGPUS_aftermpi(process_info *ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct *NetworkConst_l){
        //--> Streams
        auto *streams = (cudaStream_t*)ProcessInfo->GPUstreams;
        streams = &streams[num_streams*0];
        //event monitor
        auto *temp = (GPU_kernel_functions::eventmonitor_GPU*)ProcessInfo->GPUevents;
        GPU_kernel_functions::eventmonitor_GPU monitor = temp[0];

        const int i = 0; //todo

        if(ProcessInfo->MonitorGPUkernels){cudaEventRecord(monitor.p2p_copy_start, streams[0]);
        for(int remoteGPU = 1; remoteGPU < ProcessInfo->GPU; remoteGPU++) {
#if DEBUG_GPU_EMUL
                CUDA_CHECK_RETURN(cudaMemcpyAsync(&NetworkState_l[remoteGPU].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                            &NetworkState_l[i].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                            NetworkConst_l[i].GPU_numbers[i].nCells_gpu * sizeof(float),
                                                            cudaMemcpyDeviceToDevice,
                                                            streams[0]));
#else
                CUDA_CHECK_RETURN(cudaMemcpyPeerAsync(&NetworkState_l[remoteGPU].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                      remoteGPU,
                                                      &NetworkState_l[i].VS_GAP_N[NetworkConst_l[i].CellID_Offset + NetworkConst_l[i].GPU_numbers[i].CellID_Offset_gpu],
                                                      i,
                                                      NetworkConst_l[i].GPU_numbers[i].nCells_gpu * sizeof(float),
                                                      streams[0]));
#endif
        }
        if(ProcessInfo->MonitorGPUkernels)cudaEventRecord(monitor.p2p_copy_stop, streams[0]);
    }
    return true;
}
