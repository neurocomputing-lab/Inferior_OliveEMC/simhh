#include "JSON.h"

#include <fstream>
#include <sstream> //file parsing
#include <string.h>


bool ImportJsonConnModel(cJSON *oConnModel, SimRunInfo &sim){
    if (!cJSON_IsObject(oConnModel)){
        sim.log(LOG_ERR) << "ConnectivityModel should be an object with properties" << LOG_ENDL;
        return false;
    }
    cJSON *oType = cJSON_GetObjectItemCaseSensitive(oConnModel, "Type");
    if(!cJSON_IsString(oType)){
        sim.log(LOG_ERR) << "Connectivity model requires a Type string property" << LOG_ENDL;
        return false;
    }
    std::string type(oType->valuestring);
    if(type == "Full"){
        cJSON *oWeight = cJSON_GetObjectItemCaseSensitive(oConnModel, "Weight");
        if( cJSON_IsNumber(oWeight) && std::isfinite(oWeight->valuedouble)
            && sim.setConnectivityFull(oWeight->valuedouble)
                ){
            sim.log(LOG_INFO,0) << "Set full connectivity to " << oWeight->valuedouble<< " weight" << LOG_ENDL;
            return true;
        }
        else{
            sim.log(LOG_ERR) << "Full connectivity model requires finite gap junction weights" << LOG_ENDL;
            return false;
        }
    }
    else if(type == "Read"){
        cJSON *oFilename = cJSON_GetObjectItemCaseSensitive(oConnModel, "Filename");
        if( cJSON_IsString(oFilename) ){
//            fprintf(info_log, "Attempting to parse connections file %s...\n", oFilename->valuestring);
            sprintf(sim.conn_info.read.filename, "%s", oFilename->valuestring);
            sim.conn_info.type = ConnectivityInfo::READ;
        }
        else{
//            fprintf(error_log, "Inappropriate filename for connections map.\n");
            return false;
        }

        cJSON *oWeight     = cJSON_GetObjectItemCaseSensitive(oConnModel, "Weight");

        float temp_weight = nanf("");
        if(cJSON_IsNumber(oWeight)){
            temp_weight =  oWeight->valuedouble;
            sim.log(LOG_INFO,0) << "set read weight " << temp_weight << LOG_ENDL;
            sim.conn_info.use_condu = false;
        }
        else{
             temp_weight = nanf("");
            sim.conn_info.use_condu = true;
        }
        sim.conn_info.read.weight = temp_weight;
        return true;
    }
    else if(type == "RandomBinary"){
        cJSON *oWeight = cJSON_GetObjectItemCaseSensitive(oConnModel, "Weight");
        cJSON *oDensity = cJSON_GetObjectItemCaseSensitive(oConnModel, "Density");
        cJSON *oSeed = cJSON_GetObjectItemCaseSensitive(oConnModel, "RandomSeed");

        uint32_t random_seed;
        if( cJSON_IsString(oSeed) && !strcmp(oSeed->valuestring, "time") ){
            random_seed = (uint32_t)time(NULL);
//            if(DEBUG)  fprintf(info_log, "Set timestamp seed %d\n", (int32_t)random_seed);
        }
        else if( cJSON_IsNumber(oSeed) && (oSeed->valueint == oSeed->valuedouble) ){
            random_seed = oSeed->valueint;
//            fprintf(info_log, "Set specified seed %d\n", (int32_t)random_seed);
        }
        else{
//            fprintf(error_log, "file %s: Reading ConnectivityModel: RandomSeed should be an integer property, or \"time\" for timestamp seed\n", filename);
            return false;
        }

        float temp_weight = nanf("");
        if(cJSON_IsNumber(oWeight)){
            temp_weight =  oWeight->valuedouble;
        }
        else{
//            fprintf(error_log, "file %s: Setting weight to NaN this should not be supported in RAndombinary mode\n", filename);
            temp_weight = nanf("");
        }

        if(cJSON_IsNumber(oDensity) && sim.setConnectivityRandomBinary(temp_weight, oDensity->valueint, random_seed)){
//            fprintf(info_log, "file %s: Set RandomBinary connectivity to %f weight, %f density, %d seed\n", filename, temp_weight, oDensity->valuedouble, random_seed);
            return true;
        }
        else{
//            fprintf(error_log, "file %s: RandomBinary connectivity model requires density in [0,1]\n", filename);
            return false;
        }
    }
    else if(type == "Gaussian"){
        cJSON *oWeight = cJSON_GetObjectItemCaseSensitive(oConnModel, "Weight");
        cJSON *oDensity = cJSON_GetObjectItemCaseSensitive(oConnModel, "Density");
        cJSON *oMean = cJSON_GetObjectItemCaseSensitive(oConnModel, "Mean");
        cJSON *oVariance = cJSON_GetObjectItemCaseSensitive(oConnModel, "Variance");
        cJSON *oSeed = cJSON_GetObjectItemCaseSensitive(oConnModel, "RandomSeed");

        uint32_t random_seed;
        if( cJSON_IsString(oSeed) && !strcmp(oSeed->valuestring, "time") ){
            random_seed = (uint32_t)time(NULL);
//            if(DEBUG)  fprintf(info_log, "Set timestamp seed %d\n", (int32_t)random_seed);
        }
        else if( cJSON_IsNumber(oSeed) && (oSeed->valueint == oSeed->valuedouble) ){
            random_seed = oSeed->valueint;
//            fprintf(info_log, "Set specified seed %d\n", (int32_t)random_seed);
        }
        else{
            sim.log(LOG_ERR) <<"Wrong seed value" << LOG_ENDL;
//            fprintf(error_log, "file %s: Reading ConnectivityModel: RandomSeed should be an integer property, or \"time\" for timestamp seed\n", filename);
            return false;
        }

        if( cJSON_IsNumber(oWeight) && cJSON_IsNumber(oDensity)  && cJSON_IsNumber(oMean) && cJSON_IsNumber(oVariance)
            && sim.setConnectivityGaussian(oWeight->valuedouble, oDensity->valueint,oMean->valuedouble,oVariance->valuedouble,random_seed)
                ){
//            fprintf(info_log, "file %s: Set RandomBinary connectivity to %f weight, %f density, %d seed\n", filename, oWeight->valuedouble, oDensity->valuedouble, random_seed);
            return true;
        }
//        else if( cJSON_IsNumber(oWeight) && cJSON_IsNumber(oDensity)
//                 && sim.setConnectivityGaussian(oWeight->valuedouble, oDensity->valueint,0,0,random_seed)
//                ){
////            fprintf(info_log, "file %s: Set RandomBinary connectivity to %f weight, %f density, %d seed\n", filename, 0.0, 0.0, random_seed);
//            return true;
//        }
        else{
            sim.log(LOG_ERR) <<"requires finite gap junction weight and oMean and o Variance" << LOG_ENDL;
            exit(2);
            return false;
        }
//        TODO
        return false;
    }
    else{
        sim.log(LOG_ERR) << "Unknown connectivity model type: " << type << LOG_ENDL;
        return false;
    }

    //should always be unreachable!  (quick and dirty editor visual check)
    return true;
}
bool ImportJsonStimModel(cJSON *oStimModel, SimRunInfo &sim){
    if (!cJSON_IsObject(oStimModel)){
		sim.log(LOG_ERR) << "StimulusModel should be an object with properties" << LOG_ENDL;
		return false;
	}
	cJSON *oType = cJSON_GetObjectItemCaseSensitive(oStimModel, "Type");
	if(!cJSON_IsString(oType)){
        sim.log(LOG_ERR) << "Stimulus model requires a Type string property" << LOG_ENDL;
		return false;
	}
	std::string type(oType->valuestring);

	if(type == "DcCurrentPulseModulo"){
		cJSON *oBase = cJSON_GetObjectItemCaseSensitive(oStimModel, "BaseCurrent");
		cJSON *oStart = cJSON_GetObjectItemCaseSensitive(oStimModel, "StartTime");
		cJSON *oDuration = cJSON_GetObjectItemCaseSensitive(oStimModel, "Duration");
		cJSON *oModulo = cJSON_GetObjectItemCaseSensitive(oStimModel, "Modulo");
		cJSON *oModuloCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel, "ModuloCurrent");

		if( cJSON_IsNumber(oBase) && cJSON_IsNumber(oStart) && cJSON_IsNumber(oDuration)
			&& cJSON_IsNumber(oModulo) && oModulo->valueint == oModulo->valuedouble && cJSON_IsNumber(oModuloCurrent)
			&& sim.setDcPulseModulo(round(oStart->valuedouble/sim.timestep), round((oDuration->valuedouble + oStart->valuedouble)/sim.timestep), oBase->valuedouble, oModulo->valueint, oModuloCurrent->valuedouble)
		){
            sim.log(LOG_INFO,0) << "Set DcCurrentPulseModulo stimulus to ["<<  oStart->valuedouble<<" ... "<<oStart->valuedouble+oDuration->valuedouble<<"] time, "<<oBase->valuedouble<<" ... "<<oBase->valuedouble + (oModulo->valueint-1)*oModuloCurrent->valuedouble<<" current, "<<oModulo->valueint<<" modulo" << LOG_ENDL;
			return true;
		}
		else{
            sim.log(LOG_ERR) << "DcCurrentPulseModulo stimulus requires finite base current, start time and modulo current, positive duration, positive integer modulo" << LOG_ENDL;
			return false;
		}
	}
	else if(type == "DcCurrentStepandHold"){
        cJSON *oHoldCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel, "HoldCurrent"); if(!cJSON_IsNumber(oHoldCurrent)){ sim.log(LOG_ERR)<<"provide HoldCurrent "<<LOG_ENDL; return false;}
        cJSON *oBaseCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel, "BaseCurrent"); if(!cJSON_IsNumber(oBaseCurrent)){ sim.log(LOG_ERR)<<"provide BaseCurrent "<<LOG_ENDL; return false;}
        cJSON *oStepCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel, "StepCurrent"); if(!cJSON_IsNumber(oStepCurrent)){ sim.log(LOG_ERR)<<"provide StepCurrent "<<LOG_ENDL; return false;}
        cJSON *oStarttime   = cJSON_GetObjectItemCaseSensitive(oStimModel, "Starttime");   if(!cJSON_IsNumber(oStarttime))  { sim.log(LOG_ERR)<<"provide Starttime   "<<LOG_ENDL; return false;}
        cJSON *oDuration    = cJSON_GetObjectItemCaseSensitive(oStimModel, "Duration");    if(!cJSON_IsNumber(oDuration))   { sim.log(LOG_ERR)<<"provide Duration    "<<LOG_ENDL; return false;}
        cJSON *oHoldTime    = cJSON_GetObjectItemCaseSensitive(oStimModel, "HoldTime");    if(!cJSON_IsNumber(oHoldTime))   { sim.log(LOG_ERR)<<"provide HoldTime    "<<LOG_ENDL; return false;}

        uint32_t start_step = round(oStarttime->valuedouble/sim.timestep);
        uint32_t end_step  =  start_step + round((oDuration->valuedouble)/sim.timestep);
        uint32_t end_hold  =  end_step   + round((oHoldTime->valuedouble)/sim.timestep);

        if(sim.setDcStepandHold(start_step,end_step,end_hold,oBaseCurrent->valuedouble,oStepCurrent->valuedouble,oHoldCurrent->valuedouble)){
            return true;
        }
        else{
            sim.log(LOG_ERR)<< "DcCurrentStepandHOLD failed" << LOG_ENDL;
            return false;
        }
	}

	else if(type == "DcCurrentRampandHold"){
        cJSON *oBaseCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel, "BaseCurrent"); if(!cJSON_IsNumber(oBaseCurrent)){ sim.log(LOG_ERR)<<"provide BaseCurrent \n"; return false;}
        cJSON *oHoldCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel, "HoldCurrent"); if(!cJSON_IsNumber(oHoldCurrent)){ sim.log(LOG_ERR)<<"provide HoldCurrent \n"; return false;}
        cJSON *oRampCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel, "RampCurrent"); if(!cJSON_IsNumber(oRampCurrent)){ sim.log(LOG_ERR)<<"provide RampCurrent \n"; return false;}
        cJSON *oStarttime   = cJSON_GetObjectItemCaseSensitive(oStimModel, "Starttime");   if(!cJSON_IsNumber(oStarttime))  { sim.log(LOG_ERR)<<"provide Starttime   \n"; return false;}
        cJSON *oDuration    = cJSON_GetObjectItemCaseSensitive(oStimModel, "Duration");    if(!cJSON_IsNumber(oDuration))   { sim.log(LOG_ERR)<<"provide Duration    \n"; return false;}
        cJSON *oHoldTime    = cJSON_GetObjectItemCaseSensitive(oStimModel, "HoldTime");    if(!cJSON_IsNumber(oHoldTime))   { sim.log(LOG_ERR)<<"provide HoldTime    \n"; return false;}

        uint32_t start_ramp = round(oStarttime->valuedouble/sim.timestep);
        uint32_t end_ramp  =  start_ramp + round((oDuration->valuedouble)/sim.timestep);
        uint32_t end_hold  =  end_ramp   + round((oHoldTime->valuedouble)/sim.timestep);

        if(sim.setDcRampandHold(start_ramp,end_ramp,end_hold,oBaseCurrent->valuedouble,oRampCurrent->valuedouble,oHoldCurrent->valuedouble)){
            return true;
        }
        else{
            sim.log(LOG_ERR)<< "DcCurrentRampandHOLD failed" << LOG_ENDL;
            return false;
        }
    }

    else if(type == "DcCurrentPulseCell"){
        cJSON *oBase = cJSON_GetObjectItemCaseSensitive(oStimModel,          "BaseCurrent");
        cJSON *oStart = cJSON_GetObjectItemCaseSensitive(oStimModel,         "StartTime");
        cJSON *oDuration = cJSON_GetObjectItemCaseSensitive(oStimModel,      "Duration");
        cJSON *oCellID = cJSON_GetObjectItemCaseSensitive(oStimModel,        "CellId");
        cJSON *oPulseCurrent = cJSON_GetObjectItemCaseSensitive(oStimModel,  "PulseCurrent");
        if(cJSON_IsNumber(oBase) && cJSON_IsNumber(oStart) && cJSON_IsNumber(oDuration) && cJSON_IsNumber(oCellID) && cJSON_IsNumber(oPulseCurrent) &&
        sim.setDcPulseCell(round(oStart->valuedouble/sim.timestep), round((oDuration->valuedouble + oStart->valuedouble)/sim.timestep), oBase->valuedouble, oPulseCurrent->valuedouble,oCellID->valueint)){
            return true;
        }
        else{
            sim.log(LOG_ERR)<< "DcCurrentPulseCells failed, requires finite base current, start time and pulse current, positive duration" << LOG_ENDL;
            return false;
        }
    }
    else if(type == "OrnsteinUhlenbeck"){
        cJSON *oTheta = cJSON_GetObjectItemCaseSensitive(oStimModel, "Theta");  if (!cJSON_IsNumber(oTheta)) { sim.log(LOG_ERR)<<"provide Theta\n"; return false;}
        cJSON *oSigma = cJSON_GetObjectItemCaseSensitive(oStimModel, "Sigma");  if (!cJSON_IsNumber(oSigma)) { sim.log(LOG_ERR)<<"provide Sigma\n"; return false;}
        cJSON *oMu    = cJSON_GetObjectItemCaseSensitive(oStimModel, "Mu");     if (!cJSON_IsNumber(oMu))    { sim.log(LOG_ERR)<<"provide Mu\n";    return false;}
        cJSON *oAlpha = cJSON_GetObjectItemCaseSensitive(oStimModel, "Alpha");  if (!cJSON_IsNumber(oAlpha)) { sim.log(LOG_ERR)<<"provide Alpha\n"; return false;}
        if (sim.setOrnsteinUhlenbeck(oTheta->valuedouble, oSigma->valuedouble, oMu->valuedouble, oAlpha->valuedouble, sim.timestep, sqrt(sim.timestep))){
            return true;
        }
        else{
            sim.log(LOG_ERR)<< "DcCurrentRampandHOLD failed" << LOG_ENDL;
            return false;
        }
    }
	else{
        sim.log(LOG_ERR)<< "unknown stimulus model type:" << type << LOG_ENDL;
		return false;
	}
}
bool ImportOutputmonitorModel(cJSON *oOutputMonitorModel, SimRunInfo &sim){
    if (!cJSON_IsObject(oOutputMonitorModel)) {
        sim.log(LOG_ERR) << "MonitorModel should be an object with properties" << LOG_ENDL;
        return false;
    }

    cJSON *oFormatting = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "Formatting");
    if (!cJSON_IsInvalid(oFormatting)){
        if(cJSON_IsBool(oFormatting)) {
            if (oFormatting->valueint) {
                sim.output_monitor.formatting = true;
            }
        }
    }

    cJSON *oName = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "Name");
    if(!cJSON_IsString(oName)){
        sim.log(LOG_ERR) << "MonitorModel model requires a Name string property" << LOG_ENDL;
        return false;
    }
    std::string name(oName->valuestring);

    cJSON *oType = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "Type");
    if(!cJSON_IsString(oType)){
        sim.log(LOG_ERR) << "MonitorModel model requires a Type string property" << LOG_ENDL;
        return false;
    }
    std::string type(oType->valuestring);

    sim.output_monitor.name = name;

    if(type == "All") {
        sim.output_monitor.type = OutputMonitor::ALL;
    }

    //booleans
    cJSON *oWriteCompartPotentials = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "WriteCompartPotentials");
    if( cJSON_IsBool(oWriteCompartPotentials) ){
        sim.output_monitor.WriteCompartPotentials = (bool)(oWriteCompartPotentials->valueint);
    }
    else{
        sim.log(LOG_ERR) << "WriteCompartPotentials is a required boolean parameter" << LOG_ENDL;
        return false;
    }
    cJSON *oWriteCurrents = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "WriteCurrents");
    if( cJSON_IsBool(oWriteCurrents) ){
        sim.output_monitor.WriteCurrents = (bool)(oWriteCurrents->valueint);
    }
    else{
        sim.log(LOG_ERR) << "WriteCurrentsPotentials is a required boolean parameter" << LOG_ENDL;
        return false;
    }

    cJSON *oWriteActivationVariables = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "WriteActivationVariables");
    if( cJSON_IsBool(oWriteActivationVariables) ){
        sim.output_monitor.WriteActivationVariables = (bool)(oWriteActivationVariables->valueint);
    }
    else{
        sim.log(LOG_ERR) << "WriteActivationVariables is a required boolean parameter" << LOG_ENDL;
        return false;
    }
    cJSON *oWriteCalciumLevels = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "WriteCalciumLevels");
    if( cJSON_IsBool(oWriteCalciumLevels) ){
        sim.output_monitor.WriteCalciumLevels = (bool)(oWriteCalciumLevels->valueint);
    }
    else{
        sim.log(LOG_ERR) << "WriteCalciumLevels is a required boolean parameter" << LOG_ENDL;
        return false;
    }
    //float
    cJSON *otimestep = cJSON_GetObjectItemCaseSensitive(oOutputMonitorModel, "SaveInterval");
    if(otimestep){
        if (cJSON_IsNumber(otimestep)){
            sim.output_monitor.timestep_save_interval = otimestep->valueint;
            if(!otimestep->valueint){
                sim.log(LOG_ERR) << "SaveInterval parameter should be a multiplication (int) factor of simtime" << LOG_ENDL;
                return false;
            }
        }
        else{
            sim.log(LOG_ERR) << "SaveInterval parameter should be a multiplication factor of simtime" << LOG_ENDL;
            return false;
        }
    }else{
        sim.log(LOG_ERR) << "SaveInterval is a required uint parameter" << LOG_ENDL;
        return false;
    }
    return true;
}
bool ImportMetadatamonitorModel(cJSON *oMetaDataMonitorModel, SimRunInfo &sim){
    if (!cJSON_IsObject(oMetaDataMonitorModel)) {
        sim.log(LOG_ERR) << "MetaDataMonitor should be an object with properties" << LOG_ENDL;
        return false;
    }

    cJSON *oName = cJSON_GetObjectItemCaseSensitive(oMetaDataMonitorModel, "Name");
    if(!cJSON_IsString(oName)){
        sim.log(LOG_ERR) << "MonitorModel model requires a Name string property" << LOG_ENDL;
        return false;
    }
    std::string name(oName->valuestring);
    sim.meta_data_monitor.type = MetaDataMonitor::Full;
    sim.meta_data_monitor.name = name;
    return true;
}
bool ImportJsonFormula(cJSON *oFormula, Formula &fo,  miniLogger &log){
	if(!cJSON_IsObject(oFormula)){
	    log(LOG_ERR) << "Formula should be an object" << LOG_ENDL;
		return false;
	}
	cJSON *oType = cJSON_GetObjectItemCaseSensitive(oFormula, "Type");
	if(!cJSON_IsString(oType)){
        log(LOG_ERR) << "Formula must have a defined Type string" << LOG_ENDL;
		return false;
	}
	std::string type = oType->valuestring;

	if(type == "LinearByExponential"){
		fo.type = Formula::LINEAR_BY_EXPONENTIAL;
		Formula::LinByExp &f = fo.lin_by_exp;

		cJSON *oLvo = cJSON_GetObjectItemCaseSensitive(oFormula, "LinearVarOffset");
		if(!cJSON_IsNumber(oLvo)){
            log(LOG_ERR) << "Formula must have LinearVarOffset parameter" << LOG_ENDL;
			return false;
		}
		f.lin_offset = oLvo->valuedouble;

		cJSON *oLvs = cJSON_GetObjectItemCaseSensitive(oFormula, "LinearVarScale");
		if(!cJSON_IsNumber(oLvs)){
            log(LOG_ERR) << "Formula must have LinearVarScale parameter" << LOG_ENDL;
			return false;
		}
		f.lin_scale = oLvs->valuedouble;

		cJSON *oEvo = cJSON_GetObjectItemCaseSensitive(oFormula, "ExponentialVarOffset");
		if(!cJSON_IsNumber(oEvo)){
            log(LOG_ERR) << "Formula must have ExponentialVarOffset parameter" << LOG_ENDL;
			return false;
		}
		f.exp_var_offset = oEvo->valuedouble;

		cJSON *oEvs = cJSON_GetObjectItemCaseSensitive(oFormula, "ExponentialVarScale");
		if(!cJSON_IsNumber(oEvs)){
            log(LOG_ERR) << "Formula must have ExponentialVarScale parameter" << LOG_ENDL;
			return false;
		}
		f.exp_var_scale = oEvs->valuedouble;

		cJSON *oEs = cJSON_GetObjectItemCaseSensitive(oFormula, "ExponentialScale");
		if(!cJSON_IsNumber(oEs)){
            log(LOG_ERR) << "Formula must have ExponentialScale parameter" << LOG_ENDL;
			return false;
		}
		f.exp_scale = oEs->valuedouble;

		cJSON *oEo = cJSON_GetObjectItemCaseSensitive(oFormula, "ExponentialOffset");
		if(!cJSON_IsNumber(oEo)){
            log(LOG_ERR) << "Formula must have ExponentialOffset parameter" << LOG_ENDL;
			return false;
		}
		f.exp_offset = oEo->valuedouble;

		cJSON *oO = cJSON_GetObjectItemCaseSensitive(oFormula, "Offset");
		if(!cJSON_IsNumber(oO)){
            log(LOG_ERR) << "Formula must have Offset parameter" << LOG_ENDL;
			return false;
		}
		f.offset = oO->valuedouble;

	}
	else if(type == "ExponentialByExponential"){
		fo.type = Formula::EXPONENTIAL_BY_EXPONENTIAL;
		Formula::ExpByExp &f = fo.exp_by_exp;
		{
		cJSON *oEvo = cJSON_GetObjectItemCaseSensitive(oFormula, "NumVarOffset");
		if(!cJSON_IsNumber(oEvo)){
            log(LOG_ERR) << "Formula must have NumVarOffset parameter" << LOG_ENDL;
			return false;
		}
		f.num_var_offset = oEvo->valuedouble;

		cJSON *oEvs = cJSON_GetObjectItemCaseSensitive(oFormula, "NumVarScale");
		if(!cJSON_IsNumber(oEvs)){
            log(LOG_ERR) << "Formula must have NumVarScale parameter" << LOG_ENDL;
			return false;
		}
		f.num_var_scale = oEvs->valuedouble;

		cJSON *oEs = cJSON_GetObjectItemCaseSensitive(oFormula, "NumScale");
		if(!cJSON_IsNumber(oEs)){
            log(LOG_ERR) << "Formula must have NumScale parameter" << LOG_ENDL;
			return false;
		}
		f.num_scale = oEs->valuedouble;

		cJSON *oEo = cJSON_GetObjectItemCaseSensitive(oFormula, "NumOffset");
		if(!cJSON_IsNumber(oEo)){
            log(LOG_ERR) << "Formula must have NumOffset parameter" << LOG_ENDL;
			return false;
		}
		f.num_offset = oEo->valuedouble;
		}

		{
		cJSON *oEvo = cJSON_GetObjectItemCaseSensitive(oFormula, "DenVarOffset");
		if(!cJSON_IsNumber(oEvo)){
            log(LOG_ERR) << "Formula must have DenVarOffset parameter" << LOG_ENDL;
			return false;
		}
		f.den_var_offset = oEvo->valuedouble;

		cJSON *oEvs = cJSON_GetObjectItemCaseSensitive(oFormula, "DenVarScale");
		if(!cJSON_IsNumber(oEvs)){
            log(LOG_ERR) << "Formula must have DenVarScale parameter" << LOG_ENDL;
			return false;
		}
		f.den_var_scale = oEvs->valuedouble;

		cJSON *oEs = cJSON_GetObjectItemCaseSensitive(oFormula, "DenScale");
		if(!cJSON_IsNumber(oEs)){
            log(LOG_ERR) << "Formula must have DenScale parameter" << LOG_ENDL;
			return false;
		}
		f.den_scale = oEs->valuedouble;

		cJSON *oEo = cJSON_GetObjectItemCaseSensitive(oFormula, "DenOffset");
		if(!cJSON_IsNumber(oEo)){
            log(LOG_ERR) << "Formula must have DenOffset parameter" << LOG_ENDL;
			return false;
		}
		f.den_offset = oEo->valuedouble;
		}

		cJSON *oO = cJSON_GetObjectItemCaseSensitive(oFormula, "Offset");
		if(!cJSON_IsNumber(oO)){
            log(LOG_ERR) << "Formula must have Offset parameter" << LOG_ENDL;
			return false;
		}
		f.offset = oO->valuedouble;

	}
	else if(type == "InverseOfTwoExponentials"){
		fo.type = Formula::INVERSE_OF_TWO_EXPONENTIALS;
		Formula::InvOfTwoExp &f = fo.inv_of_two_exp;

		{
		cJSON *oEvo = cJSON_GetObjectItemCaseSensitive(oFormula, "OneVarOffset");
		if(!cJSON_IsNumber(oEvo)){
            log(LOG_ERR) << "Formula must have OneVarOffset parameter" << LOG_ENDL;
			return false;
		}
		f.exp1_var_offset = oEvo->valuedouble;

		cJSON *oEvs = cJSON_GetObjectItemCaseSensitive(oFormula, "OneVarScale");
		if(!cJSON_IsNumber(oEvs)){
            log(LOG_ERR) << "Formula must have OneVarScale parameter" << LOG_ENDL;
			return false;
		}
		f.exp1_var_scale = oEvs->valuedouble;

		cJSON *oEs = cJSON_GetObjectItemCaseSensitive(oFormula, "OneScale");
		if(!cJSON_IsNumber(oEs)){
            log(LOG_ERR) << "Formula must have OneScale parameter" << LOG_ENDL;
			return false;
		}
		f.exp1_scale = oEs->valuedouble;
		} {
		cJSON *oEvo = cJSON_GetObjectItemCaseSensitive(oFormula, "TwoVarOffset");
		if(!cJSON_IsNumber(oEvo)){
            log(LOG_ERR) << "Formula must have TwoVarOffset parameter" << LOG_ENDL;
			return false;
		}
		f.exp2_var_offset = oEvo->valuedouble;

		cJSON *oEvs = cJSON_GetObjectItemCaseSensitive(oFormula, "TwoVarScale");
		if(!cJSON_IsNumber(oEvs)){
            log(LOG_ERR) << "Formula must have TwoVarScale parameter" << LOG_ENDL;
			return false;
		}
		f.exp2_var_scale = oEvs->valuedouble;

		cJSON *oEs = cJSON_GetObjectItemCaseSensitive(oFormula, "TwoScale");
		if(!cJSON_IsNumber(oEs)){
            log(LOG_ERR) << "Formula must have TwoScale parameter" << LOG_ENDL;
			return false;
		}
		f.exp2_scale = oEs->valuedouble;
		}

		cJSON *oEo = cJSON_GetObjectItemCaseSensitive(oFormula, "DenOffset");
		if(!cJSON_IsNumber(oEo)){
            log(LOG_ERR) << "Formula must have DenOffset parameter" << LOG_ENDL;
			return false;
		}
		f.den_offset = oEo->valuedouble;

		cJSON *oO = cJSON_GetObjectItemCaseSensitive(oFormula, "Offset");
		if(!cJSON_IsNumber(oO)){
            log(LOG_ERR) << "Formula must have Offset parameter" << LOG_ENDL;
			return false;
		}
		f.offset = oO->valuedouble;




	}
	else if(type == "ClippedLinear"){
		fo.type = Formula::CLIPPED_LINEAR;
		Formula::ClippedLinear &f = fo.clipped_linear;

		cJSON *oLvo = cJSON_GetObjectItemCaseSensitive(oFormula, "VarOffset");
		if(!cJSON_IsNumber(oLvo)){
            log(LOG_ERR) << "Formula must have VarOffset parameter" << LOG_ENDL;
			return false;
		}
		f.offset = oLvo->valuedouble;

		cJSON *oLvs = cJSON_GetObjectItemCaseSensitive(oFormula, "VarScale");
		if(!cJSON_IsNumber(oLvs)){
            log(LOG_ERR) << "Formula must have VarScale parameter" << LOG_ENDL;
			return false;
		}
		f.scale = oLvs->valuedouble;

		cJSON *oMax = cJSON_GetObjectItemCaseSensitive(oFormula, "Maximum");
		if(!cJSON_IsNumber(oMax)){
            log(LOG_ERR) << "Formula must have Maximum parameter" << LOG_ENDL;
			return false;
		}
		f.maximum = oMax->valuedouble;

	}
	else{
        log(LOG_ERR) << "Unknown formula type" << LOG_ENDL;
		return false;
	}
	return true;
}
bool ImportJsonNeuronModel(cJSON *oNeuronModel, NeuronInfo &new_neu,process_info PROC){
    miniLogger log(LOG_WARN,std::cout, "Unknown",PROC.world_rank);
	if (!cJSON_IsObject(oNeuronModel)){
        log(LOG_ERR)<<"NeuronModel should be an object with properties" << LOG_ENDL;
		return false;
	}
    new_neu = NeuronInfo();

	//set label for easy debug
	std::string label;
    cJSON *oName = cJSON_GetObjectItemCaseSensitive(oNeuronModel, "Label");
	if(cJSON_IsString(oName)){
        label = oName->valuestring;
        log.set_name(label);
	}else{
	    label = "Unknown";
	}



	//Find the multiplier default is 1
    new_neu.multiplier = 1;
    cJSON *aAmount = cJSON_GetObjectItemCaseSensitive(oNeuronModel, "NeuronModelSize");
    if(cJSON_IsNumber(aAmount)) {
        new_neu.multiplier = aAmount->valueint;
    }

    //TODO ?
    cJSON *ogRatioR = cJSON_GetObjectItemCaseSensitive(oNeuronModel, "Ratio_Randomization_Offset");
    if(cJSON_IsNumber(ogRatioR)){
        new_neu.Ratio_Random_offset = ogRatioR->valuedouble;
        printf("Use Ratio randomization Enabled\n");
    }else {
        new_neu.Ratio_Random_offset = 0;
    }

    //Set random seed for each NeuronDescription !!!
    cJSON *oSeed = cJSON_GetObjectItemCaseSensitive(oNeuronModel, "RandomSeed");
    auto random_seed = (uint32_t)time(nullptr);
    if( cJSON_IsString(oSeed) && !strcmp(oSeed->valuestring, "time") ){
        random_seed = (uint32_t)time(nullptr);
        log(LOG_DEBUG,0)<<"Set timestamp seed for randomization " << (int32_t)random_seed << LOG_ENDL;
    }
    else if( cJSON_IsNumber(oSeed) && (oSeed->valueint == oSeed->valuedouble) ){
        random_seed = oSeed->valueint;
        log(LOG_INFO,0)<<"Set specified seed for randomization " << (int32_t)random_seed << LOG_ENDL;
    }
    new_neu.random_seed = random_seed;


//todo hier crtf
    cJSON *LeakConductanceLists = cJSON_GetObjectItemCaseSensitive(oNeuronModel, "LeakConductanceLists");
    if( cJSON_IsString(LeakConductanceLists)) {
        new_neu.enable_custom_leak = true;
        //create the array
        new_neu.custom_leak_array = new float[new_neu.multiplier];
        sprintf(new_neu.custom_leak_conductance_lists, "%s", LeakConductanceLists->valuestring);
        //open file
        std::ifstream infile(new_neu.custom_leak_conductance_lists);
        if (!infile) {
            log(LOG_ERR) << " GLeakConductanceLists file not found: " << new_neu.custom_leak_conductance_lists << LOG_ENDL;
            fprintf(stderr, "LeakConductanceLists file not found %s\n", new_neu.custom_leak_conductance_lists);
            exit(3);
        }
        std::string line;
        float value;
        size_t count = 0;
        while (std::getline(infile, line)) {
            std::istringstream iss(line);
            if (!(iss >> value)) { log(LOG_ERR) << "malformed file for custom_leak_conductance_lists: " << new_neu.custom_leak_conductance_lists << LOG_ENDL;  exit(3); break; } // error
            new_neu.custom_leak_array[count] = value;
            count++;
        }
        if (count != new_neu.multiplier) {
            log(LOG_ERR) << "malformed file for custom_leak_conductance_lists not enough values in the file format with returns plz: " << new_neu.custom_leak_conductance_lists << LOG_ENDL;
            exit(3);
        }
    }

    //Check the compartments !!
    cJSON *aNeuronComps = cJSON_GetObjectItemCaseSensitive(oNeuronModel, "Compartments");
	if(cJSON_GetArraySize(aNeuronComps) < 1){
	    log(LOG_ERR) << "There should be at least one compartment in a NeuronModel" << LOG_ENDL;
		return false;
	}


	cJSON * oNeuronComp;
	cJSON_ArrayForEach(oNeuronComp, aNeuronComps) {
	    CompartmentInfo new_com;

	    //Inverse Capacitance
        cJSON *oInvCap = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "InverseCapacitance");
        if( cJSON_IsNumber(oInvCap) ){
            new_com.inverse_capacitance = oInvCap->valuedouble;
        }
        else{
            log(LOG_ERR) << "InverseCapacitance is a required numeric parameter" << LOG_ENDL;
            return false;
        }

        //Passive leak Conductivity
        cJSON *ogLeak = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "PassiveLeakConductivity");
        cJSON *ogLeakR = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "PassiveLeakConductivity_Randomization_offset");
        if( cJSON_IsNumber(ogLeak) ){
            new_com.passive_leak_conductivity = ogLeak->valuedouble;
            if( cJSON_IsNumber(ogLeakR)){
                new_com.passive_leak_conductivity_Randomization_offset = ogLeakR->valuedouble;
                log(LOG_INFO,0) << "Use passive leak randomization Enabled" << LOG_ENDL;
            }else {
                new_com.passive_leak_conductivity_Randomization_offset = 0;
            }
        }
        else{
            log(LOG_ERR) << "PassiveLeakConductivity is a required numeric parameter" << LOG_ENDL;
            return false;
        }

        //PassiveLeakInversionPotential
        cJSON *ovLeak = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "PassiveLeakInversionPotential");
        if( cJSON_IsNumber(ovLeak) ){
            new_com.passive_leak_inversion_potential = ovLeak->valuedouble;
        }
        else{
            log(LOG_ERR) << "PassiveLeakInversionPotential is a required numeric parameter" << LOG_ENDL;
            return false;
        }

        //ConductivityRatio
        cJSON *oGP = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "ConductivityRatio");
        if( cJSON_IsNumber(oGP) ){
            new_com.gp = oGP->valuedouble;
        }
        else{
            log(LOG_ERR) << "ConductivityRatio is a required numeric parameter" << LOG_ENDL;
            return false;
        }

        //InitialVoltage
        cJSON *ovInit = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "InitialVoltage");
        if( cJSON_IsNumber(ovInit) ){
            new_com.initial_voltage = ovInit->valuedouble;
        }
        else{
            log(LOG_ERR) << "InitialVoltage is a required numeric parameter" << LOG_ENDL;
            return false;
        }

        //CalciumConcentration
        cJSON *oCalciumConcentration = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "CalciumConcentration");
        if(cJSON_IsInvalid(oCalciumConcentration) || oCalciumConcentration == NULL){
            log(LOG_INFO,0) << "No valid calcium concentration on comp index: " << new_neu.series_compartments.size() << LOG_ENDL;
            new_com.enable_CalciumConcentration = false;
        }
        else{
            log(LOG_DEBUG,0) << "Calcium implementation" << LOG_ENDL;
            new_com.enable_CalciumConcentration = true;

            //select the memory
            GateInfo* new_CalciumConcentration = &new_com.CalciumConcentration;

            //alpha formula
            cJSON *oAlpha = cJSON_GetObjectItemCaseSensitive(oCalciumConcentration, "AlphaFormula");
            if(!ImportJsonFormula(oAlpha, new_CalciumConcentration->alpha_formula, log)){
                //todo label toevogen zodat het duidelijk is welke niet lukt
                log(LOG_ERR) << "Could not interpret gate alpha formula Calcium concentration" << LOG_ENDL;
                return false;
            }

            //beta formula
            cJSON *oBeta = cJSON_GetObjectItemCaseSensitive(oCalciumConcentration, "BetaFormula");
            if(!ImportJsonFormula(oBeta, new_CalciumConcentration->beta_formula, log)){
                //todo label toevogen zodat het duidelijk is welke niet lukt
                log(LOG_ERR) << "Could not interpret gate beta formula Calcium concentration" << LOG_ENDL;
                return false;
            }

            //CurrentGatingExponent
            cJSON *oGateExp = cJSON_GetObjectItemCaseSensitive(oCalciumConcentration, "CurrentGatingExponent");
            if(! (cJSON_IsNumber(oGateExp) && oGateExp->valuedouble == oGateExp->valueint && oGateExp->valueint >= 0) ){
                log(LOG_ERR) << "CurrentGatingExponent is a required non-negative integer parameter, Calcium concentration" << LOG_ENDL;
                return false;
            }
            new_CalciumConcentration->gating_power_factor = oGateExp->valueint;

            //CurrentGating
            cJSON *oGateCurrent = cJSON_GetObjectItemCaseSensitive(oCalciumConcentration, "CurrentGating");
            if(cJSON_IsString(oGateCurrent)){
                std::string GateCurrent = oGateCurrent->valuestring;
                if(GateCurrent == "GateVariable"){
                    new_CalciumConcentration->current_gating_variable = GateInfo::GATE_BY_VAR;
                } else if(GateCurrent == "BetaFunction"){
                    new_CalciumConcentration->current_gating_variable = GateInfo::GATE_BY_BETA;
                } else {
                    log(LOG_ERR) << "CurrentGating type unknown, Calcium concentration" << LOG_ENDL;
                    return false;
                }
            } else{
                log(LOG_ERR) << "CurrentGating is a required string parameter, Calcium concentration" << LOG_ENDL;
                return false;
            }

            //AlphaGating
            cJSON *oGateAlpha = cJSON_GetObjectItemCaseSensitive(oCalciumConcentration, "AlphaGating");
            if(cJSON_IsString(oGateAlpha)){
                std::string GateAlpha = oGateAlpha->valuestring;
                if(GateAlpha == "CompartmentVoltage"){
                    new_CalciumConcentration->alpha_dependency = GateInfo::ALPHA_OF_POTENTIAL;
                } else if(GateAlpha == "PreviousGateValue"){
                    new_CalciumConcentration->alpha_dependency = GateInfo::ALPHA_OF_PREVIOUS_GATE_VALUE;
                } else if(GateAlpha == "CalciumConcentration"){
                    new_CalciumConcentration->alpha_dependency = GateInfo::ALPHA_OF_PREVIOUS_GATE_VALUE; //ALPHA_OF_CALCIUM_CONCENTRATION
                } else {
                    log(LOG_ERR) << "AlphaGating tye unknown" << LOG_ENDL;
                    return false;
                }
            } else{
                log(LOG_ERR) << "AlphaGating is a required string parameter, Calcium concentration" << LOG_ENDL;
                return false;
            }

            //Dynamics
            cJSON *oDynamics = cJSON_GetObjectItemCaseSensitive(oCalciumConcentration, "Dynamics");
            if(cJSON_IsString(oDynamics)){
                std::string Dynamics = oDynamics->valuestring;
                if(Dynamics == "Classical"){
                    new_CalciumConcentration->value_dynamics = GateInfo::DYNAMICS_CLASSIC;
                } else if(Dynamics == "Ratio"){
                    new_CalciumConcentration->value_dynamics = GateInfo::DYNAMICS_RATIO;
                } else if(Dynamics == "ConcentratedCalcium"){
                    new_CalciumConcentration->value_dynamics = GateInfo::DYNAMICS_CONCENTRATED_CALCIUM;
                } else {
                    log(LOG_ERR) << "Dynamics tye unknown" << LOG_ENDL;
                    return false;
                }
            } else{
                log(LOG_ERR) << "Dynamics is a required string parameter, Calcium concentration" << LOG_ENDL;
                return false;
            }

            //InitialState
            cJSON *oIniVal = cJSON_GetObjectItemCaseSensitive(oCalciumConcentration, "InitialState");
            if(!(cJSON_IsNumber(oIniVal)) ){
                log(LOG_ERR) << "InitialState is a required string parameter, Calcium concentration" << LOG_ENDL;
                return false;
            }
            new_CalciumConcentration->initial_value = oIniVal->valuedouble;

        }

        //ION Channels
		cJSON *aChannels = cJSON_GetObjectItemCaseSensitive(oNeuronComp, "IonChannels");
		if( !cJSON_IsArray(aChannels) ){
            log(LOG_ERR) << "IonChannels is a required array of channel parms in a compartment" << LOG_ENDL;
			return false;
		}

		cJSON * oChannel;
		cJSON_ArrayForEach(oChannel, aChannels) {
			ChannelInfo new_cha;

			//-> LeakConductivity
			cJSON *ogLeak = cJSON_GetObjectItemCaseSensitive(oChannel, "LeakConductivity");
			cJSON *ogLeakR = cJSON_GetObjectItemCaseSensitive(oChannel, "LeakConductivity_Randomization_offset");
			if( cJSON_IsNumber(ogLeak) ){
				new_cha.leak_conductivity = ogLeak->valuedouble;
				if( cJSON_IsNumber(ogLeakR)){
                    new_cha.leak_conductivity_Randomization_offset = ogLeakR->valuedouble;
                    log(LOG_INFO,0) << "LeakConductivity randomization Enabled" << LOG_ENDL;
				}else {
                    new_cha.leak_conductivity_Randomization_offset = 0;
                }
			}else{
                log(LOG_ERR) << "LeakConductivity is a required numeric parameter" << LOG_ENDL;
				return false;
			}

			//->LeakInversionPotential
			cJSON *ovLeak = cJSON_GetObjectItemCaseSensitive(oChannel, "LeakInversionPotential");
			cJSON *ovLeakR = cJSON_GetObjectItemCaseSensitive(oChannel, "LeakInversionPotential_Randomization_offset");
			if( cJSON_IsNumber(ovLeak) ){
				new_cha.leak_inversion_potential = ovLeak->valuedouble;
                if( cJSON_IsNumber(ovLeakR)){
                    new_cha.leak_inversion_potential_Randomization_offset = ovLeakR->valuedouble;
                }else {
                    new_cha.leak_inversion_potential_Randomization_offset = 0;
                }
			}else{
                log(LOG_ERR) << "LeakInversionPotential is a required numeric parameter" << LOG_ENDL;
				return false;
			}

			//-> IsCalciumChannel
			cJSON *oCa2 = cJSON_GetObjectItemCaseSensitive(oChannel, "IsCalciumChannel");
			if( cJSON_IsBool(oCa2) ){
				new_cha.is_ca2plus_channel = (bool)(oCa2->valueint);
			}else{
                log(LOG_ERR) << "IsCalciumChannel is a required boolean parameter" << LOG_ENDL;
				return false;
			}

			//-> GATES

			cJSON *aChannels = cJSON_GetObjectItemCaseSensitive(oChannel, "Gates");
			if( cJSON_IsArray(aChannels) ){
				cJSON *oGate;
				cJSON_ArrayForEach(oGate, aChannels) {
					GateInfo new_gate;

					cJSON *oAlpha = cJSON_GetObjectItemCaseSensitive(oGate, "AlphaFormula");
					if(!ImportJsonFormula(oAlpha, new_gate.alpha_formula, log)){
                        log(LOG_ERR) << "TODO waar ? -> Could not interpret gate alpha formula" << LOG_ENDL;
						return false;
					}

					cJSON *oBeta = cJSON_GetObjectItemCaseSensitive(oGate, "BetaFormula");
					if(!ImportJsonFormula(oBeta, new_gate.beta_formula, log)){
                        log(LOG_ERR) << "TODO waar ? -> Could not interpret gate beta formula" << LOG_ENDL;
						return false;
					}

					//-> CurrentGatingExponent
					cJSON *oGateExp = cJSON_GetObjectItemCaseSensitive(oGate, "CurrentGatingExponent");
					if(! (cJSON_IsNumber(oGateExp) && oGateExp->valuedouble == oGateExp->valueint && oGateExp->valueint >= 0) ){
                        log(LOG_ERR) << "TODO waar ? -> CurrentGatingExponent is a required non-negative integer parameter" << LOG_ENDL;
						return false;
					}
					new_gate.gating_power_factor = oGateExp->valueint;

					//-> CurrentGating
					cJSON *oGateCurrent = cJSON_GetObjectItemCaseSensitive(oGate, "CurrentGating");
					if(cJSON_IsString(oGateCurrent)){
						std::string GateCurrent = oGateCurrent->valuestring;
						if(GateCurrent == "GateVariable"){
							new_gate.current_gating_variable = GateInfo::GATE_BY_VAR;
						} else if(GateCurrent == "BetaFunction"){
							new_gate.current_gating_variable = GateInfo::GATE_BY_BETA;
						} else {
                            log(LOG_ERR) << "TODO waar ? -> CurrentGating type unknown" << LOG_ENDL;
							return false;
						}
					} else{
                        log(LOG_ERR) << "TODO waar ? -> CurrentGating is a required string parameter" << LOG_ENDL;
						return false;
					}

					//-> AlphaGating
					cJSON *oGateAlpha = cJSON_GetObjectItemCaseSensitive(oGate, "AlphaGating");
					if(cJSON_IsString(oGateAlpha)){
						std::string GateAlpha = oGateAlpha->valuestring;
						if(GateAlpha == "CompartmentVoltage"){
							new_gate.alpha_dependency = GateInfo::ALPHA_OF_POTENTIAL;
						} else if(GateAlpha == "PreviousGateValue"){
							new_gate.alpha_dependency = GateInfo::ALPHA_OF_PREVIOUS_GATE_VALUE;
                        } else if(GateAlpha == "CalciumConcentration"){
                            new_gate.alpha_dependency = GateInfo::ALPHA_OF_CALCIUM_CONCENTRATION;
                        }
						else {
                            log(LOG_ERR) << "TODO waar ? -> AlphaGating type unknown" << LOG_ENDL;
							return false;
						}
					} else{
                        log(LOG_ERR) << "TODO waar ? -> AlphaGating is a required string parameter" << LOG_ENDL;
						return false;
					}

					//-> Dynamics
					cJSON *oDynamics = cJSON_GetObjectItemCaseSensitive(oGate, "Dynamics");
					if(cJSON_IsString(oDynamics)){
						std::string Dynamics = oDynamics->valuestring;
						if(Dynamics == "Classical"){
							new_gate.value_dynamics = GateInfo::DYNAMICS_CLASSIC;
						} else if(Dynamics == "Ratio"){
							new_gate.value_dynamics = GateInfo::DYNAMICS_RATIO;
						} else if(Dynamics == "ConcentratedCalcium"){
							new_gate.value_dynamics = GateInfo::DYNAMICS_CONCENTRATED_CALCIUM;
						} else {
                            log(LOG_ERR) << "TODO waar ? -> Dynamics type unknown" << LOG_ENDL;
							return false;
						}
					} else{
                        log(LOG_ERR) << "TODO waar ? -> Dynamics is a required string parameter" << LOG_ENDL;
						return false;
					}

					cJSON *oIniVal = cJSON_GetObjectItemCaseSensitive(oGate, "InitialState");
					if(! (cJSON_IsNumber(oIniVal)) ){
                        log(LOG_ERR) << "TODO waar ? -> InitialState is a required numeric parameter" << LOG_ENDL;
						return false;
					}
					new_gate.initial_value = oIniVal->valuedouble;

                    //Push back into sim discription
					new_cha.multiplied_gates.push_back(new_gate);
				}
			}
			else{
                log(LOG_ERR) << "TODO waar ? -> Gates is a required array of gate parms in a channel" << LOG_ENDL;
				return false;
			}

            //Push back into sim discription
			new_com.ion_channels.push_back(new_cha);
		}
		//Push back into sim discription
		new_neu.series_compartments.push_back(new_com);
	}
	return true;
}
bool ImportJsonCellPopulation(cJSON *oPopModel, SimRunInfo &sim, process_info PROC){
	if (!cJSON_IsObject(oPopModel)){
	    sim.log(LOG_ERR) << "CellPopulation should be an object with properties" << LOG_ENDL;
		return false;
	}

    cJSON *aNeuronModels = cJSON_GetObjectItemCaseSensitive(oPopModel, "NeuronModels");
    if(!cJSON_IsArray(aNeuronModels)){
        sim.log(LOG_ERR) <<  "NeuronModels in cell population model should be an array of neuron models, plz encaptulate in squeare brakets" << LOG_ENDL;
        return false;
    }

    int len = cJSON_GetArraySize(aNeuronModels);
    if(len <= 0){
        sim.log(LOG_ERR) <<  "Cell Population definition needs at least one cell" << LOG_ENDL;
    }
    sim.population.neuron_models.resize(len);

    cJSON *oNeuronModel;
    int neu_count = 0;
    int comp_count = 0;
    int cell_count = 0;
    int channel_count = 0;
    int gate_count = 0;
    cJSON_ArrayForEach(oNeuronModel, aNeuronModels) {
        neu_count++;
        if(ImportJsonNeuronModel(oNeuronModel, sim.population.neuron_models[neu_count - 1],PROC)){
            cell_count += sim.population.neuron_models[neu_count -1].multiplier;
            comp_count += (sim.population.neuron_models[neu_count -1].series_compartments.size() * sim.population.neuron_models[neu_count -1].multiplier);

            size_t tempchan = 0;
            size_t tempgate = 0;
            for(size_t i = 0; i < sim.population.neuron_models[neu_count -1].series_compartments.size(); i++){
                tempchan += sim.population.neuron_models[neu_count -1].series_compartments[i].ion_channels.size();
                for(size_t j = 0; j <sim.population.neuron_models[neu_count -1].series_compartments[i].ion_channels.size(); j++){
                    tempgate += sim.population.neuron_models[neu_count -1].series_compartments[i].ion_channels[j].multiplied_gates.size();
                }
            }
            channel_count += tempchan * sim.population.neuron_models[neu_count-1].multiplier;
            gate_count    += tempgate * sim.population.neuron_models[neu_count-1].multiplier;
        }
        else{
            sim.log(LOG_ERR) <<  "Could not make sense of NeuronModel no: " << neu_count << LOG_ENDL;
            return false;
        }
    }

    sim.population.type = CellPopulationInfo::SET;
    sim.population.population_size = cell_count;
    sim.population.comparment_count = comp_count;
    sim.population.channel_count = channel_count;
    sim.population.gate_count = gate_count;

    sim.log(LOG_INFO,0)<<"Set CellPopulation to Explicit,"<<cell_count<< " cells, " <<comp_count<< " compartments, " << channel_count << " channels, " << gate_count << " gates" << LOG_ENDL;
    return true;
}
bool ReadSimRunDefinition(const char *filename, SimRunInfo &sim,process_info ProcessInfo){
    miniLogger log(std::cout, __FUNCTION__,ProcessInfo.world_rank);
    log.set_Loglevel(LOG_WARN);

    //declaration of variables used
	FILE *file = NULL;
	char *filedata = NULL;
	size_t file_size = 0;
	size_t read_result = -1;
	cJSON *oRoot = NULL;
	log(LOG_INFO,0)<<"Reading file :" << filename << LOG_ENDL;

	// open file as bytes
	file = fopen(filename, "rb");
	if (!file){
		log(LOG_ERR) << "Can't read Configuration file" << LOG_ENDL;
		goto CLEANUP;
	}
	// get size
	if (fseek(file, 0, SEEK_END) != 0) goto CLEANUP;
	file_size = ftell(file);
	if (file_size < 0) goto CLEANUP;
	if (fseek(file, 0, SEEK_SET) != 0) goto CLEANUP;
	// load the file data
	filedata = (char *) malloc( file_size + 1 );
	if (!filedata) goto CLEANUP;
	// read the file into memory
	read_result = fread(filedata, sizeof(char), file_size, file);
	if (read_result != file_size){
		log(LOG_ERR) << "Configuration file incorrectly read" << LOG_ENDL;
	    goto CLEANUP;
	}
	filedata[read_result] = '\0';

	oRoot = cJSON_Parse(filedata);
	if(!oRoot){
        log(LOG_ERR) << "Could not parse JSON file" << LOG_ENDL;
        const char *error_ptr = cJSON_GetErrorPtr();
		if (error_ptr){
			size_t bytesfrom = error_ptr - filedata;
            log(LOG_ERR) << "Error in char: " << bytesfrom << LOG_ENDL;
            log(LOG_ERR) << "before: " << error_ptr << LOG_ENDL;
        }
		goto CLEANUP;
	}
	//-> get simetime
    {
        cJSON *oSimTime = cJSON_GetObjectItemCaseSensitive(oRoot, "SimTime");
        if (oSimTime) {
            if (cJSON_IsNumber(oSimTime)) {
                const float newtime = (float) oSimTime->valuedouble;
                if (sim.setTime(newtime)) {
                    sim.log(LOG_INFO,0) << "simtime: " << newtime << LOG_ENDL;
                } else {
                    sim.log(LOG_ERR) << "simetime: " << oSimTime->valuedouble << " out of range" << LOG_ENDL;
                }
            } else {
                log(LOG_ERR,0) << "file: " << filename << " SimTime parameter should be a number" << LOG_ENDL;
            }
        }
    }
	//-> get SimTimestep
    {
        cJSON *oSimTimestep = cJSON_GetObjectItemCaseSensitive(oRoot, "SimTimestep");
        if (oSimTimestep) {
            if (cJSON_IsNumber(oSimTimestep) && sim.setTimestep(oSimTimestep->valuedouble)) {
                sim.log(LOG_INFO,0) << "SimTimestep: " << oSimTimestep->valuedouble << LOG_ENDL;
            } else {
                log(LOG_ERR) << "SimTimestep parameter should be a valid number" << LOG_ENDL;
            }
        }
        else{
            log(LOG_ERR,0) << "file: " << filename << " SimTimestep parameter should be a number" << LOG_ENDL;
        }
    }
	//-> get Number of GPU's
    {
        sim.GPU_backend = false;
        cJSON *oGPU_enable = cJSON_GetObjectItemCaseSensitive(oRoot, "GPU_backend");
        if(cJSON_IsBool(oGPU_enable)) {
            if (oGPU_enable->valueint) {
                sim.GPU_backend = oGPU_enable->valueint;
                sim.log(LOG_INFO, 0) << "GPUs selected: " << sim.GPU_backend << LOG_ENDL;
            } else {
                sim.GPU_backend = false;
                sim.log(LOG_INFO,0) << "CPU selected" << LOG_ENDL;
            }
        }
        else if(cJSON_IsNumber(oGPU_enable)) {
            if (oGPU_enable->valueint) {
                if(oGPU_enable->valueint > 0) {
                    sim.GPU_backend = oGPU_enable->valueint;
                    sim.log(LOG_INFO, 0) << "GPUs selected: " << sim.GPU_backend << LOG_ENDL;
                } else {
                    sim.GPU_backend = false;
                    sim.MPThreads = oGPU_enable->valueint * -1;
                    sim.log(LOG_INFO, 0) << "CPU threads selected: " << sim.MPThreads << LOG_ENDL;
                }
            } else {
                sim.GPU_backend = false;
                sim.log(LOG_INFO,0) << "CPU selected" << LOG_ENDL;
            }
        }
        else if(!cJSON_IsNull(oGPU_enable)){
            log(LOG_INFO,0) << "No GPU_enable default to CPU usage" << LOG_ENDL;
        }
    }
//	Get MPI setting !
    {
        sim.MPI_alltoallv_enable = false;
        cJSON *oMPI_alltoallv = cJSON_GetObjectItemCaseSensitive(oRoot, "MPI_alltoallv");
        if(cJSON_IsBool(oMPI_alltoallv)) {
            if (oMPI_alltoallv->valueint) {
                sim.MPI_alltoallv_enable =  true;
                sim.log(LOG_INFO,0) << "MPI alltoallv selected" << LOG_ENDL;
            } else {
                sim.MPI_alltoallv_enable = false;
                sim.log(LOG_INFO,0) << "MPI allgather selected" << LOG_ENDL;
            }
        }
    }

    //	Get MPI setting !
    {
        sim.GPU_Time_kernels = false;
        cJSON *oGPU_Time_kernels = cJSON_GetObjectItemCaseSensitive(oRoot, "GPU_Time_kernels");
        if(cJSON_IsBool(oGPU_Time_kernels)) {
            if (oGPU_Time_kernels->valueint) {
                sim.GPU_Time_kernels =  true;
                sim.log(LOG_INFO,0) << "GPU time kernels true" << LOG_ENDL;
            } else {
                sim.GPU_Time_kernels = false;
                sim.log(LOG_INFO,0) << "GPU time kernels false" << LOG_ENDL;
            }
        }
    }

//    "OrderedBlock" : (BOOL)
    {
        sim.OrderedBlock = false;
        cJSON *oOrderedBlock = cJSON_GetObjectItemCaseSensitive(oRoot, "OrderedBlock");
        if(cJSON_IsBool(oOrderedBlock)) {
            if (oOrderedBlock->valueint) {
                sim.OrderedBlock =  true;
                sim.log(LOG_INFO,0) << "OrderedBlock enabled" << LOG_ENDL;
            } else {
                sim.OrderedBlock = false;
                sim.log(LOG_INFO,0) << "OrderedBlock disabled" << LOG_ENDL;
            }
        }
    }

//    "MPthreads": (int)
    {
        sim.MPThreads = 0;
        cJSON *oMPthreads = cJSON_GetObjectItemCaseSensitive(oRoot, "MPThreads");
        if(cJSON_IsNumber(oMPthreads)) {
            if (oMPthreads->valueint > 0) {
                sim.MPThreads =  oMPthreads->valueint;
                sim.log(LOG_INFO,0) << "MPthreads is set to use " <<sim.MPThreads  <<" threads" << LOG_ENDL;
            } else {
                sim.MPThreads = false;
                sim.log(LOG_INFO,0) << "MPThreads is set to auto" << LOG_ENDL;
            }
        }
    }

	//-> Get Connectivity Model
	{
        sim.setConnectivityNone();
        cJSON *oConnModel = cJSON_GetObjectItemCaseSensitive(oRoot, "ConnectivityModel");
        if(oConnModel){
            if(ImportJsonConnModel(oConnModel, sim)){
                //it's done and logged
            }
            else{
                log(LOG_WARN) << "file: " << filename << " Could not make sense of ConnectivityModel which is provided" << LOG_ENDL;
            }
	    }else{
            log(LOG_WARN) << "file: " << filename << " No Connectivity Model provided" << LOG_ENDL;
        }
	}

    //-> Get StimulusModel
	{
        cJSON *oStimModel = cJSON_GetObjectItemCaseSensitive(oRoot, "StimulusModel");
        if(oStimModel) {
            if (ImportJsonStimModel(oStimModel, sim)) {
                //it's done and logged
            } else {
                log(LOG_WARN) << "Could not make sense of StimulusModel" << LOG_ENDL;
            }
        }else{
            log(LOG_WARN) << "file: " << filename << "No Stimulus Model provided" << LOG_ENDL;
        }
	}

	//-> Get OutputMonitor
    {
        cJSON *oMonitorModel = cJSON_GetObjectItemCaseSensitive(oRoot, "OutputMonitor");
        if(oMonitorModel){
            if(ImportOutputmonitorModel (oMonitorModel, sim)){
                //it's done and logged
            }
            else{
                log(LOG_WARN) << "Could not make sense of OutputMonitor" << LOG_ENDL;
            }
        }
        else{
            log(LOG_WARN) << "No OutputMonitor Model provided" << LOG_ENDL;
        }
        sim.output_monitor.OpenFiles(ProcessInfo.world_rank,ProcessInfo.MPI);
    }


    //-> META_OutputData MONITOR
    {
        cJSON *oMetaDataModel = cJSON_GetObjectItemCaseSensitive(oRoot, "MetaDataMonitor");
        if(oMetaDataModel){
            if(ImportMetadatamonitorModel (oMetaDataModel, sim)){
                //it's done and logged
            }
            else{
                log(LOG_WARN) << "Could not make sense of MetaDataMonitor" << LOG_ENDL;
            }
        }
        else{
            log(LOG_WARN) << "No MetaDataMonitor provided" << LOG_ENDL;
        }
    }

	//-> CellPopulation
	{
        cJSON *oPopModel = cJSON_GetObjectItemCaseSensitive(oRoot, "CellPopulation");
        if(oPopModel){
            if(ImportJsonCellPopulation(oPopModel, sim,ProcessInfo)){
                //it's done and logged
            }
            else{
                log(LOG_WARN) << "Could not make sense of CellPopulation" << LOG_ENDL;
            }
        }
	}

CLEANUP:
	if(oRoot){
		cJSON_Delete(oRoot);
		oRoot = NULL;
	}
	if(filedata){
		free(filedata);
		filedata = nullptr;
	}
	if(file){
		fclose(file);
		file = nullptr;
	}
	return true;
}


bool WriteMetaData_json(FILE *fout, const RunMetaData &md){

    bool ok = false;
    char *json_text = nullptr;
    cJSON *oRoot;

    oRoot = cJSON_CreateObject();

    if(cJSON_AddStringToObject(oRoot,"Process Name", md.Proces_name) == nullptr) goto CLEANUP;
    if(cJSON_AddStringToObject(oRoot,"CPU", md.metaCPU.name) == nullptr) goto CLEANUP;

#if ORDEREDBLOCK == 1
    if (cJSON_AddBoolToObject(oRoot, "ORDEREDBLOCK", true) == nullptr) goto CLEANUP;
#else
    if (cJSON_AddBoolToObject(oRoot, "ORDEREDBLOCK", false) == nullptr) goto CLEANUP;
#endif

    //numbers
    cJSON *oNetwork;
    cJSON_AddItemToObject(oRoot,"Network", oNetwork = cJSON_CreateObject());
    if(cJSON_AddNumberToObject(oNetwork, "Steps",md.Steps) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nCells",md.nCells) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nCells_l",md.nCells_l) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nCompartments_l",md.nCompartments_l) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nChannels_l",md.nChannels_l) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nGates_l",md.nGates_l) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nCells_u",md.nCells_u) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nCompartments_u",md.nCompartments_u) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nChannels_u",md.nChannels_u) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "nGates_u",md.nGates_u) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "NoConnections_local", md.NOConnections) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "NoConnections_local_percell", md.NOConnections/md.nCells_l) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "Target_density", md.target_density) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(oNetwork, "Actual_density", md.actual_density) == nullptr) goto CLEANUP;
    if(cJSON_AddStringToObject(oNetwork, "Connection_type", md.ConnType) == nullptr) goto CLEANUP;
    //timing
    cJSON *otiming;
    cJSON_AddItemToObject(oRoot,"Timing", otiming = cJSON_CreateObject());

    if(cJSON_AddNumberToObject(otiming,"SimulationTimeFull_ms", md.SimulationTimeFull_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"Networkinitializations_ms", md.Networkinitializations_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"SynapticNetworkGenerationTime_ms", md.NetworkSynapticNetworkGenerationTime_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"Timesteps", md.Timsteps_ms) == nullptr) goto CLEANUP;

    if(cJSON_AddNumberToObject(otiming,"OutputWriteTime_ms", md.OutputWriteTime_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"ComputeLaunchTime_ms", md.ComputeLaunchTime_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"MPICommunicationTime_ms", md.MPICommunicationTime_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"LaunchWriteThread_ms", md.LaunchWriteThread_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"Synchronizingtheloop1_ms", md.Synchronizingtheloop1_ms) == nullptr) goto CLEANUP;
    if(cJSON_AddNumberToObject(otiming,"Synchronizingtheloop2_ms", md.Synchronizingtheloop2_ms) == nullptr) goto CLEANUP;

    cJSON *oBackend;
    cJSON_AddItemToObject(oRoot,"Backend", oBackend = cJSON_CreateObject());
    if(md.type == RunMetaData::CPU) {
        if(cJSON_AddStringToObject(oBackend,"type", "CPU") == nullptr) goto CLEANUP;
        if(cJSON_AddStringToObject(oBackend,"name", md.metaCPU.name) == nullptr) goto CLEANUP;
        if(cJSON_AddNumberToObject(oBackend,"NumThreads", md.metaCPU.NumThreads) == nullptr) goto CLEANUP;
        {
            cJSON *obtiming;
            cJSON_AddItemToObject(oBackend, "KernelTiming", obtiming = cJSON_CreateObject());
            if(cJSON_AddNumberToObject(obtiming,"Computetime_ms", md.metaCPU.Computetime_ms) == nullptr) goto CLEANUP;
        }
        {
            //memory
            cJSON *omemory;
            cJSON_AddItemToObject(oBackend,"Memory", omemory = cJSON_CreateObject());
            if(cJSON_AddStringToObject(omemory,"unit", "kB") == nullptr) goto CLEANUP;
            if(cJSON_AddNumberToObject(omemory,"total", md.metaCPU.peak_resident_memory_bytes) == nullptr) goto CLEANUP;
            if(cJSON_AddNumberToObject(omemory,"used", md.metaCPU.current_resident_memory_bytes) == nullptr) goto CLEANUP;
        }
    }


    if(md.type == RunMetaData::GPU){
        if(cJSON_AddStringToObject(oBackend,"type", "GPU") == nullptr) goto CLEANUP;
        if(cJSON_AddNumberToObject(oBackend,"Amount", md.metaGPU.NumGPUS) == nullptr) goto CLEANUP;
        for(size_t i = 0; i < md.metaGPU.NumGPUS; i++){
            cJSON *oSgpu;
            std::string tmp = md.metaGPU.sGPU_Meta[i].name;
            tmp += "  id: "+ std::to_string(i);
            cJSON_AddItemToObject(oBackend,tmp.c_str(), oSgpu = cJSON_CreateObject());
            if(cJSON_AddNumberToObject(oSgpu, "clockRate",md.metaGPU.sGPU_Meta[i].clockRate) == nullptr) goto CLEANUP;
            if(cJSON_AddNumberToObject(oSgpu, "major",md.metaGPU.sGPU_Meta[i].major) == nullptr) goto CLEANUP;
            if(cJSON_AddNumberToObject(oSgpu, "minor",md.metaGPU.sGPU_Meta[i].minor) == nullptr) goto CLEANUP;
            {
                cJSON *obtiming;
                cJSON_AddItemToObject(oSgpu, "KernelTiming", obtiming = cJSON_CreateObject());
                if(cJSON_AddNumberToObject(obtiming, "time_Icalc_comp_ms",md.metaGPU.sGPU_Meta[i].time_Icalc_comp) == nullptr) goto CLEANUP;
                if(cJSON_AddNumberToObject(obtiming, "time_Icalc_gate_ms",md.metaGPU.sGPU_Meta[i].time_Icalc_gate) == nullptr) goto CLEANUP;
                if(cJSON_AddNumberToObject(obtiming, "time_Igap_ms",md.metaGPU.sGPU_Meta[i].time_Igap) == nullptr) goto CLEANUP;
                if(cJSON_AddNumberToObject(obtiming, "time_dev_copy_ms",md.metaGPU.sGPU_Meta[i].time_dev_copy) == nullptr) goto CLEANUP;
                if(cJSON_AddNumberToObject(obtiming, "time_p2p_copy_ms",md.metaGPU.sGPU_Meta[i].time_p2p_copy) == nullptr) goto CLEANUP;
            }
            {
                //memory
                cJSON *memory;
                cJSON_AddItemToObject(oSgpu,"Memory", memory = cJSON_CreateObject());
                if(cJSON_AddNumberToObject(memory, "memsizeused",md.metaGPU.sGPU_Meta[i].memsizeused) == nullptr) goto CLEANUP;
                if(cJSON_AddNumberToObject(memory, "totalGlobalMem",md.metaGPU.sGPU_Meta[i].totalGlobalMem) == nullptr) goto CLEANUP;
            }
        }
    }

    {
        cJSON *oMPI;
        cJSON_AddItemToObject(oRoot, "MPI", oMPI = cJSON_CreateObject());
        if (md.Alltoallv) {
            if (cJSON_AddStringToObject(oMPI, "type", "Alltoallv") == nullptr) goto CLEANUP;
        } else {
            if (cJSON_AddStringToObject(oMPI, "type", "AllGather") == nullptr) goto CLEANUP;
        }
#ifdef MPI_CUDA_AWARE
        if (cJSON_AddBoolToObject(oMPI, "CUDA_Aware", true) == nullptr) goto CLEANUP;
#else
        if (cJSON_AddBoolToObject(oMPI, "CUDA_Aware", false) == nullptr) goto CLEANUP;
#endif
        if (cJSON_AddNumberToObject(oMPI, "MPI_size", md.NoMPIprocesses) == nullptr) goto CLEANUP;
        if (cJSON_AddNumberToObject(oMPI, "MPI_rank", md.NoMPIprocess) == nullptr) goto CLEANUP;
        if (cJSON_AddNumberToObject(oMPI, "xcell_size_global", md.xcell_size_global) == nullptr) goto CLEANUP;
        if (cJSON_AddNumberToObject(oMPI, "xcell_size_local", md.xcell_size_local) == nullptr) goto CLEANUP;
        if (cJSON_AddNumberToObject(oMPI, "NoXternalConnections", md.NoXternalConnections) == nullptr) goto CLEANUP;
        if(md.Alltoallv) {
            for (int i = 0; i < md.NoMPIprocesses; i++) {
                if (i == md.NoMPIprocess) continue;
                cJSON *oMPIit;
                std::string tmp = "send to world_rank: " + std::to_string(i);
                cJSON_AddItemToObject(oMPI, tmp.c_str(), oMPIit = cJSON_CreateObject());
                if (cJSON_AddNumberToObject(oMPIit, "xcell_count_pp", md.xcell_count_pp[i]) == nullptr) goto CLEANUP;
                if (cJSON_AddNumberToObject(oMPIit, "xcell_count_pp_g", md.xcell_count_pp_g[i]) == nullptr) goto CLEANUP;
            }
        }
    }

    json_text = cJSON_Print(oRoot);
    if(!json_text) {
        fprintf(stderr, "Failed to serialize metadata JSON, somehow.\n");
        goto CLEANUP;
    }

    if(!(fout && fputs(json_text,fout) > 0 )){
        ok = false;
        goto CLEANUP;
    }
    ok  = true;

    CLEANUP:
    if(json_text) free(json_text);
    if(oRoot) cJSON_Delete(oRoot);
    return ok;
}
