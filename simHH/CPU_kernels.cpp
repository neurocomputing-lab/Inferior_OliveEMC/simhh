//
// Created by max on 22-03-21.
// todo: in general make it faster please
//

#include "CPU_kernels.h"
#include "initializations.h"  // for initializations
#include <cmath>              // for math
#include <cstring>            // std::memcpy

#define DEBUGK 0

namespace CPU_kernel_functions{
    float fn_Igap     (const uint32_t cellid,const float weight, const float *vsGAP, const CellConstStruct* neighConns, int cellid_offset){
        const float VSN = vsGAP[cellid + cellid_offset];
        const uint32_t neighcountt = neighConns[cellid].NeighCount;
        const uint64_t *ids =  neighConns[cellid].NeighIds;
        float fAcc = 0;
        float vAcc = 0;
        for (uint32_t x = 0; x < neighcountt; x++) {

            const float vDiff = VSN - vsGAP[ids[x]];
            fAcc += vDiff * weight * expf(vDiff * vDiff * (-1.0 / 100.0));
            vAcc += weight * vDiff;

        }
        return 0.8 * fAcc + 0.2 * vAcc;
    }
    float fn_Igap_condu     (const uint32_t cellid, const float *vsGAP, const CellConstStruct* neighConns, int cellid_offset){
        const float VSN = vsGAP[cellid + cellid_offset];
        const uint32_t neighcountt = neighConns[cellid].NeighCount;
        const uint64_t *ids =  neighConns[cellid].NeighIds;
        const float *neigcondu =  neighConns[cellid].NeighCondu;
        float fAcc = 0;
        float vAcc = 0;
        for (uint32_t x = 0; x < neighcountt; x++) {
            const float vDiff = VSN - vsGAP[ids[x]];
            fAcc += vDiff * neigcondu[x] * expf(vDiff * vDiff * (-1.0 / 100.0));
            vAcc += neigcondu[x] * vDiff;
        }
        return 0.8 * fAcc + 0.2 * vAcc;
    }
    float fn_Igap_full(const uint32_t cellid,const float weight, const float *vsGAP, const uint32_t nCells, int cellid_offset){
        const float V = vsGAP[cellid + cellid_offset];
        float fAcc = 0;
        float vAcc = 0;
        for (uint32_t i = 0; i < nCells; i++) {
            const float vDiff = V - vsGAP[i];                               //a lot of uncoalled accesesssss.
            fAcc += vDiff * weight * expf(vDiff * vDiff * (-1.0 / 100.0));
            vAcc += weight * vDiff;
        }
        return 0.8 * fAcc + 0.2 * vAcc;
    }

    float fCustom_nieuw_a(const uint32_t fType, const float v,const GateConstStruct *gateConstStruct) {
        const float vDiff =  gateConstStruct->aX[1] - v;
        const float vDiff2 = gateConstStruct->aX[6] - v;
        const float z1 = gateConstStruct->aX[2] * vDiff;
        const float z2 = gateConstStruct->aX[5] * vDiff2;
        float num, denum;
        if(fType == 0){
            num = gateConstStruct->aX[5] * vDiff;
            denum = gateConstStruct->aX[0] * expf(z1) + gateConstStruct->aX[3];
        }else if(fType == 1){
            num = gateConstStruct->aX[8];
            denum = gateConstStruct->aX[0] * expf(z1) + gateConstStruct->aX[3] + gateConstStruct->aX[4] * expf(z2) + gateConstStruct->aX[7];
        }else if(fType == 2){
            num = gateConstStruct->aX[0] * expf(z1) + gateConstStruct->aX[3];
            denum = gateConstStruct->aX[4] * expf(z2) +gateConstStruct->aX[7];
        }else{
            return minSS(z1, gateConstStruct->aX[0]);
        }
        if(denum == 0) denum = 0.000000000000001;
        if (fType != 1) return num/denum +  gateConstStruct->aX[8];
        return num / denum;
    }
    float fCustom_nieuw_b(const uint32_t fType, const float v,const GateConstStruct *gateConstStruct) {
        const float bx0 =  gateConstStruct->bX[0];
        const float bx3 =  gateConstStruct->bX[3];
        const float bx4 =  gateConstStruct->bX[4];
        const float bx7 =  gateConstStruct->bX[7];
        const float bx8 =  gateConstStruct->bX[8];

        const float vDiff =  gateConstStruct->bX[1] - v;
        const float vDiff2 = gateConstStruct->bX[6] - v;
        const float z1 = gateConstStruct->bX[2] * vDiff;
        const float z2 = gateConstStruct->bX[5] * vDiff2;
        float num;
        float denum;
        if(fType == 0){
            num = gateConstStruct->bX[5] * vDiff;
            denum = bx0 * expf(z1) + bx3;
        }
        else if(fType == 1){
            num = bx8;
            denum = bx0 * expf(z1) + bx3 + bx4 * expf(z2) + bx7;
        }
        else if(fType == 2){
            num = bx0 * expf(z1) + bx3;
            denum = bx4 * expf(z2) + bx7;
        }else {
            return minSS(z1, bx0);
        }
        if(denum == 0) denum = 0.000000000000001;
        if (fType != 1) return num/denum + bx8;
        return num / denum;
    }
    float fn_iInteract(float gInt, const uint32_t Identifier, const float V, const float gp, float *gpNext, float *vsNow){
        float iCompPrev = 0;
        float iCompNext = 0;
        if(ORDEREDBLOCK){
            if (Identifier == 0 || Identifier == 1)
                iCompNext = (V - vsNow[BLOCKSIZE]) * gInt / (1 - *gpNext);
            if (Identifier == 1 || Identifier == 2)
                iCompPrev = (V - vsNow[-BLOCKSIZE]) * gInt / gp;
        }
        else{
            if (Identifier == 0 || Identifier == 1)
                iCompNext = (V - vsNow[1]) * gInt / (1 - *gpNext);
            if (Identifier == 1 || Identifier == 2)
                iCompPrev = (V - vsNow[-1]) * gInt / gp;
        }
        return iCompNext + iCompPrev;

    }
    float fn_ileak(const float gLeak, const float V, const float Vleak){
        return  gLeak * (V - Vleak);
    }
    float fn_iApp(const uint32_t step, const StimulusInfo *stimulusInfo, const bool enable, const uint32_t cellid, float *ou_noise, XorShiftMul *rand, float rand_glob) {
        if (enable) {
            if (stimulusInfo->type == StimulusInfo::DCCURRENTPULSEMODULO) {
                if ((step >= stimulusInfo->dcCurrentPulseModulo.start_step) & (step < stimulusInfo->dcCurrentPulseModulo.end_step)) {
                    return (stimulusInfo->dcCurrentPulseModulo.base + stimulusInfo->dcCurrentPulseModulo.modulo_scale * ((cellid) % (stimulusInfo->dcCurrentPulseModulo.modulo)));
                }
            }
            else if (stimulusInfo->type == StimulusInfo::DCCURRENTSTEPANDHOLD) {
                if ((step >= stimulusInfo->dcCurrentStepandHold.start_step) & (step < stimulusInfo->dcCurrentStepandHold.end_step)) {
                    return stimulusInfo->dcCurrentStepandHold.stepcurrent;
                }
                else if  ((step >= stimulusInfo->dcCurrentStepandHold.end_step) & (step < stimulusInfo->dcCurrentStepandHold.end_hold)) {
                    return stimulusInfo->dcCurrentStepandHold.holdcurrent;
                }else{
                    return stimulusInfo->dcCurrentStepandHold.basecurrent;
                }
            }
            else if (stimulusInfo->type == StimulusInfo::DCCURRENTRAMPANDHOLD){
                if ((step >= stimulusInfo->dcCurrentRampandHold.start_ramp) & (step < stimulusInfo->dcCurrentRampandHold.end_ramp)) {
                    return stimulusInfo->dcCurrentRampandHold.rampcurrent - ((stimulusInfo->dcCurrentRampandHold.rampcurrent - stimulusInfo->dcCurrentRampandHold.basecurrent)/((float) ((stimulusInfo->dcCurrentRampandHold.end_ramp - stimulusInfo->dcCurrentRampandHold.start_ramp) * step)));
                } else if ((step >= stimulusInfo->dcCurrentRampandHold.end_ramp) & (step < stimulusInfo->dcCurrentRampandHold.end_hold)) {
                    return stimulusInfo->dcCurrentStepandHold.holdcurrent;
                } else {
                    return stimulusInfo->dcCurrentStepandHold.basecurrent;
                }
            }
            else if (stimulusInfo->type == StimulusInfo::DCCURRENTPULSECELL) {
                if (stimulusInfo->dcCurrentPulseCell.Cell_id == cellid) {
                    if ((step >= stimulusInfo->dcCurrentPulseCell.start_step) & (step < stimulusInfo->dcCurrentPulseCell.end_step)) {
                        return stimulusInfo->dcCurrentPulseCell.pulse;
                    }
                } else {
                    return stimulusInfo->dcCurrentPulseCell.base;
                }
            }
            else if (stimulusInfo->type == StimulusInfo::ORNSTEINUHLENBECK) {
                float theta = stimulusInfo->ornsteinUhlenbeck.theta;
                float sigma = stimulusInfo->ornsteinUhlenbeck.sigma;
                float mu = stimulusInfo->ornsteinUhlenbeck.mu;
                float alpha = stimulusInfo->ornsteinUhlenbeck.alpha;
                float dt = stimulusInfo->ornsteinUhlenbeck.dt;
                float sqrt_dt = stimulusInfo->ornsteinUhlenbeck.sqrt_dt;
                float Iapp_global = alpha*sigma*sqrt_dt*rand_glob;
                uint64_t random = rand[cellid].Get() % rand[cellid].sample_scale;
                float random_normal = (float)random / (float)rand[cellid].max_rand_value;
//                std::cout << theta << "\t" << sigma << "\t" << mu << "\t" << alpha << "\t" << dt << "\t" << sqrt_dt << "\t" << random_normal<< "\t" << random << "\t" << rand[cellid].sample_scale << "\t" << rand[cellid].max_rand_value <<"\n";
                ou_noise[cellid]  +=  theta*(mu-ou_noise[cellid])*dt + (1-alpha)*sigma*sqrt_dt*random_normal + alpha*Iapp_global;
                return ou_noise[cellid];
            }
        }
        return 0;
    }
    float fn_UpdateYS(const GateConstStruct *gateConstStruct, const float V, const float dt, const float YSN, const float CalciumLevel) {
        float z1;
        const float beta = fCustom_nieuw_b(gateConstStruct->bFtype & mask1,V,gateConstStruct);
        if ((gateConstStruct->aFtype & mask1) == 3) {        //vind dit niet een mooie oplossing tbh
            z1 = CalciumLevel;
        } else {
            z1 = V;
        }
        const float alpha = fCustom_nieuw_a(gateConstStruct->aFtype & mask1,z1,gateConstStruct);
    #if DEBUGK == 1
        float dydt = 0;
        if (beta && (gateConstStruct->aFtype & mask2) == 4) {
            dydt = (alpha - YSN) / beta;
        } else {
            dydt = (1 - YSN) * alpha - YSN * beta;
        }
        const float dit = YSN + dt * dydt;
        if(std::isinf(dit)) {printf("dit %f YSN %f alpha %f beta %f V %f dydt %f , Calcium %f\n",dit,YSN,alpha,beta,V,dydt,CalciumLevel);}
        if(std::isnan(dit)) {printf("dit %f YSN %f alpha %f beta %f V %f dydt %f , Calcium %f\n",dit,YSN,alpha,beta,V,dydt,CalciumLevel);}
        return dit;
    #else
        if (beta && (gateConstStruct->aFtype & mask2) == 4) {
            return YSN + dt*((alpha - YSN) / beta);
        } else {
            return YSN + dt *((1 - YSN) * alpha - YSN * beta);
        }
        return 0;
    #endif
    }
    float fn_UpdateCalc(const float iCa,const GateConstStruct *gateConstStruct,const float V, const float dt, const float YSN){
        const float beta = fCustom_nieuw_b(gateConstStruct->bFtype & mask1,V,gateConstStruct);
        const float alpha = fCustom_nieuw_a(gateConstStruct->aFtype & mask1,V,gateConstStruct);
    #if DEBUGK == 1
        const float dydt = (-iCa) * alpha - YSN * beta;                    //calcium update
        const float dit = YSN + dt * dydt;
        if(std::isinf(dit)) {printf("dit %f celloffset %d YSN %f alpha %f beta %f V %f dydt %f iCa %f\n",dit,10,YSN,alpha,beta,V,dydt,iCa);}
        if(std::isnan(dit)) {printf("dit %f celloffset %d YSN %f alpha %f beta %f V %f dydt %f iCa %f\n",dit,10,YSN,alpha,beta,V,dydt,iCa);}
        return dit;
    #else
        return YSN + dt*((-iCa) * alpha - YSN * beta);                    //calcium update
    #endif
    }
    float fn_iChannels(float *currentNext, const CompartmentConstStruct *compartment_variables,const float *ysNow,const float V, const float dt,const float *calcNow, float *calcNext,const float *g, const float *Vchan){
        float iChannels = 0;
        float iCa = 0;
        uint32_t gate_plus = 0;
        for(size_t i = 0; i < compartment_variables->nChannels; i++){
            float Yprod = 1;
    #if ORDEREDBLOCK == 1
            const float gc = g[i*BLOCKSIZE];
            const float Vchanc = Vchan[i*BLOCKSIZE];
    #else
            const float gc  = g[i];
            const float Vchanc = Vchan[i];
    #endif
            for(size_t j = 0; j < compartment_variables->Channel[i].nGates; j++){
                float YprodN = 0;
    #if ORDEREDBLOCK == 1
                YprodN = ysNow[gate_plus * BLOCKSIZE];
    #else
                YprodN = ysNow[gate_plus];
    #endif
                if ((compartment_variables->Channel[i].Gate[j].bFtype & mask2) == 12)
                    YprodN = fCustom_nieuw_b(compartment_variables->Channel[i].Gate[j].bFtype & mask1,V,&compartment_variables->Channel[i].Gate[j]);
                YprodN = powf(YprodN,compartment_variables->Channel[i].Gate[j].p);
                Yprod = Yprod * YprodN;
                gate_plus++;
            }
            float iChannel = Yprod;
            iChannel =  gc * (V - Vchanc) * Yprod;
            if(compartment_variables->Channel[i].is_ca2plus_channel) {
                iCa += iChannel;
            }
            currentNext[i] = iChannel;
            iChannels +=iChannel;
        }
    //    --->update Calcium concentration
        if(compartment_variables->enable_CalciumConcentration){
            float calc_conc = fn_UpdateCalc(iCa, &compartment_variables->CalciumConcentration,V,dt,calcNow[0]);
            calcNext[0] = calc_conc;
        }
        return iChannels;
    }
    float fn_iChannels_and_update(const CompartmentConstStruct *compartment_variables,const float *ysNow, float *ynNext, const float V, const float dt,const float *calcNow, float *calcNext,const float *g, const float *Vchan){
        float iChannels = 0;
        float iCa = 0;
        uint32_t gate_plus = 0;
        for(size_t i = 0; i < compartment_variables->nChannels; i++){
            float Yprod = 1;
    #if ORDEREDBLOCK == 1
            const float gc = g[i*BLOCKSIZE];
            const float Vchanc = Vchan[i*BLOCKSIZE];
    #else
            const float gc  = g[i];
            const float Vchanc = Vchan[i];
    #endif
            for(size_t j = 0; j < compartment_variables->Channel[i].nGates; j++){
                float YprodN = 0;
    #if ORDEREDBLOCK == 1
                YprodN = ysNow[gate_plus * BLOCKSIZE];
    #else
                YprodN = ysNow[gate_plus];
    #endif
                if ((compartment_variables->Channel[i].Gate[j].bFtype & mask2) == 12)
                    YprodN = fCustom_nieuw_b(compartment_variables->Channel[i].Gate[j].bFtype & mask1,V,&compartment_variables->Channel[i].Gate[j]);   //instateneous variables
                if((compartment_variables->Channel[i].Gate[j].aFtype & mask2) == 8) {
                    YprodN = 1; //calcium channel only needed when performing updates!  //todo
                }
                YprodN = powf(YprodN,compartment_variables->Channel[i].Gate[j].p);
                Yprod = Yprod * YprodN;


                //update gates
    #if ORDEREDBLOCK == 1
                ynNext[gate_plus * BLOCKSIZE] = fn_UpdateYS(&compartment_variables->Channel[i].Gate[j], V, dt, ysNow[gate_plus * BLOCKSIZE], calcNow[0]);
    #else
                ynNext[gate_plus] = fn_UpdateYS(&compartment_variables->Channel[i].Gate[j], V, dt, ysNow[gate_plus], calcNow[0]);
    #endif
                gate_plus++;
            }
            float iChannel = Yprod;
            iChannel =  gc * (V - Vchanc) * Yprod;
            if(compartment_variables->Channel[i].is_ca2plus_channel) {
                iCa += iChannel;
            }
            iChannels +=iChannel;
        }

        //--->update Calcium concentration
        if(compartment_variables->enable_CalciumConcentration){
            float calc_conc = fn_UpdateCalc(iCa, &compartment_variables->CalciumConcentration,V,dt,calcNow[0]);
            calcNext[0] = calc_conc;
        }

        return iChannels;
    }

void CompartmentUpdate_all(size_t step,SimRunInfo &sim, Network_state &NetworkState_l, const StimulusInfo *stimulusInfo, NetworkConstStruct &NetworkConst_l,float dt){
#if ORDEREDBLOCK == 1
    #pragma omp parallel for schedule(dynamic, BLOCKSIZE)
#else
    #pragma omp parallel for
#endif
        for(uint32_t compartment_No = 0; compartment_No < NetworkConst_l.nCompartments_l; compartment_No++) {
            //get Compartment voltage
            const float V = NetworkState_l.vsNow[compartment_No];

            //get indexes
            const uint32_t cellid = NetworkConst_l.CellID_match_compID[compartment_No];
            const uint32_t compartmentIndex = NetworkConst_l.CompartmentIndex[compartment_No];
            const uint32_t ChannelIndex = NetworkConst_l.ChannelIndex[compartment_No];
            const uint32_t gate_index = NetworkConst_l.GateIndex[compartment_No];

            const CompartmentConstStruct *compartment_variables = &NetworkConst_l.compartment[compartmentIndex];

            bool first_compartment = false;
            if (compartment_variables->identifier == 0 || compartment_variables->identifier == 3)
                first_compartment = true;

            const float iLeak = fn_ileak(compartment_variables->gLeak, V, compartment_variables->vLeak);

            //--->Interaction Current
            const float iInteract = fn_iInteract(compartment_variables->gInt,
                                                 compartment_variables->identifier, V,
                                                 compartment_variables->gp,
                                                 &NetworkConst_l.compartment[compartmentIndex + 1].gp,
                                                 &NetworkState_l.vsNow[compartment_No]);

            //--->Gap Connection Current
            float iGap = 0;
            if (first_compartment) {
                //todo
                if (sim.conn_info.type == ConnectivityInfo::CONSTANT) {
                    iGap = fn_Igap_full(cellid, NetworkConst_l.weight, NetworkState_l.VS_GAP, NetworkConst_l.nCells, NetworkConst_l.CellID_Offset);
                }
                else if (sim.conn_info.type != ConnectivityInfo::NONE && sim.conn_info.use_condu) {
                    iGap = fn_Igap_condu(cellid, NetworkState_l.VS_GAP,NetworkConst_l.CellPopulation, NetworkConst_l.CellID_Offset);
                }
                else if (sim.conn_info.type != ConnectivityInfo::NONE) {
                    iGap = fn_Igap(cellid, NetworkConst_l.weight, NetworkState_l.VS_GAP,NetworkConst_l.CellPopulation, NetworkConst_l.CellID_Offset);
                }
            }

            //--->Application Current
            const float iApp = fn_iApp(step, stimulusInfo, first_compartment, cellid + NetworkConst_l.CellID_Offset, NetworkState_l.ou_noise,(XorShiftMul*)NetworkState_l.rand_state, NetworkState_l.rng->GetF());

            //--->Channel current
            const float iChannels = fn_iChannels_and_update(compartment_variables, &NetworkState_l.ysNow[gate_index], &NetworkState_l.ysNext[gate_index],
                                                            V, dt, &NetworkState_l.calcNow[compartment_No], &NetworkState_l.calcNext[compartment_No],
                                                            &NetworkConst_l.G_Channels[ChannelIndex],
                                                            &NetworkConst_l.Inverpotential_Channels[ChannelIndex]);

            //--->Update potentials
//            if(iApp != 0.0) std::cout<<"IAPP\n";
//            if(iGap != 0.0) std::cout<<"iGap\n";


            const float output = V + dt * ((-iGap - iChannels - iInteract - iLeak + iApp) * compartment_variables->S);
            NetworkState_l.vsNext[compartment_No] = output;
            if (first_compartment) {
                NetworkState_l.VS_GAP_N[cellid + NetworkConst_l.CellID_Offset] = output;
            }
        } //close inner for loop
    } // close CompartmentUpdate_all
} //close name space


bool RunTimestep_cpu(size_t step, SimRunInfo &sim, Network_state &NetworkState_l,NetworkConstStruct &NetworkConst_l){
    using namespace CPU_kernel_functions;

    //run a timestep
    CompartmentUpdate_all(step,sim,NetworkState_l,&sim.stimulusInfo,NetworkConst_l,sim.timestep);

    //copy back the state to host buffer before printing
    if(sim.output_monitor.type != OutputMonitor::NONE) {
        if (((step) % sim.output_monitor.timestep_save_interval == 0)) {
            fn_copy_back_dev_to_host_cpu(sim, NetworkState_l, NetworkConst_l);
        }
    }
    return true;
}

bool fn_init_cpu_Backend(SimRunInfo &sim, process_info &ProcessInfo,Network_state &NetworkState_l,NetworkConstStruct *NetworkConst_l){
    INIT_LOGP(LOG_DEFAULT,ProcessInfo.world_rank);
    log(LOG_DEBUG) << "fn_init_cpu_Backend" << LOG_ENDL;

    NetworkConst_l->CellPop      = NetworkConst_l->CellPopulation;
    NetworkConst_l->compartment  = NetworkConst_l->CellPopulation[0].compartment;
    NetworkConst_l->Channel      = NetworkConst_l->CellPopulation[0].compartment[0].Channel;
    NetworkConst_l->Gate         = NetworkConst_l->CellPopulation[0].compartment[0].Channel[0].Gate;

    //initialize memory
    NetworkState_l.vsNow        = new float[NetworkConst_l->nCompartments_l];
    NetworkState_l.vsNext       = new float[NetworkConst_l->nCompartments_l];
    NetworkState_l.calcNow      = new float[NetworkConst_l->nCompartments_l];
    NetworkState_l.calcNext     = new float[NetworkConst_l->nCompartments_l];
    NetworkState_l.currentsNow  = new float[NetworkConst_l->nChannels_l];
    NetworkState_l.currentsNext = new float[NetworkConst_l->nChannels_l];
    NetworkState_l.ysNow        = new float[NetworkConst_l->nGates_l];
    NetworkState_l.ysNext       = new float[NetworkConst_l->nGates_l];
    NetworkState_l.VS_GAP_N     = new float[NetworkConst_l->nCells]; //dubble dubber
    NetworkState_l.VS_GAP       = new float[NetworkConst_l->nCells];
    //    NetworkState_l.Igap         = new float[NetworkConst_l->nCompartments_l];

    log(LOG_DEBUG) << "copys:" <<NetworkState_l.VS_GAP_N << "\t" <<NetworkState_l.VS_GAP  <<  "\t" << NetworkState_l.VS_GAP_host<< LOG_ENDL;

    //copy the initial states to the correct buffers
    std::memcpy(NetworkState_l.vsNow,       NetworkState_l.hostVs,       NetworkConst_l->nCompartments_l * sizeof(float));
    std::memcpy(NetworkState_l.calcNow,     NetworkState_l.hostCalc,     NetworkConst_l->nCompartments_l * sizeof(float));
    std::memcpy(NetworkState_l.currentsNow, NetworkState_l.hostCurrents, NetworkConst_l->nChannels_l * sizeof(float));
    std::memcpy(NetworkState_l.ysNow,       NetworkState_l.hostYs,       NetworkConst_l->nGates_l * sizeof(float));

    std::memcpy(NetworkState_l.VS_GAP,      NetworkState_l.VS_GAP_host,  NetworkConst_l->nCells * sizeof(float));

    //    std::memcpy(NetworkState_l.VS_GAP_N,    NetworkState_l.VS_GAP_host,  NetworkConst_l->nCells * sizeof(float));

    log(LOG_DEBUG,0) << "remap networkconst to device memory" << LOG_ENDL;
    auto *CellConstStruct_dev       = new CellConstStruct[NetworkConst_l->nCells_l];
    auto *CompartmentConstStruct_dev= new CompartmentConstStruct[NetworkConst_l->nCompartments_u];
    auto *ChannelConstStruct_dev    = new ChannelConstStruct[NetworkConst_l->nChannels_u];
    auto *GateConstStruct_dev       = new GateConstStruct[NetworkConst_l->nGates_u];

    fn_cellStructure_Initializations(sim,NetworkConst_l,CellConstStruct_dev,CompartmentConstStruct_dev,ChannelConstStruct_dev,GateConstStruct_dev, NetworkConst_l->CellID_Offset, NetworkConst_l->nCells_l);

    std::memcpy(CellConstStruct_dev,        NetworkConst_l->CellPopulation,   NetworkConst_l->nCells_l        * sizeof(CellConstStruct));
    std::memcpy(CompartmentConstStruct_dev, NetworkConst_l->compartment,      NetworkConst_l->nCompartments_u * sizeof(CompartmentConstStruct) );
    std::memcpy(ChannelConstStruct_dev,     NetworkConst_l->Channel,          NetworkConst_l->nChannels_u     * sizeof(ChannelConstStruct)     );
    std::memcpy(GateConstStruct_dev,        NetworkConst_l->Gate,             NetworkConst_l->nGates_u        * sizeof(GateConstStruct)        );

    delete[] NetworkConst_l->CellPopulation;
    delete[] NetworkConst_l->compartment;
    delete[] NetworkConst_l->Channel;
    delete[] NetworkConst_l->Gate;

    NetworkConst_l->CellPopulation = CellConstStruct_dev;
    NetworkConst_l->compartment    = CompartmentConstStruct_dev;
    NetworkConst_l->Channel        = ChannelConstStruct_dev;
    NetworkConst_l->Gate           = GateConstStruct_dev;

    NetworkState_l.rng = new XorShiftMul(((uint64_t)sim.stimulusInfo.ornsteinUhlenbeck.random_seed<<32) + sim.stimulusInfo.ornsteinUhlenbeck.random_seed);  //heap alloc
    if(sim.stimulusInfo.type == StimulusInfo::ORNSTEINUHLENBECK) {
        NetworkState_l.ou_noise = new float[NetworkConst_l->nCompartments_l];
        auto *temp_vec = new std::vector<XorShiftMul>;
        temp_vec->reserve(NetworkConst_l->nCompartments_l);
        for(size_t t = 0; t<NetworkConst_l->nCompartments_l; t++){
            temp_vec->push_back(XorShiftMul(((uint64_t) sim.stimulusInfo.ornsteinUhlenbeck.random_seed << 32) + t + NetworkConst_l->CellID_Offset));
            NetworkState_l.ou_noise[t] = 0;
        }
        NetworkState_l.rand_state = temp_vec->data();
    }

    return true;
}
bool fn_copy_back_dev_to_host_cpu(SimRunInfo sim, Network_state &NetworkState_l,NetworkConstStruct &NetworkConst_l){
    if (sim.output_monitor.WriteCompartPotentials)
        std::memcpy( NetworkState_l.hostVs,       NetworkState_l.vsNow, NetworkConst_l.nCompartments_l * sizeof(float));
    if (sim.output_monitor.WriteCalciumLevels)
        std::memcpy( NetworkState_l.hostCalc,       NetworkState_l.calcNow, NetworkConst_l.nCompartments_l * sizeof(float));
    if (sim.output_monitor.WriteCurrents)
        std::memcpy( NetworkState_l.hostCurrents,       NetworkState_l.currentsNow, NetworkConst_l.nChannels_l * sizeof(float));
    if (sim.output_monitor.WriteActivationVariables)
        std::memcpy( NetworkState_l.hostYs,       NetworkState_l.ysNow, NetworkConst_l.nGates_l * sizeof(float));
    return true;
}
void fn_copy_host_to_dev_cpu(void* dev, void* host, size_t size){
    memcpy(dev, host, size);
}