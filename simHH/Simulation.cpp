//
// Created by max on 24-03-21.
//

#include <iomanip>        // std::setprecision
#include <thread>         // std::thread

#include "Simulation.h"
#include "miniLogger/miniLogger.h"
#include "CPU_kernels.h"
#include "GPU_kernels.h"
#include "mpi_communication.h"
#include "memory.h"

#define PROGRESS_INTERVALS 10
#define SIMULATION_DEBUG 1

bool runSimulation(SimRunInfo &sim,process_info &ProcessInfo,Network_state *NetworkState_l,NetworkConstStruct* NetworkConst_l, RunMetaData &runMetaData){

    Timer runSimulation_Timer;
    //enabeling a welkom message
    INIT_LOGP(LOG_DEFAULT,ProcessInfo.world_rank);

    if(ProcessInfo.GPU) log(LOG_INFO) << "RUNNING simulation with GPU support : " <<ProcessInfo.GPU <<LOG_ENDL;
    else log(LOG_INFO) << "RUNNING simulation with CPU support" <<LOG_ENDL;
    log.set_Loglevel(LOG_DEFAULT);

    //create threat for writing the output
    std::thread Write_Output_Thread;

    //prep output file(s)
    sim.output_monitor.PrintTimestep(
            NetworkState_l[0].hostVs,
            NetworkState_l[0].hostYs,
            NetworkState_l[0].hostCalc,
            NetworkState_l[0].hostCurrents,
            sim.timestep,
            0,
            NetworkConst_l->nCells_l,
            NetworkConst_l->nCompartments_l,
            NetworkConst_l->nChannels_l,
            NetworkConst_l->nGates_l,
            (int)(sim.time/sim.timestep),
            &runMetaData.OutputWriteTime_ms
    );

#ifdef USE_MPI
    if(ProcessInfo.world_size>1) {
        mpi_communicator_init(NetworkState_l, NetworkConst_l, ProcessInfo);
        runMetaData.NoXternalConnections = ProcessInfo.xcell_size_local;
    }
#endif


    //-> INITIALIZE the backend
    if(ProcessInfo.GPU){
#ifdef USE_CUDA
        log(LOG_DEBUG,0) << "INIT GPU MEMORY" << LOG_ENDL;
        NetworkState_l = fn_init_gpu_Backend_state(sim,ProcessInfo, NetworkState_l, NetworkConst_l);
        NetworkConst_l = fn_init_gpu_Backend_const(sim,ProcessInfo, NetworkState_l, NetworkConst_l);
        fn_init_gpu_Backend_streams(&ProcessInfo,runMetaData); //must be last because of metadata!

#else
        log(LOG_ERR) << "no gpubackend support build in" << LOG_ENDL;
        exit(3);
#endif
    } else {
        log(LOG_DEBUG,0) << "INIT CPU MEMORY" << LOG_ENDL;
        fn_init_cpu_Backend(sim,ProcessInfo, *NetworkState_l, NetworkConst_l);
        runMetaData.metaCPU.peak_resident_memory_bytes = get_memory_total_cpu();
        runMetaData.metaCPU.current_resident_memory_bytes = get_memory_cpu_current();
    }



    //save all metadata
    runMetaData.init(NetworkConst_l);
    runMetaData.initMPI(ProcessInfo);
    runMetaData.Networkinitializations_ms += runSimulation_Timer.get_time();


    log(LOG_DEBUG) << "Start outerloop of the simulation" << LOG_ENDL;

    for (size_t step = 0; step < (size_t) (sim.time / sim.timestep); step++) {
        Timer RunTimestep_timer;

        //print the output :D
        {

            if (sim.output_monitor.type != OutputMonitor::NONE && step > sim.output_monitor.timestep_save_interval && ((step - 1) % sim.output_monitor.timestep_save_interval == 0)) {
                Timer Wait_outputjoin;
                if (Write_Output_Thread.joinable()) Write_Output_Thread.join();
                runMetaData.WaitForOutputThread_ms +=  Wait_outputjoin.get_time();
                Timer Launch_output;
                float *temp;
                for (int i = 0; i <= ProcessInfo.GPU; i++) {
                    temp = NetworkState_l[i].hostVs_print;
                    NetworkState_l[i].hostVs_print = NetworkState_l[i].hostVs;
                    NetworkState_l[i].hostVs = temp;

                    temp = NetworkState_l[i].hostYs_print;
                    NetworkState_l[i].hostYs_print = NetworkState_l[i].hostYs;
                    NetworkState_l[i].hostYs = temp;

                    temp = NetworkState_l[i].hostCalc_print;
                    NetworkState_l[i].hostCalc_print = NetworkState_l[i].hostCalc;
                    NetworkState_l[i].hostCalc = temp;

                    temp = NetworkState_l[i].hostCurrents_print;
                    NetworkState_l[i].hostCurrents_print = NetworkState_l[i].hostCurrents;
                    NetworkState_l[i].hostCurrents = temp;
                    if (i > ProcessInfo.GPU - 2) {
                        break;
                    }
                }
                const uint32_t nCE = NetworkConst_l->nCells_l;
                const uint32_t nCO = NetworkConst_l->nCompartments_l;
                const uint32_t nCh = NetworkConst_l->nChannels_l;
                const uint32_t nGA = NetworkConst_l->nGates_l;

                Write_Output_Thread = std::thread(
                        &OutputMonitor::PrintTimestep,
                        sim.output_monitor,
                        NetworkState_l[0].hostVs_print,
                        NetworkState_l[0].hostYs_print,
                        NetworkState_l[0].hostCalc_print,
                        NetworkState_l[0].hostCurrents_print,
                        sim.timestep,
                        step,
                        nCE,
                        nCO,
                        nCh,
                        nGA,
                        (int) (sim.time / sim.timestep),
                        &runMetaData.OutputWriteTime_ms);

                runMetaData.LaunchWriteThread_ms += Launch_output.get_time();
            }
        }

        //compute a timestep on a specific Backend
        {
            //todo //cleanup thread management plz for CPU
            if(!ProcessInfo.GPU)
                if (Write_Output_Thread.joinable()) Write_Output_Thread.join();
            Timer computeT;
            if (ProcessInfo.GPU) {
                RunTimestep_gpu(step, sim, NetworkState_l, NetworkConst_l, ProcessInfo);
                runMetaData.ComputeLaunchTime_ms += computeT.get_time();
            } else {
                RunTimestep_cpu(step, sim, *NetworkState_l, *NetworkConst_l);
                runMetaData.metaCPU.Computetime_ms += computeT.get_time();
            }
        }

        //Sync process 0 (GJ - comp - share)
        {
            Timer sync1T;
            if (ProcessInfo.GPU) {
                sync_process_stream(ProcessInfo, 0);
                runMetaData.Synchronizingtheloop1_ms += sync1T.get_time();
            } else {
                //sync up cpu
            }
        }

#ifdef USE_MPI
//share with MPI
        if(ProcessInfo.world_size>1) {
            Timer mpi_timer;
            mpi_communicator_timestep(NetworkState_l,NetworkConst_l,&ProcessInfo);
            runMetaData.MPICommunicationTime_ms += mpi_timer.get_time();
        }
#endif

        //Sync all streams
        if(ProcessInfo.GPU) {
            Timer sync2T;
            sync_process_stream(ProcessInfo,0);
            sync_process_stream(ProcessInfo,1);
            sync_process_stream(ProcessInfo,2);
            runMetaData.Synchronizingtheloop2_ms += sync2T.get_time();
            if(ProcessInfo.MonitorGPUkernels) updatemetadataGPU(ProcessInfo,runMetaData);
        }else{
//            sync up CPU
        }

       //Shuffle the buffers needed only for out of order processing of neurons (cpu implementation it's not needed but yeah why not) tODO
        if(ProcessInfo.GPU>1){
            for(int i = 0; i < ProcessInfo.GPU; i++) {
                float *temp_pnt = NetworkState_l[i].vsNow;
                NetworkState_l[i].vsNow = NetworkState_l[i].vsNext;
                NetworkState_l[i].vsNext = temp_pnt;
                temp_pnt = NetworkState_l[i].ysNow;
                NetworkState_l[i].ysNow = NetworkState_l[i].ysNext;
                NetworkState_l[i].ysNext = temp_pnt;
                temp_pnt = NetworkState_l[i].calcNow;
                NetworkState_l[i].calcNow = NetworkState_l[i].calcNext;
                NetworkState_l[i].calcNext = temp_pnt;
                temp_pnt = NetworkState_l[i].VS_GAP_N;
                NetworkState_l[i].VS_GAP_N = NetworkState_l[i].VS_GAP;
                NetworkState_l[i].VS_GAP = temp_pnt;
            }
        }else {
            float *temp_pnt = NetworkState_l[0].vsNow;
            NetworkState_l[0].vsNow = NetworkState_l[0].vsNext;
            NetworkState_l[0].vsNext = temp_pnt;
            temp_pnt = NetworkState_l[0].ysNow;
            NetworkState_l[0].ysNow = NetworkState_l[0].ysNext;
            NetworkState_l[0].ysNext = temp_pnt;
            temp_pnt = NetworkState_l[0].calcNow;
            NetworkState_l[0].calcNow = NetworkState_l[0].calcNext;
            NetworkState_l[0].calcNext = temp_pnt;
            temp_pnt = NetworkState_l[0].VS_GAP_N;
            NetworkState_l[0].VS_GAP_N = NetworkState_l[0].VS_GAP;
            NetworkState_l[0].VS_GAP = temp_pnt;
        }

        //LOG progress to console
        if (step && (step % ((int) ((sim.time / sim.timestep) / PROGRESS_INTERVALS + 0.9))) == 0) {
#if SIMULATION_DEBUG == 1
            log(LOG_MES,0) << "Step "<< step<< " v(0,0) = "<< std::fixed << std::setprecision(5) << NetworkState_l[0].hostVs[0] << ", progress = " <<  (int) ((double(step) / (sim.time / sim.timestep)) * 100) << "%" << std::setprecision(2)<< "\t\t (" << runSimulation_Timer.get_time() << " ms)" << LOG_ENDL;
#else
            float temp = (float)runSimulation_Timer.get_time() / step;
            size_t temp2 = (size_t)(temp * (float)((size_t) (sim.time / sim.timestep) - step)/1000);
            log(LOG_MES,0) << "progress = " <<  (int) ((double(step) / (sim.time / sim.timestep)) * 100) << "%" << "\t etl: " <<temp2 << " s \t\t rt: " << (size_t)runSimulation_Timer.get_time()/1000 << " s" << LOG_ENDL;
#endif
        }
        runMetaData.Steps++;
        runMetaData.Timsteps_ms = runSimulation_Timer.get_time();
    }

    //get back potential write output thread
    if(Write_Output_Thread.joinable()) Write_Output_Thread.join();

    //
    if(ProcessInfo.GPU){
        //todo i dont know if i need to set my device here Could go wrong :( todo veranderen..
        for(int i = 0; i < ProcessInfo.GPU; i++) {
            fn_copy_back_dev_to_host_gpu(sim, NetworkState_l[i], NetworkConst_l[i], NetworkConst_l[i].GPU_numbers[i] );
        }

    }else{
        fn_copy_back_dev_to_host_cpu(sim,NetworkState_l[0],*NetworkConst_l);
    }

    //LOG last step to console
#if SIMULATION_DEBUG == 1
    log(LOG_MES,0) << "Step "<< (int)(sim.time/sim.timestep)<< " v(0,0) = "<< std::fixed << std::setprecision(5) << NetworkState_l[0].hostVs[0] << ", progress = 100%" << std::setprecision(2)<< "\t\t (" << runSimulation_Timer.get_time() << " ms)" << LOG_ENDL;
#else
    log(LOG_MES,0) << "progress = " << "100%" << "\t\t The simulation loop took: " << (size_t)(runSimulation_Timer.get_time()/1000) << " s)" << LOG_ENDL;
#endif
    //LOG last step to file
    {
        if(sim.output_monitor.type != OutputMonitor::NONE &&  ((size_t)(sim.time / sim.timestep))%sim.output_monitor.timestep_save_interval == 0) {
            const uint32_t nCE = NetworkConst_l->nCells_l;
            const uint32_t nCO = NetworkConst_l->nCompartments_l;//why needed ?
            const uint32_t nCh = NetworkConst_l->nChannels_l;
            const uint32_t nGA = NetworkConst_l->nGates_l;
            sim.output_monitor.PrintTimestep(
                    NetworkState_l[0].hostVs,
                    NetworkState_l[0].hostYs,
                    NetworkState_l[0].hostCalc,
                    NetworkState_l[0].hostCurrents,
                    sim.timestep,
                    sim.time / sim.timestep,
                    nCE,
                    nCO,
                    nCh,
                    nGA,
                    (int) (sim.time / sim.timestep),
                    &runMetaData.OutputWriteTime_ms
                    );
        }
    }
    return true;
}