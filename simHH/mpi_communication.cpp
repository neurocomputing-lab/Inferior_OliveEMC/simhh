//
// Created by max on 03-05-21.
//

#include "mpi_communication.h"
#include "GPU_kernels.h"

bool mpi_communicator_init(Network_state* NetworkState_l, NetworkConstStruct* NetworkConst_l, process_info& ProcessInfo){
    //we share the initial vs gap network first with all nodes anyway
    if(ProcessInfo.world_size>1) {
            MPI_CHECK_RETURN(MPI_Allgather(
                    MPI_IN_PLACE,                                                     //sendbuffer
                    1,                                                                //sendcount
                    MPI_INT,                                                          //type
                    NetworkState_l[0].VS_GAP_host,                                    //receivebuffer
                    NetworkConst_l->nCells_l,                                         //recvcount (from any process)
                    MPI_INT,                                                          //type
                    MPI_COMM_WORLD));                                                 //handle
    }


    //send and receive displacements.
    if(ProcessInfo.MPI_alltoallv_enable) {


        ProcessInfo.sdispls = new int[ProcessInfo.world_size];
        ProcessInfo.rdispls = new int[ProcessInfo.world_size];
        ProcessInfo.sdispls[0] = 0;
        ProcessInfo.rdispls[0] = 0;

        //todo move filling the processinfo.connection in use to here.
        //---> sorting out who needs to get what.
        {
            ProcessInfo.xcell_count_pp = new int[ProcessInfo.world_size];
            ProcessInfo.xcell_count_pp_g = new int[ProcessInfo.world_size];
            ProcessInfo.xcell = new int[NetworkConst_l->nCells];

            int TEMPNODE = 0;
            ProcessInfo.xcell_count_pp[TEMPNODE] = 0;
            for (size_t i = 0; i < NetworkConst_l->nCells; i++) {
                if (i % NetworkConst_l->nCells_l == 0 && i) {
                    TEMPNODE++;
                    ProcessInfo.xcell_count_pp[TEMPNODE] = 0;
                }
                if (ProcessInfo.connections_inuse[i] && (TEMPNODE != ProcessInfo.world_rank)) {
                    ProcessInfo.xcell_count_pp[TEMPNODE]++;
                    ProcessInfo.xcell[ProcessInfo.xcell_size_local] = i;
                    ProcessInfo.xcell_size_local++;
                }
            }

            MPI_CHECK_RETURN(MPI_Alltoall(
                    ProcessInfo.xcell_count_pp,
                    1,
                    MPI_INT,
                    ProcessInfo.xcell_count_pp_g,
                    1,
                    MPI_INT,
                    MPI_COMM_WORLD));

            for (int i = 1; i < ProcessInfo.world_size; ++i)
                ProcessInfo.sdispls[i] = ProcessInfo.sdispls[i - 1] + ProcessInfo.xcell_count_pp[i - 1];

            for (int i = 1; i < ProcessInfo.world_size; ++i)
                ProcessInfo.rdispls[i] = ProcessInfo.rdispls[i - 1] + ProcessInfo.xcell_count_pp_g[i - 1];

            for (int j = 0; j < ProcessInfo.world_size; ++j)
                ProcessInfo.xcell_size_global += ProcessInfo.xcell_count_pp_g[j];

            ProcessInfo.cellstoSend = new int[ProcessInfo.xcell_size_global];
            MPI_CHECK_RETURN(MPI_Alltoallv(
                    ProcessInfo.xcell,
                    ProcessInfo.xcell_count_pp,
                    ProcessInfo.sdispls,
                    MPI_INT,
                    ProcessInfo.cellstoSend,
                    ProcessInfo.xcell_count_pp_g,
                    ProcessInfo.rdispls,
                    MPI_INT,
                    MPI_COMM_WORLD));
        }
        ProcessInfo.VtoSend_MPI  = new float[ProcessInfo.xcell_size_global+1];
        ProcessInfo.VtoStore_MPI = new float[ProcessInfo.xcell_size_local+1];

    }else{
        if(ProcessInfo.GPU) {
#ifdef MPI_CUDA_AWARE
            //todo memory leak !
//            std::cout << NetworkState_l->VS_GAP_host << "\n";
//            std::cout << "free1?" << "\n";
//            cudaApi_free(NetworkState_l->VS_GAP_host);
//            std::cout << "check" << "\n";
#else

#endif
        }
    }
    return true;
}


bool mpi_communicator_timestep(Network_state* NetworkState_l, NetworkConstStruct* NetworkConst_l, process_info* ProcessInfo) {
    INIT_LOGP(LOG_DEFAULT,ProcessInfo->world_rank);

    if(ProcessInfo->world_size>1) {
        // --------->> FIX the memory
        {
            if (ProcessInfo->GPU) {
                setdevice(0);
                if (ProcessInfo->MPI_alltoallv_enable) {
#ifdef MPI_CUDA_AWARE
                    //TODO
                    fn_copy_vsgap_to_host_gpu(NetworkState_l[0].VS_GAP_host, NetworkState_l[0].VS_GAP_N, NetworkConst_l->nCells_l, NetworkConst_l->CellID_Offset,ProcessInfo);
                    for (int i = 0; i < ProcessInfo->xcell_size_global; i++) {
                        ProcessInfo->VtoSend_MPI[i] = NetworkState_l->VS_GAP_host[ProcessInfo->cellstoSend[i]];
                    }
#else
                    fn_copy_vsgap_to_host_gpu(NetworkState_l[0].VS_GAP_host, NetworkState_l[0].VS_GAP_N, NetworkConst_l->nCells_l, NetworkConst_l->CellID_Offset,ProcessInfo);
                    for (int i = 0; i < ProcessInfo->xcell_size_global; i++) {
                        ProcessInfo->VtoSend_MPI[i] = NetworkState_l->VS_GAP_host[ProcessInfo->cellstoSend[i]];
                    }
#endif
                } else {
#ifdef MPI_CUDA_AWARE
                    NetworkState_l[0].VS_GAP_host = NetworkState_l[0].VS_GAP_N;
#else
                    fn_copy_vsgap_to_host_gpu(NetworkState_l[0].VS_GAP_host, NetworkState_l[0].VS_GAP_N, NetworkConst_l->nCells_l, NetworkConst_l->CellID_Offset,ProcessInfo);
#endif
                }
            } else {
                NetworkState_l[0].VS_GAP_host = NetworkState_l[0].VS_GAP_N; //ja dit is huilon he deze pointer managment :P let op switch elke step dus wel nodig
            }
        }

        // --------->> SHARING
        {
            if (ProcessInfo->MPI_alltoallv_enable) {
                MPI_CHECK_RETURN(MPI_Alltoallv(
                        ProcessInfo->VtoSend_MPI,
                        ProcessInfo->xcell_count_pp_g,
                        ProcessInfo->rdispls,
                        MPI_FLOAT,
                        ProcessInfo->VtoStore_MPI,
                        ProcessInfo->xcell_count_pp,
                        ProcessInfo->sdispls,
                        MPI_FLOAT,
                        MPI_COMM_WORLD));
            } else {
                MPI_CHECK_RETURN(MPI_Allgather(
                        MPI_IN_PLACE,    //sendbuffer
                        1,                                         //sendcount
                        MPI_INT,                                                          //type
                        NetworkState_l[0].VS_GAP_host,                                    //receivebuffer
                        NetworkConst_l->nCells_l,                                         //recvcount (from any process)
                        MPI_INT,                                                          //type
                        MPI_COMM_WORLD));                                                 //handle
            }
        }

        // --------->> FIX the memory
        {
            if (ProcessInfo->GPU) {
                if (ProcessInfo->MPI_alltoallv_enable) {
#ifdef MPI_CUDA_AWARE
                    //TODO
                    for (int i = 0; i < ProcessInfo->xcell_size_local; ++i) {
                        NetworkState_l->VS_GAP_host[ProcessInfo->xcell[i]] = ProcessInfo->VtoStore_MPI[i];
                    }
                    fn_copy_vsgap_to_device_gpu(NetworkState_l[0].VS_GAP_host, NetworkState_l[0].VS_GAP_N, NetworkConst_l->nCells,ProcessInfo); //iknow weird order
#else
                    for (int i = 0; i < ProcessInfo->xcell_size_local; ++i) {
                        NetworkState_l->VS_GAP_host[ProcessInfo->xcell[i]] = ProcessInfo->VtoStore_MPI[i];
                    }
                    fn_copy_vsgap_to_device_gpu(NetworkState_l[0].VS_GAP_host, NetworkState_l[0].VS_GAP_N, NetworkConst_l->nCells,ProcessInfo); //iknow weird order
#endif
                } else {
#ifndef MPI_CUDA_AWARE
                    fn_copy_vsgap_to_device_gpu(NetworkState_l[0].VS_GAP_host, NetworkState_l[0].VS_GAP_N, NetworkConst_l->nCells,ProcessInfo); //iknow weird order
#endif
                }
                //copy over multiple gpus
                if (ProcessInfo->GPU > 1) {
                    share_betweenGPUS_aftermpi(ProcessInfo, NetworkState_l, NetworkConst_l);
                }
            } //cpu support is fixed
        }
    }
    return true;
}