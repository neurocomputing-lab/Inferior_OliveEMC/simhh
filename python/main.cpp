#include <pybind11/pybind11.h>
#include "../simHH/includes/simHH.h"
#include <iostream>

bool run(const char *filename){
    return simHH(filename);
}

void print_info() {
    std::cout << "HELLO this is work in progress :D JOKES " << std::endl;
}

namespace py = pybind11;

PYBIND11_MODULE(pysimHH, m) {
    m.doc() = R"pbdoc(
        Pybind11 example plugin
        -----------------------

        .. currentmodule:: cmake_example

        .. autosummary::
           :toctree: _generate

           add
           subtract
    )pbdoc";

    m.def("run", &run, R"pbdoc(
      run with a supplied filename.
    )pbdoc");

    m.def("print_info", &print_info, R"pbdoc(
      Print information about this lib
    )pbdoc");

    m.def("subtract", [](int i, int j) { return i - j; }, R"pbdoc(
        Subtract two numbers
        Some other explanation about the subtract function.
    )pbdoc");
}
